[![Coverage](/app/coverage.svg)]()

**This is an user administration API**

- [erdalmutlu/kaya-api](#kaya-api)
    - [Contributing](#contributing)
    - [Quick Start](#quick-start)

## Contributing
If you find this software useful here's how you can help:

- Send a pull request with your kickass new features and bug fixes
- Help new users with [issues](https://gitlab.com/erdalmutlu/kaya-api/issues) they may encounter
- Support the development of this image and star this repo!

## Quick Start

For docker, please read [docker/README.docker](docker/README.docker)

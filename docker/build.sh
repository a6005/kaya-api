#!/usr/bin/env bash
#
# This script builds docker images.
#
###########################################################################
### defaults
bundle_version="_2.1.4_"
docker_template_file=docker-compose-template.yml
docker_file=docker-compose.yml
env_file=../app/initializers/env.rb
project_dir=`pwd`
image_conf_file=docker_image.conf
### end of defaults
###########################################################################
cleanup()
{
        rm -rf $tmp_file
}

update_app_secrets()
{
	> $tmp_file
	while IFS= read -r line
	do
		str=`echo $line | grep "^APPSECRET = " | grep -v "ENV"`
		if [ -n "$str" ]; then
			s=`LC_ALL=C tr -dc 'A-Za-z0-9' </dev/urandom | head -c 40`
			line="APPSECRET = \"$s\""
		fi
		echo "$line" >> $tmp_file
	done < $env_file
	cp $env_file ${env_file}.org
	mv $tmp_file $env_file
	mv ${env_file}.org $env_file
}

initialize()
{
	
	check_progs
	set_project_dir
	cd $project_dir/docker || exit 1
	if [ ! -f $image_conf_file ]; then
		echo "$0: $image_conf_file file does not exist!"
		exit 1
	fi
	# source the image conf
	. $image_conf_file

	get_git_info
	# update_app_secrets
	update_version
	# download_files
	check_prerequisites
	check_git_porcelain
	check_if_already_build
	prepare_api
}

get_git_info()
{
	version_str=`git describe`
	if [ -z "$version_str" ]; then
		echo "$0: git describe returned epmty string. No tags found!"
		exit 1
	fi
	get_branch_name
}

get_version_from_branch()
{
	version_str=`git branch -v | grep '*' | cut -d " " -f 2`
}

get_branch_name()
{
	branch_name=`git branch --show-current`
}

set_build_env()
{
	case $branch_name in
		test)
			build_env=test
			;;
		master)
			build_env=prod
			;;
		*)
			build_env=dev
			;;
	esac
}

update_version()
{
	v="Version: $version_str"
	echo $v > ../app/VERSION
	echo "VERSION = '$version_str'" > ../app/src/initializers/version.rb
	set_build_env
	sed -e "s/__VERSION__/$version_str/" -e "s/__BUILD_ENV__/$build_env/g" $docker_template_file > $docker_file
}

set_project_dir()
{
	pwd_str=`pwd`
	str=`basename $pwd_str`
	# check whether we are invoked from Jenkins
	if [ -z ${JENKINS_HOME+x} ]; then
		# not from Jenkins
		project_dir=`git rev-parse --show-toplevel`
	else
		# from Jankins
		if [ -z ${WORKSPACE+x} ]; then
			project_dir="${pwd_str}"
		else
			project_dir="${WORKSPACE}"
		fi
	fi
}

# change to docker directory
change_dir()
{
	pwd_str=`pwd`
	str=`basename $pwd_str`
	echo "pwd_str=$pwd_str str=$str"
	if [ $str == "docker" ]; then
		# we are already in the docker directory
		return
	fi
	# docker_dir=$pwd_str/docker

	# check whether we are invoked from Jenkins
	if [ -z ${JENKINS_HOME+x} ]; then
		# not from Jenkins
		docker_dir=`git rev-parse --show-toplevel`/docker
	else
		# from Jankins
		if [ -z ${WORKSPACE+x} ]; then
			docker_dir="${pwd_str}/docker"
		else
			docker_dir="${WORKSPACE}/docker"
		fi
	fi
	echo "change_dir: $docker_dir"
	cd $docker_dir || echo "$0: Could not checnge to directory: $docker_dir!"
}

download_files()
{
	f=wkhtmltopdf-linux-amd64
	if [ -f $HOME/$f ]; then
		echo "Using $f form $HOME directory..."
		cp $HOME/$f app/
	elif [ ! -f app/$f ]; then
		echo "Downloading $f ..."
		curl https://ftp-alt.ics-group.com.tr/linux/$f --silent --output app/$f
		if [ $? -ne 0 ]; then 
			echo "Could not find $f!"
			exit 1
		fi
	fi
	chmod 755 app/$f
}

check_prog()
{
	str=`which $1`
	if [ $? -ne 0 ]; then
		echo "$0: No such program: $1 !"
		exit 1
	fi 
}

check_progs()
{
	for f in bundle curl
	do
		check_prog $f
	done
}

check_prerequisites()
{
	for f in $docker_template_file
	do
		if [ ! -f $f ]; then
			echo "File $f does not exists!"
			exit 1
		fi
	done
}

check_git_porcelain()
{
	str=`git status --porcelain`
	if [ -n "$str" ]; then
		echo "$0: The working tree is not clean!"
		git status --porcelain
		echo "$0: Please commit your changes or disgard them and try again"
		exit 1
	fi
}

prepare_api()
{
	cd $project_dir/app || exit 1

	rm -f spec/support/api/schemas/*.json
	rm -f logs/*.log

	bundle $bundle_version version >/dev/null 2>&1
	if [ $? -ne 0 ] ; then
		echo "$0: Installing bundler version $bundle_version"
		v=${bundle_version//_/}
		gem install bundler -v $v || exit 1
	fi

	echo "$0: Installing gems ..."
	# Gemfile.lock must come from git repo. 
	# rm -f Gemfile.lock
	echo "$0: bundle $bundle_version install"

	bundle $bundle_version install
}

build()
{
	build_docker_images
}

build_docker_images()
{
	cd $project_dir/docker || exit 1

	declare -i i=0
	while [ $i -lt $number_of_images ]
	do
		s="${image_names[$i]}"
		v="${image_versions[$i]}"
		if [ "$v" = "__FROM_GIT__" ]; then
			v=$version_str
		fi
		echo "$0: Building image $s:$v..."
		docker-compose build $s || exit 1
		echo "$0: Saving $s:$v image as $s-${v}.tar ..."
		docker save -o $s-${v}.tar $s:$v || exit 1
		i=i+1
	done
}

check_if_already_build()
{
	cd $project_dir/docker || exit 1

	declare -i i=0
	while [ $i -lt $number_of_images ]
	do
		s="${image_names[$i]}"
		v="${image_versions[$i]}"
		if [ "$v" = "__FROM_GIT__" ]; then
			v=$version_str
		fi
		echo "$0: Checking wether image $s:$v is already build or not..."
		if [ ! -f $s-${v}.tar ]; then
			return
		else
			echo "$0: $s-${v}.tar already exists. No need to build."
		fi
		i=i+1
	done
	echo "$0: All images are alreading built for commit: $version_str. No need for building." 
	exit 0
}
###########################################################################
tmp_file=`mktemp`
trap cleanup EXIT

version_str=""

initialize
build

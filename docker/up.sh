#!/usr/bin/env bash
#
# This script start docker in development environment.
#
#####################################################################
MY_UID=`id -u`
MY_GID=`id -g`
export MY_UID MY_GID
# echo "MY_UID=$MY_UID MY_GID=$MY_GID"

docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d $@

# remove these files
docker exec kaya-api rm -f resque.pid db_migrated.txt

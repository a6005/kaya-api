#!/usr/bin/env bash
#
# This script scans docker images for valnurabilities.
#
###########################################################################
### defaults
image_conf_file=docker_image.conf
SONAR_URL="https://sonarqube-test.elan-prj.com"
### end of defaults
###########################################################################
if [ -z "$SONAR_TOKEN" ]; then
	echo "$0: SONAR_TOKEN is not defined! Please define and export it."
	exit 1
fi

if [ ! -f $image_conf_file ]; then
	echo "$0: image.conf file does not exist!"
	exit 1
fi

# source the image conf
. $image_conf_file
 
version_str=`git describe`
declare -i i=0
while [ $i -lt $number_of_images ]
do
	s="${image_names[$i]}"
	v="${image_versions[$i]}"
	if [ "$v" = "__FROM_GIT__" ]; then
		v=$version_str
	fi
	echo "$0: Checking image $s:$v..."
	docker image ls | grep "$s" | grep "$v"
	if [ $? -ne 0 ]; then
		echo "$0: Docker image $s:$v does not exist! Please build it first."
		i=i+1
		continue
	fi
	echo "$0: Scanning image $s:$v..."
	sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.projectKey=$project_name -Dsonar.projectBaseDir=../app -Dsonar.ruby.coverage.reportPaths=./coverage/.resultset.sonarqube.json -Dsonar.sources=. -Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_TOKEN
	i=i+1
done

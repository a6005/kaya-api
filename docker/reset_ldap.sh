#!/usr/bin/env bash
#
# This script removes and creates OpenLDAP docker volumes.
#
#####################################################################
docker-compose down
for v in kaya-api-ldap-data kaya-api-ldap-slapd-d
do
	docker volume rm $v
	docker volume create --name="$v"
done

#!/usr/bin/env bash

server_str=sunucu20
ssh_port=22
project_str="kaya-api"

namespace_str=elan
tag_str=latest
if [ $# -eq 2 ]; then
	namespace_str=$1
	tag_str=$2
fi

# copy docker images
rsync -e "ssh -p $ssh_port" -av docker/*.tar root@${server_str}:/root/downloads/docker/$project_str/

# load docker images
ssh -p $ssh_port root@$server_str /root/scripts/docker_load_images.sh /root/scripts/docker_load_images_${project_str}.conf

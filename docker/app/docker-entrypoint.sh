#!/bin/sh

set -e

cd /usr/src/app

echo "params: $@"

# bundle exec rake db:migrate || echo "Problem occured during db migration!"

#PIDFILE=./resque.pid BACKGROUND=yes QUEUES=reset_password_ldap,reset_password_yandex bundle exec rake resque:all || echo "Problem occured during resque startup!"

# execute the command what is given with the cmd
exec "$@"
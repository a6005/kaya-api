#!/usr/bin/env bash
#
## This script removes and creates database docker volume.
# This scripts reset the database.
#####################################################################
#docker-compose down
#for v in kaya-api-db-data
#do
#	docker volume rm $v
#	docker volume create --name="$v"
#done
cd ../app
rake db:reset
cd ../docker

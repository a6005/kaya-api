#!/usr/bin/env bash

test_server=sunucu02
project_str="kaya-api"

namespace_str=elan
tag_str=latest
if [ $# -eq 2 ]; then
	namespace_str=$1
	tag_str=$2
fi

# copy docker images
# rsync -e ssh -av docker/*.tar root@${test_server}:/root/downloads/docker/$project_str/

# load docker images
# ssh root@$test_server /root/scripts/docker_load_images.sh /root/scripts/docker_load_images_${project_str}.conf
ssh root@$test_server /root/scripts/docker_pull_images.sh /root/scripts/docker_pull_images_${project_str}.conf $namespace_str $tag_str

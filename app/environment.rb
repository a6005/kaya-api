require 'pathname'
require 'bundler/setup'

APP_ROOT = Pathname.new(File.expand_path('../', __FILE__))

SRC_PATH = APP_ROOT.join('src')
SPEC_PATH = APP_ROOT.join('spec')
API_DEF_PATH = SRC_PATH.join('api_definitions', 'api_definition.rb')
DB_PATH = SRC_PATH.join('db')
HELPERS_PATH = SRC_PATH.join('helpers')
INITIALIZERS_PATH = SRC_PATH.join('initializers')
INTERACTIONS_PATH = SRC_PATH.join('interactions')
LIB_PATH = SRC_PATH.join('lib')
MODELS_PATH = SRC_PATH.join('models')
WORKERS_PATH = SRC_PATH.join('workers')

# these should be on persistent storage
LOG_PATH = ENV['LOG_PATH'] ? Pathname.new(ENV['LOG_PATH']) : APP_ROOT.join('logs')
STORAGE_PATH = ENV['STORAGE_PATH'] ? Pathname.new(ENV['STORAGE_PATH']) : APP_ROOT.join('tmp')

ENVIRONMENT = ENV['ENVIRONMENT'] ? ENV['ENVIRONMENT'] : 'development'
ENV['RACK_ENV'] = ENVIRONMENT

# do not change app secret for in development
if ENVIRONMENT == 'development'
   APP_SECRET = 'IyEvYmluL2Jhc2gKc2V0IC1lCiMgUnVuIHJlc3F1RD15ZXMgUVVFVUVTP0ZSB0aO'
else
   APP_SECRET = ENV['APP_SECRET'] || `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1`
end

JWT_ALGORITHM = ENV['JWT_ALGORITHM'] || 'HS512'

API_PATH_PREFIX = 'api/v1'

API_URL = ENV['API_URL'] || 'http://localhost:9292'
DB_HOST = ENV['DB_HOST'] || 'localhost'

# password history https://www.ibm.com/support/pages/password-strength-rules
# This value specifies how many unique passwords must be used before
# a previous password can be re-used. Passwords that match any 
# password in the history list cannot be reused.
PASSWORD_HISTORY_LENGTH = ENV['PASSWORD_HISTORY_LENGTH'] || 5
PASSWORD_HISTORY_SALT = '$6$by03kVsAXQ1rMBBu'

# man chage
MIN_PASSWORD_DAYS = ENV['MIN_PASSWORD_DAYS'] || 0
MAX_PASSWORD_DAYS = ENV['MAX_PASSWORD_DAYS'] || 180
WARN_PASSWORD_DAYS = ENV['WARN_PASSWORD_DAYS'] || 15 

MIN_PASSWORD_LENGTH = ENV['MIN_PASSWORD_LENGTH'] || 8
PASSWORD_REGEX = /^(?=.*[a-zA-Z])(?=.*[0-9]).{#{MIN_PASSWORD_LENGTH},}$/
#
# /^(?=.*[a-zA-Z])(?=.*[0-9]).{#{MIN_PASSWORD_LENGTH},}$/
#   |             |          |
#   |             |          Ensure there are at least 6 characters.
#   |             Look ahead for an arbitrary string followed by a number.
#   Look ahead for an arbitrary string followed by a letter.
 
SALT_LENGTH = ENV['SALT_LENGTH'] || 16

REDIS_URL = ENV['REDIS_URL'] || 'redis://localhost:6379'
RESQUE_USERNAME = ENV['RESQUE_USERNAME'] || 'admin'
RESQUE_PASSWORD = ENV['RESQUE_PASSWORD'] || 'test123654'
RESQUE_PID_FILE = '/usr/src/app/resque.pid'

DB_MIGRATED_FILE = '/usr/src/app/db_migrated.txt'

SIMPLE_COV_MIN_COVERAGE = ENV['SIMPLE_COV_MIN_COVERAGE'] || 95

# ldap backend
LDAP_URI = ENV['LDAP_URI'] || 'ldap://kaya-api-ldap:389'
LDAP_ADMIN = ENV['LDAP_ADMIN'] || 'admin'
LDAP_PASSWORD = ENV['LDAP_PASSWORD'] || 'secret'
LDAP_DC1 = ENV['LDAP_DC1'] || 'example'
LDAP_DC2 = ENV['LDAP_DC2'] || 'com'
LDAP_BASE_DN = ENV['LDAP_BASE_DN'] || "dc=#{LDAP_DC1},dc=#{LDAP_DC2}"
LDAP_RDN = ENV['LDAP_RDN'] || "cn=#{LDAP_ADMIN},#{LDAP_BASE_DN}"
LDAP_USER_PREFIX = ENV['LDAP_USER_PREFIX'] || 'people'
LDAP_GROUP_PREFIX = ENV['LDAP_GROUP_PREFIX'] || 'groups'
LDAP_SHORT_COMPANY_NAME = ENV['LDAP_SHORT_COMPANY_NAME'] || 'Example'
LDAP_LONG_COMPANY_NAME = ENV['LDAP_LONG_COMPANY_NAME'] || 'Example Inc'
LDAP_ORGANIZATION_NAME = ENV['LDAP_ORGANIZATION_NAME'] || 'Example Organization'

# now require the environments
Bundler.require(:default, ENVIRONMENT.to_sym)
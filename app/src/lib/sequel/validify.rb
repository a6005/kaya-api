module Sequel
  module Plugins
    module Validify
      def self.apply(model, opts=OPTS)
        model.instance_exec do
          plugin :validation_helpers
        end
      end
      module InstanceMethods
        # Validations
        def before_validation
          super
          set_object_defaults if new?
        end
        def validate
          super
          validate_before_create if new?
          validate_before_update unless new?
          validate_before_save
        end
        
        def validate_before_create
=begin
          # This is an abstract method used
          # for validation before a record is created
=end
        end
        def validate_before_update
=begin
          # This is an abstract method used
          # for validation before record is updated
=end
        end
        def validate_before_save
=begin
          # This is an abstract method used
          # for validation before saving a record
          # It is executed after validate_before_create or validate_before_update
=end
        end
        def set_object_defaults
=begin
          # This is an abstract method used
          # to set default values before a new record is cerated
=end
        end
      end
    end
  end
end
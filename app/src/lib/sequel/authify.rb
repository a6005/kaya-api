require "sequel"

module Sequel
  module Plugins
    module Authify
      def self.blank_string?(string)
        string.nil? or string =~ /\A\s*\z/
      end
      
      # Configure the plugin by setting the available options. Options:
      # * :salt_length - the salt_length  when creating password hash. Default: 16
      # confirmation validations won't be included. Default: true
      def self.configure(model, options = {})
        model.instance_eval do
          @salt_length = options.fetch(:salt_length, 16)
          @digest_column = options.fetch(:digest_column, :password_digest)
        end
      end
      
      module ClassMethods
        attr_reader :salt_length, :digest_column
        
        # NOTE: nil as a value means that the value of the instance variable
        # will be assigned as is in the subclass.
        Plugins.inherited_instance_variables(self, :@salt_length => nil, :@digest_column => nil)
      end
      
      module InstanceMethods
        attr_accessor :password_confirmation
        attr_reader :password
        def password=(unencrypted)
          @password = unencrypted
          unless Authify.blank_string?(unencrypted)
            salt = "$6$" + (model.salt_length - 3).times.map { (('a'..'z').to_a + (1..9).to_a + ('A'..'Z').to_a).sample }.join
            self.send "#{model.digest_column}=", unencrypted.crypt(salt)
          end
        end
        
        def authenticate(unencrypted)
          if unencrypted.crypt(self.send(model.digest_column)) == self.send(model.digest_column)
            self
          end
        end
        def validate
          super
          if Authify.blank_string?(self.send(model.digest_column)) ||
            Authify.blank_string?(password) || Authify.blank_string?(password_confirmation)
            errors.add(:password, KAYA_CODE[:blank_password])
          end
          if password != password_confirmation
            errors.add(:password_confirmation, KAYA_CODE[:confirmation_not_match]) 
          end
        end
      end
    end
  end
end
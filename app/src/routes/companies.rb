# frozen_string_literal: true

class App
   hash_routes("/#{API_PATH_PREFIX}").on 'companies' do |r|
     i_dir = 'companies'
     i_name = 'company'
     i_ldap_group_dir = 'ldap_groups'
     i_ldap_group_name = 'ldap_group'
     i_ldap_user_dir = 'ldap_users'
     i_ldap_user_name = 'ldap_user'

     r.on Integer, 'ldap/groups' do |id|
       r.get do
         r.halt *run_interaction(i_ldap_group_dir, "get_#{i_ldap_group_name}", path_params: {id: id})
       end
       r.put do
         r.halt *run_interaction(i_ldap_group_dir, "update_#{i_ldap_group_name}", path_params: {id: id})
       end
     end

     r.on Integer, 'ldap/users' do |id|
       r.get do
         r.halt *run_interaction(i_ldap_user_dir, "get_#{i_ldap_user_name}", path_params: {id: id})
       end
       r.put do
         r.halt *run_interaction(i_ldap_user_dir, "update_#{i_ldap_user_name}", path_params: {id: id})
       end
     end

     r.is Integer do |id|
       r.get do
         r.halt *run_interaction(i_dir, "get_#{i_name}", path_params: {id: id})
       end
       r.put do
         r.halt *run_interaction(i_dir, "update_#{i_name}", path_params: {id: id})
       end
       r.delete do
         r.halt *run_interaction(i_dir, "delete_#{i_name}", path_params: {id: id})
       end
     end
     r.post do
       r.halt *run_interaction(i_dir, "create_#{i_name}")
     end
     r.get do
       r.halt *run_interaction(i_dir, "search_#{i_name}")
     end     
   end
end 
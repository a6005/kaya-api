# frozen_string_literal: true

class App
  hash_routes('/api/v1').on 'users' do |r|
    i_dir = 'users'
    i_name = 'user'
    i_reset_password_dir = 'reset_password'
    i_reset_password_name = i_reset_password_dir
    i_password_history_dir = 'password_history'
    i_password_history_name = i_password_history_dir
    i_user_email_dir = 'user_emails'
    i_user_email_name = 'user_email'

    r.on Integer, 'emails' do |id|
      r.get do
        r.halt *run_interaction(i_user_email_dir, "search_#{i_user_email_name}", path_params: {id: id})
      end
      r.post do
        r.halt *run_interaction(i_user_email_dir, "create_#{i_user_email_name}", path_params: {id: id})
      end
      r.delete do
        r.halt *run_interaction(i_user_email_dir, "delete_#{i_user_email_name}", path_params: {id: id})
      end
    end

    r.on Integer, 'reset_password' do |id|
      r.post do
        r.halt *run_interaction(i_reset_password_dir, "create_#{i_reset_password_name}", path_params: {id: id})
      end
    end
    
    r.on Integer, 'password_history' do |id|
      r.get do
        r.halt *run_interaction(i_password_history_dir, "get_#{i_password_history_name}", path_params: {id: id})
      end
    end

    r.is Integer do |id|
      r.get do
        r.halt *run_interaction(i_dir, "get_#{i_name}", path_params: {id: id})
      end
      r.put do
        r.halt *run_interaction(i_dir, "update_#{i_name}", path_params: {id: id})
      end
      r.delete do
        r.halt *run_interaction(i_dir, "delete_#{i_name}", path_params: {id: id})
      end
    end
    r.post do
      r.halt *run_interaction(i_dir, "create_#{i_name}")
    end
    r.get do
      r.halt *run_interaction(i_dir, "search_#{i_name}")
    end
  end
end
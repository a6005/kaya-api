# frozen_string_literal: true

class App
  hash_routes('/api/v1').on 'email_lists' do |r|
    i_dir = 'email_lists'
    i_name = 'email_list'
    i_member_dir = 'email_list_members'
    i_member_name = 'email_list_member'

    r.on Integer, 'members' do |id|
      r.get Integer do |email_id|
        r.halt *run_interaction(i_member_dir, "get_#{i_member_name}", path_params: {id: id, email_id: email_id})
      end
      r.get do
        r.halt *run_interaction(i_member_dir, "search_#{i_member_name}", path_params: {id: id})
      end
      r.put Integer do |email_id|
        r.halt *run_interaction(i_member_dir, "update_#{i_member_name}", path_params: {id: id, email_id: email_id})
      end
      r.delete Integer do |email_id|
        r.halt *run_interaction(i_member_dir, "delete_#{i_member_name}", path_params: {id: id, email_id: email_id})
      end
      r.post do
        r.halt *run_interaction(i_member_dir, "create_#{i_member_name}", path_params: {id: id})
      end
    end

    r.is Integer do |id|
      r.get do
        r.halt *run_interaction(i_dir, "get_#{i_name}", path_params: {id: id})
      end
      r.put do
        r.halt *run_interaction(i_dir, "update_#{i_name}", path_params: {id: id})
      end
      r.delete do
        r.halt *run_interaction(i_dir, "delete_#{i_name}", path_params: {id: id})
      end
    end
    r.post do
      r.halt *run_interaction(i_dir, "create_#{i_name}")
    end
    r.get do
      r.halt *run_interaction(i_dir, "search_#{i_name}")
    end
  end
end
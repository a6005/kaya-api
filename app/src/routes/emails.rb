# frozen_string_literal: true

class App
  hash_routes('/api/v1').on 'emails' do |r|
    i_dir = 'emails'
    i_name = 'email' 
    r.is Integer do |id|
      r.get do
        r.halt *run_interaction(i_dir, "get_#{i_name}", path_params: {id: id})
      end
      r.put do
        r.halt *run_interaction(i_dir, "update_#{i_name}", path_params: {id: id})
      end
      r.delete do
        r.halt *run_interaction(i_dir, "delete_#{i_name}", path_params: {id: id})
      end
    end
    r.post do
      r.halt *run_interaction(i_dir, "create_#{i_name}")
    end
    r.get do
      r.halt *run_interaction(i_dir, "search_#{i_name}")
    end
  end
end
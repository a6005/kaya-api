# frozen_string_literal: true

class InteractionRunner
  attr_accessor  :request, :interaction_dir, :interaction_name, :options, :interaction, :request
  
  def initialize(i_dir, i_name, r, o={})
    @interaction_dir = i_dir
    @interaction_name = i_name
    @request = r
    @options = o
    require INTERACTIONS_PATH.join(i_dir, i_name + '.rb')
    @interaction = Kernel.const_get("Interactions::#{i_name.camelize}")
  end

  def run
    return [KAYA_HTTP[:internal_server_error], {message: KAYA_CODE[:db_connection_error]}.to_json] unless check_db_connection
    begin
      # generate hash while downcasing keys
      headers = request.env.inject({}){|acc, (k,v)| acc[$1.downcase] = v if k =~ /^http_(.*)/i; acc}
      APP_LOGGER.debug interaction.name
      APP_LOGGER.debug "Headers: #{headers}"
      APP_LOGGER.debug "query params: #{request.params}"
      APP_LOGGER.debug "path params: #{options[:path_params]}"
      body = request.body.read
      body_params = body.empty? ? nil : JSON.parse(body).symbolize_keys
      out = interaction.run(authenticate: options[:authenticate],
                            path_params: options[:path_params],
                            query_params: request.params,
                            body_params: body_params,
                            headers: headers)
      if out.valid?
        out.result
      else
        APP_LOGGER.debug 'run_interaction: out is not valid => BAD REQUEST'
        APP_LOGGER.debug out.errors
        APP_LOGGER.debug "errors http_code: #{out.errors.first.options[:http_code]}"
        http_code = out.errors.first.options[:http_code] || KAYA_HTTP[:bad_request]
        [http_code, {message: out.errors.full_messages.to_sentence}.to_json]
      end
    rescue => e
      APP_LOGGER.error e.class
      APP_LOGGER.error "\t #{e.backtrace.join("\n\t ")}"
      [KAYA_HTTP[:bad_request], {message: e.message}.to_json]
    end
  end

  private
  def check_db_connection
    begin
      init_check_db
    rescue => e
      begin
        APP_LOGGER.debug 'check_db_connection: DB connection is down! Trying to connect...'
        init_check_db
        APP_LOGGER.debug 'check_db_connection: initialized...'
      rescue Sequel::DatabaseConnectionError => e
        APP_LOGGER.debug 'check_db_connection: DB connection failed! Giving up...'
        return false
      end
    end
    return true
  end
  
  def init_check_db
    APP_LOGGER.debug 'init_check_db: Initializing db connection...'
    require INITIALIZERS_PATH.join('db.rb')
    # DB["select datetime('now');"].first
    DB[:companies].first
  end
end
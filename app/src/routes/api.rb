routes = %w[companies departments domains emails email_lists email_providers employee_types languages pipelines
  sequrity_groups users]
routes.each { |x| require_relative x }

class App
  hash_routes.on 'api' do |r|
    r.hash_routes
  end
  hash_routes('/api').on 'v1' do |r|
    r.hash_routes
  end
  hash_routes('/api/v1').on 'authentication' do |r|
    r.post do
      r.halt *run_interaction('authentication', 'create_authentication', authenticate: false)
    end
  end
  hash_routes('/api/v1').on 'definition' do |r|
    r.get do
      ::Apigen::Formats::OpenAPI::V3.generate(API)
    end
  end
end
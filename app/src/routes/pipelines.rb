# frozen_string_literal: true

class App
  hash_routes("/#{API_PATH_PREFIX}").on 'pipelines' do |r|
    i_dir = 'pipelines'
    i_name = 'pipeline'

    i_job_dir = 'jobs'
    i_job_name = 'job'

    r.on Integer, 'jobs' do |id|
      r.on Integer do |job_id|
        r.get do
          r.halt *run_interaction(i_job_dir, "get_#{i_job_name}", path_params: {id: id, job_id: job_id})
        end
        r.put do
          r.halt *run_interaction(i_job_dir, "update_#{i_job_name}", path_params: {id: id, job_id: job_id})
        end
      end

      r.get do
        r.halt *run_interaction(i_job_dir, "search_#{i_job_name}", path_params: {id: id})
      end  
    end

    r.is Integer do |id|
      r.get do
        r.halt *run_interaction(i_dir, "get_#{i_name}", path_params: {id: id})
      end
    end

    r.get do
      r.halt *run_interaction(i_dir, "search_#{i_name}")
    end     
  end
end 
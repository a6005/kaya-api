# frozen_string_literal: true

require 'roda'
%w[application.rb authentication.rb google.rb openldap.rb job_dispatcher.rb yandex.rb].each do |f|
  require HELPERS_PATH.join(f).to_s
end
Dir[INITIALIZERS_PATH.join('*.rb')].each { |file| 
    f = File.basename(file)
    require file unless f == 'db.rb'
}
require WORKERS_PATH.join('workers.rb')
require API_DEF_PATH

class App < Roda
  include ::Helpers::Application
  include ::Helpers::Authentication
  current_user_id = -1
    
  opts[:check_dynamic_arity] = false
  opts[:check_arity] = :warn
  opts[:root] = APP_ROOT
  
  plugin :all_verbs
  use Rack::Cors do 
    allowed_methods = %i[get post put delete options head]
    allow do
      origins '*'
      resource '*', headers: :any, methods: allowed_methods
    end
  end

  plugin :default_headers,
    'Content-Type' => 'application/json',
    # 'Strict-Transport-Security'=>'max-age=16070400;', # Uncomment if only allowing https:// access
    'X-Frame-Options' => 'deny',
    'X-Content-Type-Options' => 'nosniff',
    'X-XSS-Protection' => '1; mode=block'

  plugin :content_security_policy do |csp|
    csp.default_src :none
    csp.style_src :self
    csp.form_action :self
    csp.script_src :self
    csp.connect_src :self
    csp.base_uri :none
    csp.frame_ancestors :none
  end

  plugin :common_logger, $stdout
  
  if ENVIRONMENT == 'development'
    plugin :exception_page
    class RodaRequest
      def assets
        exception_page_assets
        super
      end
    end
  end
  
  plugin :halt
  plugin :sessions, key: '_App.session',
    #cookie_options: {secure: ENV['RACK_ENV'] != 'test'}, # Uncomment if only allowing https:// access
    secret: APP_SECRET
  # plugin :basic_auth # bearer authentication will be used
  plugin :basic_auth
  plugin :hash_routes
 
  require_relative 'routes/api'
  route do |r|
    r.on 'resque' do
      require 'resque/server'
      require MODELS_PATH.join('models.rb')

      r.basic_auth do |email, password| 
        c_user = User.find(email: email, admin: true)
        if c_user
          r.run Resque::Server.new
        else
          r.halt(*k_response(http: :unauthorized, kaya: :unauthorized))
        end
      end
      #headers = request.env.inject({}) { |acc, (k, v)| acc[$1.downcase] = v if k =~ /^http_(.*)/i; acc }
      #c_user = current_user(headers[:authorization]) 
      #if c_user && c_user.admin
      #  r.run Resque::Server.new
      #else
      #  r.halt(*k_response(http: :unauthorized, kaya: :unauthorized))
      #end
    end
    r.root do
      r.redirect "#{API_PATH_PREFIX}/definition"
    end
    r.hash_routes
  end

  def init_check_db
      APP_LOGGER.debug 'App:init_check_db: Initializing db connection...'
      require INITIALIZERS_PATH.join('db.rb')
      # DB["select datetime('now');"].first
      DB[:companies].first
  end

  def check_db_connection
    begin
      init_check_db
    rescue => e
      begin
        APP_LOGGER.debug 'App:check_db_connection: DB connection is down! Trying to connect...'
        init_check_db
        APP_LOGGER.debug 'App:check_db_connection: initialized...'
      rescue Sequel::DatabaseConnectionError => e
        APP_LOGGER.debug 'check_db_connection: DB connection failed! Giving up...'
        return false
      end
    end
    return true
  end

  def migrate_db
    APP_LOGGER.debug "db migrated file: #{DB_MIGRATED_FILE}"
    return true if File.file?(DB_MIGRATED_FILE)
    APP_LOGGER.debug "creating db migrated file: #{DB_MIGRATED_FILE}"
    if system('rake db:migrate')
      return false unless File.write(DB_MIGRATED_FILE, '')
    end
    APP_LOGGER.debug "created db migrated file: #{DB_MIGRATED_FILE}"
    true
  end
  
  def check_start_resque
    return true if File.file?(RESQUE_PID_FILE)

    r_client = Redis.new(url: REDIS_URL)
    begin
      r_client.info
    rescue Redis::CannotConnectError => e
      APP_LOGGER.error "App:check_start_resque: Could not connect to Redis server! error: #{e.message}" 
      return false
    end

    q = 'email_list_google,email_list_yandex,reset_password_ldap,reset_password_google,reset_password_yandex,sync_groups_ldap,sync_users_google,sync_users_yandex,sync_users_ldap'
    cmd_str = "PIDFILE=#{RESQUE_PID_FILE} BACKGROUND=yes QUEUES=#{q} rake resque:all"
    APP_LOGGER.debug "cmd_str: #{cmd_str}"
    system(cmd_str)
    # check if realy started
    File.file?(RESQUE_PID_FILE)
  end

  def run_interaction(interaction_dir, interaction_name, options = {})
    # return [KAYA_HTTP[:internal_server_error], { message: KAYA_CODE[:db_connection_error] }.to_json] unless check_db_connection
    return k_response(http: :internal_server_error, kaya: :db_connection_error) unless check_db_connection
    migrate_db

    require MODELS_PATH.join('models.rb')
    
    return k_response(http: :internal_server_error, kaya: :resque_start_error) unless check_start_resque
    require INTERACTIONS_PATH.join('base.rb')
    require INTERACTIONS_PATH.join(interaction_dir, interaction_name + '.rb')
    interaction = Kernel.const_get("Interactions::#{interaction_name.camelize}")
    begin
      # generate hash while downcasing keys
      headers = request.env.inject({}) { |acc, (k,v)| acc[$1.downcase] = v if k =~ /^http_(.*)/i; acc }
      APP_LOGGER.debug "App:run_interaction: #{interaction.name}"
      APP_LOGGER.debug "App:run_interaction: headers: #{headers}"
      APP_LOGGER.debug "App:run_interaction: query params: #{request.params}"
      APP_LOGGER.debug "App:run_interaction: path params: #{options[:path_params]}"
      body = request.body.read
			body_params = body.empty? ? nil : JSON.parse(body).symbolize_keys
      out = interaction.run(authenticate: options[:authenticate],
                            path_params: options[:path_params],
                            query_params: request.params,
                            body_params: body_params,
                            headers: headers)
      if out.valid?
        APP_LOGGER.debug 'App:run_interaction: out is valid'
        APP_LOGGER.debug "App:run_interaction: out.result: #{out.result}"
        out.result
      else
        APP_LOGGER.debug 'App:run_interaction: out is not valid => BAD REQUEST VERY BAD'
        APP_LOGGER.debug out.errors
        http_code = out.errors.first.options[:http_code] || :bad_request
        kaya_code = out.errors.first.options[:kaya_code] || :bad_request
        APP_LOGGER.debug "App:run_interaction: http_code: #{http_code} kaya_code: #{kaya_code}"
        k_response http: http_code.to_sym, kaya: kaya_code.to_sym
      end
    rescue => e
      APP_LOGGER.debug 'App:run_interaction: rescue SOMETHING UNKNOWN IS HAPPENING'
      APP_LOGGER.error "App:run_interaction: #{e.class}"
      APP_LOGGER.error "App:run_interaction: #{e.backtrace.join("\n\t ")}"
      k_response http: :bad_request, kaya: :bad_request, error: e
    end
  end
end
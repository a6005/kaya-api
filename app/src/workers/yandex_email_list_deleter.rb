# frozen_string_literal: true

class YandexEmailListDeleter
  @queue = :email_list_yandex

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    sleep 2
    j.run
    json = j.data
    APP_LOGGER.debug "YandexEmailListDeleter:perform: job id: #{job_id}"
    APP_LOGGER.debug "YandexEmailListDeleter:perform: json : #{json}"
    if delete_email_list(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.delete_email_list(json)
    return false unless KAYA_DOMAIN_TOKENS.key?(json['domain'])
    ::Helpers::Yandex.delete_email_list(KAYA_DOMAIN_TOKENS[json['domain']][:token], json['domain'], json['email'])
  end  
end
# frozen_string_literal: true

class LdapUserCreator
  @queue = :sync_users_ldap

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    sleep 2
    j.run
    json = j.data
    APP_LOGGER.debug "LdapUserCreator:perform: job id: #{job_id}"
    APP_LOGGER.debug "LdapUserCreator:perform: json : #{json}"
    if create_user(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.create_user(json)
    x = User.find(id: json['user_id'])
    return false if x.nil?

    ::Helpers::OpenLDAP.sync_users(KAYA_LDAP[x.company.code], x.ldap_hash)
  end  
end
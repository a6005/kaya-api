# frozen_string_literal: true

class LdapPasswordResetter
  @queue = :reset_password_ldap
  
  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "LdapPasswordResetter:perform: job id: #{job_id}"
    APP_LOGGER.debug "LdapPasswordResetter:perform: json : #{json}"
    if reset_password(json)
     j.complete
    else
     j.fail
    end 
  end
    
  def self.reset_password(json)
    APP_LOGGER.debug "LdapPasswordResetter:reset_password: json: #{json}"
    APP_LOGGER.debug "LdapPasswordResetter:reset_password: user id: #{json['user_id']}"
    x = User.find(id: json['user_id'])
    return false if x.nil?
    ::Helpers::OpenLDAP.reset_password(KAYA_LDAP[x.company.code], x.os_username, x.password_hash)
  end  
end
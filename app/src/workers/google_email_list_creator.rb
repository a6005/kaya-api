# frozen_string_literal: true

class GoogleEmailListCreator
  @queue = :email_list_google

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "GoogleEmailListCreator:perform: job id: #{job_id}"
    APP_LOGGER.debug "GoogleEmailListCreator:perform: json : #{json}"
    if create_email_list(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.create_email_list(json)
    return false unless KAYA_DOMAIN_ADMIN_EMAILS.key?(json['domain'])
    file_name = "credentials_#{json['domain']}.json"
    ::Helpers::Google.create_email_list(file_name,
      KAYA_DOMAIN_ADMIN_EMAILS[json['domain']][:email], json['email'], json['name'], json['description'])
  end  
end
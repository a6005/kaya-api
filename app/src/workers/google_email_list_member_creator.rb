# frozen_string_literal: true

class GoogleEmailListMemberCreator
  @queue = :email_list_google

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "GoogleEmailListMemberCreator:perform: job id: #{job_id}"
    APP_LOGGER.debug "GoogleEmailListMemberCreator:perform: json : #{json}"
    if add_email_list_member(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.add_email_list_member(json)
    return false unless KAYA_DOMAIN_ADMIN_EMAILS.key?(json['domain'])
    file_name = "credentials_#{json['domain']}.json"
    ::Helpers::Google.add_email_list_member(file_name,
      KAYA_DOMAIN_ADMIN_EMAILS[json['domain']][:email],
      json['email_list_email'], json['email'])
  end  
end
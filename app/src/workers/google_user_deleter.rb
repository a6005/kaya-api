# frozen_string_literal: true

class GoogleUserDeleter
  @queue = :sync_users_google

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "GoogleUserDeleter:perform: job id: #{job_id}"
    APP_LOGGER.debug "GoogleUserDeleter:perform: json : #{json}"
    if delete_user(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.delete_user(json)
    return false unless KAYA_DOMAIN_ADMIN_EMAILS.key?(json['domain'])
    file_name = "credentials_#{json['domain']}.json"
    ::Helpers::Google.delete_user(file_name,
      KAYA_DOMAIN_ADMIN_EMAILS[json['domain']][:email], json['email'])
  end  
end
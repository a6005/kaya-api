workers = %w[
    google_user_creator.rb google_user_deleter.rb google_user_disabler.rb google_user_updater.rb 
    google_email_list_creator.rb google_email_list_deleter.rb google_password_resetter.rb
    google_email_list_member_creator.rb google_email_list_member_deleter.rb google_email_list_member_updater.rb
    mail_message_mailer.rb ldap_password_resetter.rb ldap_security_group_deleter.rb
    ldap_security_group_updater.rb ldap_user_creator.rb ldap_user_deleter.rb ldap_user_updater.rb
    yandex_email_list_creator.rb yandex_email_list_deleter.rb yandex_password_resetter.rb
    yandex_email_list_member_creator.rb yandex_email_list_member_deleter.rb yandex_email_list_member_updater.rb
    yandex_user_creator.rb yandex_user_deleter.rb yandex_user_disabler.rb yandex_user_updater.rb 
]
workers.each {|file| require SRC_PATH.join('workers', file).to_s}
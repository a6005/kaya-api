# frozen_string_literal: true

class YandexEmailListMemberCreator
  @queue = :email_list_yandex

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    sleep 2
    j.run
    json = j.data
    APP_LOGGER.debug "YandexEmailListMemberCreator:perform: job id: #{job_id}"
    APP_LOGGER.debug "YandexEmailListMemberCreator:perform: json : #{json}"
    if add_email_list_member(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.add_email_list_member(json)
    return false unless KAYA_DOMAIN_TOKENS.key?(json['domain'])
    ::Helpers::Yandex.add_email_list_member(KAYA_DOMAIN_TOKENS[json['domain']][:token], json['domain'],
      json['email_list_email'], json['email'], json['can_send_on_behalf'])
  end  
end
# frozen_string_literal: true

class LdapSecurityGroupUpdater
  @queue = :sync_groups_ldap
  
  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "LdapSecurityGroupUpdater:perform: job id: #{job_id}"
    APP_LOGGER.debug "LdapSecurityGroupUpdater:perform: json : #{json}"
    if update_group(json)
     j.complete
    else
     j.fail
    end 
  end
    
  def self.update_group(json)
    APP_LOGGER.debug "LdapSecurityGroupUpdater:update_group_: json: #{json}"
    APP_LOGGER.debug "LdapSecurityGroupUpdater:update_group: security_group id: #{json['security_group_id']}"
    x = SecurityGroup.find(id: json['security_group_id'])
    return false if x.nil?

    ::Helpers::OpenLDAP.sync_groups(KAYA_LDAP[x.company.code], x.ldap_hash)
  end  
end
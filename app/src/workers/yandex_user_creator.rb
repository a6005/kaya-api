# frozen_string_literal: true

class YandexUserCreator
  @queue = :sync_users_yandex

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "YandexUserCreator:perform: job id: #{job_id}"
    APP_LOGGER.debug "YandexUserCreator:perform: json : #{json}"
    if create_user(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.create_user(json)
    return false unless KAYA_DOMAIN_TOKENS.key?(json['domain'])
    ::Helpers::Yandex.create_user(KAYA_DOMAIN_TOKENS[json['domain']][:token],
      json['domain'], json['email'], json['password'], json['name'], json['surname'])
  end  
end
# frozen_string_literal: true

class LdapSecurityGroupDeleter
  @queue = :sync_groups_ldap
  
  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "LdapSecurityGroupDeleter:perform: job id: #{job_id}"
    APP_LOGGER.debug "LdapSecurityGroupDeleter:perform: json : #{json}"
    if delete_group(json)
     j.complete
    else
     j.fail
    end 
  end
    
  def self.delete_group(json)
    APP_LOGGER.debug "LdapSecurityGroupDeleter:delete_group: json: #{json}"
    APP_LOGGER.debug "LdapSecurityGroupDeleter:delete_group: group name: #{json['group_name']}"
    ::Helpers::OpenLDAP.delete_group(KAYA_LDAP[json['company_code']], json['group_name'])
  end  
end
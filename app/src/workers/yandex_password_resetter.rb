# frozen_string_literal: true

class YandexPasswordResetter
  @queue = :reset_password_yandex
  
  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "YandexPasswordResetter:perform: job id: #{job_id}"
    APP_LOGGER.debug "YandexPasswordResetter:perform: json : #{json}"
    if reset_password(json)
      j.complete
    else
      j.fail
    end 
  end
    
  def self.reset_password(json)
    return false unless KAYA_DOMAIN_TOKENS.key?(json['domain'])
    ::Helpers::Yandex.reset_password(KAYA_DOMAIN_TOKENS[json['domain']][:token],
      json['domain'], json['email'], json['password'])
  end  
end
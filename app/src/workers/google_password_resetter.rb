# frozen_string_literal: true

class GooglePasswordResetter
  @queue = :reset_password_google
  
  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "GooglePasswordResetter:perform: job id: #{job_id}"
    APP_LOGGER.debug "GooglePasswordResetter:perform: json : #{json}"
    if reset_password(json)
      j.complete
    else
      j.fail
    end 
  end
    
  def self.reset_password(json)
    return false unless KAYA_DOMAIN_ADMIN_EMAILS.key?(json['domain'])
    file_name = "credentials_#{json['domain']}.json"
    ::Helpers::Google.reset_password(file_name,
      KAYA_DOMAIN_ADMIN_EMAILS[json['domain']][:email],
      json['email'], json['password'])
  end  
end
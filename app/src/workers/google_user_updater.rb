# frozen_string_literal: true

class GoogleUserUpdater
  @queue = :sync_users_yandex

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "GoogleUserUpdater:perform: job id: #{job_id}"
    APP_LOGGER.debug "GoogleUserUpdater:perform: json : #{json}"
    if update_user(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.update_user(json)
    return false unless KAYA_DOMAIN_ADMIN_EMAILS.key?(json['domain'])
    file_name = "credentials_#{json['domain']}.json"
    ::Helpers::Google.update_user(file_name,
      KAYA_DOMAIN_ADMIN_EMAILS[json['domain']][:email],
      json['email'], json['name'], json['surname'], json['enabled'])
  end  
end
# frozen_string_literal: true

class LdapUserUpdater
  @queue = :sync_users_ldap
  
  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "LdapUserUpdater:perform: job id: #{job_id}"
    APP_LOGGER.debug "LdapUserUpdater:perform: json : #{json}"
    if update_user(json)
     j.complete
    else
     j.fail
    end 
  end
    
  def self.update_user(json)
    APP_LOGGER.debug "LdapUserUpdater:update_user: json: #{json}"
    APP_LOGGER.debug "LdapUserUpdater:update_user: user id: #{json['user_id']}"
    x = User.find(id: json['user_id'])
    return false if x.nil?

    ::Helpers::OpenLDAP.sync_users(KAYA_LDAP[x.company.code], x.ldap_hash)
  end  
end
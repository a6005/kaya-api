class MailMessageMailer
	@queue = "mails".to_sym
	def self.perform(id)
		begin
		message = MailMessage.find(id: id)
		template = Template.find(id: message.tmid)
		company = message.company
		
		# Server Details
		Mail.defaults do
			delivery_method :smtp, address: company.smtp ,domain: company.dmin, port: company.port
		end
		
		# Create Mail
		mail = Mail.new
		mail.charset = 'UTF-8'
		mail.content_transfer_encoding = '8bit'
		mail.subject = message.subj
		mail.from    = message.company.frml 
		mail.to      = message.trcp
		mail.cc		 = message.cccp if message.cccp
		mail.bcc     = message.bccp if message.bccp
		if template
			mail.html_part do
				content_type 'text/html; charset=UTF-8'
				body  template.render(:html,message.data)
			end
		else	
			mail.body = message.txtd.to_s
		end
		message.data_objects.each do |obj|
			mail.add_file(filename: obj.get_meta("file_name").mval,content: obj.read)
		end
		message.this.update(stat: MailMessage.enums[:stat][:sent])
		mail.deliver!
		##If successfull
		rescue => e 
			APP_LOGGER.error "\t #{e.backtrace.join("\n\t ")}"
			message.this.update(erms: "#{e.class}: #{e.message}",stat: MailMessage.enums[:stat][:failed])
		end
	end
end
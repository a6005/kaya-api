# frozen_string_literal: true

class YandexUserUpdater
  @queue = :sync_users_yandex

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    j.run
    json = j.data
    APP_LOGGER.debug "YandexUserUpdater:perform: job id: #{job_id}"
    APP_LOGGER.debug "YandexUserUpdater:perform: json : #{json}"
    if update_user(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.update_user(json)
    return false unless KAYA_DOMAIN_TOKENS.key?(json['domain'])
    ::Helpers::Yandex.update_user(KAYA_DOMAIN_TOKENS[json['domain']][:token], json['domain'],
      json['email'], json['name'], json['surname'], json['enabled'])
  end  
end
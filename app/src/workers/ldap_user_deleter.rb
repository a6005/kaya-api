# frozen_string_literal: true

class LdapUserDeleter
  @queue = :sync_users_ldap

  def self.perform(job_id)
    j = Job.find id: job_id
    raise KAYA_CODE[:invalid_job_id] unless j
    sleep 2
    j.run
    json = j.data
    APP_LOGGER.debug "LdapUserDeleter:perform: job id: #{job_id}"
    APP_LOGGER.debug "LdapUserDeleter:perform: json : #{json}"
    if delete_user(json)
      j.complete
    else
      j.fail
    end 
  end
  
  def self.delete_user(json)
    APP_LOGGER.debug "LdapUserDeleter:delete_user: json: #{json}"
    APP_LOGGER.debug "LdapUserDeleter:delete_user: username: #{json['username']}"
    ::Helpers::OpenLDAP.delete_user(KAYA_LDAP[json['company_code']], json['username'])
  end  
end
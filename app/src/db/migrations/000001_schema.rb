Sequel.migration do
  up do
    create_table 'companies' do
      primary_key :id
      String      :name, null: false
      String      :code, null: false
      #Blob       :logo  
      index       :name, unique: true
      index       :code, unique: true
    end
    create_table 'email_providers' do
      primary_key :id
      String    :name, null: false
      Boolean   :implemented, default: false
      Boolean   :allow_external, default: false
      index     :name, unique: true
    end
    create_table 'domains' do
      primary_key :id
      foreign_key :company_id, :companies, on_delete: :restrict, null:  false
      foreign_key :email_provider_id, :email_providers, null: false
      String      :name, null: false
      String      :description, null: false
      Boolean     :sync_enabled, default: false
      index       :name, unique: true
    end
    create_table 'security_groups' do
      primary_key :id
      foreign_key :company_id, :companies, on_delete: :restrict, null: false
      Integer :os_gid, null: false
      String :name, null: false
      String :description, null: false
      index [:company_id, :os_gid], unique: true
      index [:company_id, :name], unique: true
    end
    create_table 'email_lists' do
      primary_key :id
      foreign_key :company_id, :companies, on_delete: :restrict, null: false
      foreign_key :domain_id, :domains, on_delete: :restrict, null: false
      String :name, null: false
      String :description, null: false
      String :email, null: false
      String :list_id
      index [:company_id, :domain_id, :email], unique: true
      index [:company_id, :domain_id, :name], unique: true
    end
    create_table 'emails' do
      primary_key :id
      String      :email, null: false
      Boolean     :confirmed, default: false
      index       [:email], unique: true
    end
    create_table 'email_list_members' do
      foreign_key :email_list_id, :email_lists, on_delete: :restrict, null: false
      foreign_key :email_id, :emails, on_delete: :restrict, null: false
      Boolean     :owner, default: false
      Boolean     :can_send_on_behalf, default: false
      primary_key [:email_list_id, :email_id]
      index       [:email_id, :email_list_id], unique: true
    end
    create_table 'employee_types' do
      primary_key :id
      foreign_key :company_id, :companies, on_delete: :restrict, null: false
      String      :name, null: false
      index       [:company_id, :name], unique: true
    end
    create_table 'departments' do
      primary_key :id
      foreign_key :company_id, :companies, on_delete: :restrict, null: false
      String      :name, null: false
      index       [:company_id, :name], unique: true
    end
    create_table 'languages' do
      primary_key :id
      String      :language_code, fixed: true, size: 2, null: false
      String      :name, null: false
      String      :name_in_english, null: false
    end
    create_table 'users' do
      primary_key   :id
      foreign_key   :company_id, :companies, on_delete: :restrict, null: false
      Integer       :os_uid, null: false # operating system user id
      String        :os_username, null: false # operating system username
      Boolean       :admin, default: false
      Boolean       :enabled, default: true
      Boolean       :has_ldap, default: true
      Boolean       :has_email, default: true
      foreign_key   :department_id, :departments, on_delete: :restrict, null: false
      foreign_key   :security_group_id, :security_groups, on_delete: :restrict, null: false
      foreign_key   :employee_type_id, :employee_types, on_delete: :restrict, null: false
      String        :employee_number, null: false
      String        :name, null: false
      String        :surname, null: false
      String        :initials, null: false
      String        :password_hash, null: false
      String        :login_shell, null: false
      String        :home_dir, null: false
      String        :email, null: false
      String        :title, null: false
      String        :room_number, null: false
      String        :phone, null: false
      String        :fax, null: false
      Integer       :mindays, null: false
      Integer       :maxdays, null: false
      Integer       :warndays, null: false
      Timestamp     :create_time
      Timestamp     :update_time
      Timestamp     :disable_time
      Timestamp     :password_change_time
      String        :reset_password_token
      Timestamp     :reset_password_token_ctime
      foreign_key   :language_id, :languages, on_delete: :restrict, null: false
      index         [:company_id, :os_uid], unique: true
      index         [:company_id, :os_username], unique: true
      index         [:company_id, :name, :surname], unique: false
      index         [:company_id, :surname, :name], unique: false
      index         [:company_id, :home_dir], unique: true
      index         :email, unique: true
    end
    create_table 'security_group_members' do
      foreign_key   :security_group_id, :security_groups, on_delete: :restrict, null: false
      foreign_key   :user_id, :users, on_delete: :restrict, null: false
      Boolean       :owner, default: false
      primary_key   [:security_group_id, :user_id]
      index         [:user_id, :security_group_id], unique: true
    end
    create_table 'user_emails' do
      foreign_key   :user_id, :users, on_delete: :restrict, null: false
      foreign_key   :email_id, :emails, on_delete: :restrict, null: false
      primary_key   [:user_id, :email_id]
    end
    create_table 'password_history' do
      foreign_key :user_id, :users, on_delete: :restrict, null: false
      Timestamp :password_change_time, null: false
      String :password_hash, null: false
      String :reverse_password_hash, null: false
      foreign_key :changed_by_user_id, :users, on_delete: :restrict, null: false
      primary_key [:user_id, :password_change_time]
      index [:user_id, :password_hash], unique: true
      index [:user_id, :reverse_password_hash], unique: true
    end
    create_table('roles') do
      primary_key  :id
      foreign_key  :company_id, :companies, on_delete: :restrict, null: false
      String    :name, null: false
      Boolean    :admin, default: false
      index    :name, unique: true
      index    [:company_id, :id], unique: true
    end
    create_table('authorizations') do
      primary_key :id
      foreign_key :company_id, :companies, on_delete: :restrict, null: false
      foreign_key :role_id, :roles, on_delete: :restrict, null: false
      String      :service, null: false
      String      :action, null: false
      Boolean     :authorized, null: false, default: false
      index       :service
      index       :role_id
      index       [:company_id, :id], unique: true
    end
    create_table('users_roles') do
      foreign_key  :user_id, :users, on_delete: :restrict, null: false
      foreign_key  :role_id, :roles, on_delete: :restrict, null: false
      primary_key  [:user_id, :role_id], unique: true
    end
    create_table('pipelines') do
      primary_key   :id
      String        :name, null: false
      foreign_key   :user_id, :users, on_delete: :restrict, null: false
      Integer       :status # job.status => :created, :running, :failed, :completed, :canceled
      Timestamp     :start_time
      Timestamp     :finish_time
      index         [:user_id, :name, :id], unique: true
    end
    create_table('jobs') do
      primary_key  :id
      foreign_key  :pipeline_id, :pipelines, on_delete: :restrict, null: false
      Integer      :status  # job.status => :created, :running, :failed, :completed, :canceled
      String       :worker, null: false
      Timestamp    :create_time
      Timestamp    :enqueue_time
      Timestamp    :start_time
      Timestamp    :finish_time
      Json         :data
      index        [:pipeline_id, :id], unique: true
    end
  end

  down do
    drop_table(:jobs)
    drop_table(:pipelines)
    drop_table(:users_roles)
    drop_table(:authorizations)
    drop_table(:roles)
    drop_table(:password_history)
    drop_table(:user_emails)
    drop_table(:security_group_members)
    drop_table(:email_list_members)
    drop_table(:users)
    drop_table(:departments)
    drop_table(:employee_types)
    drop_table(:email_lists)
    drop_table(:emails)
    drop_table(:security_groups)
    drop_table(:domains)
    drop_table(:email_providers)
    drop_table(:companies)
    drop_table(:languages)
  end
end

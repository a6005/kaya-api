#!/usr/bin/ruby -w

# Initialize
Dir[SRC_PATH.join('lib','*.rb')].each { |file| require file }
# Initialize applicatiion 
Dir[SRC_PATH.join('models', 'models.rb')].each { |file| require file }

TEST_SEED_DIR = SRC_PATH.join('db', 'seeds', 'seed_alt')
# field seperator in seed files
FS = "@@@"

[['IÇS Grubu', 'ics']].each do |name, code|
  Company.create(name: name, code: code)
end

company_id = 1
[
  [ 'google', false, true ],
  [ 'yandex', false, false ]
].each do |name, implemented, allow_external|
  EmailProvider.create(name: name, implemented: implemented, allow_external: allow_external)
end

[
  ['altiniplik.com.tr', 'yandex', 'ALT'],
  ['ics-group.com.tr', 'google', 'İÇS'],
  ['elanbilisim.com.tr', 'google', 'ELAN Google'],
  ['elanbilisim.com', 'yandex', 'ELAN Yandex'],
  ['iristekstil.com.tr', 'google', 'İris'],
  ['zagoracorap.com', 'google', 'Zagora'],
].each do |domain, provider, description, implemented, allow_external|
  Domain.create(company_id: company_id, email_provider_id: EmailProvider.find(name: provider).id,
    name: domain, description: description, sync_enabled: false)
end

[ 'Elan', 'IT', 'ICS' ].each do |name|
  Department.create(company_id: company_id, name: name)
end

File.open(TEST_SEED_DIR.join('groups.csv')).each do |line|
  line.chomp!
  SecurityGroup.create(company_id: company_id, os_gid: line.split(FS)[0].to_i, name: line.split(FS)[1], description: line.split(FS)[1])
end

['Fulltime', 'Parttime', 'Trainee'].each do |name| EmployeeType.create(company_id: company_id, name: name) end

[
	['tr', 'Türkçe', 'Turkish'],
	['en', 'English', 'English'],
	['bg', 'Български', 'Bulgarian']
].each do |language_code, name, name_in_english|
	Language.create(language_code: language_code, name: name, name_in_english: name_in_english)
end

language_id = 1

File.open(TEST_SEED_DIR.join('users.csv')).each do |line|
  line.chomp!
  os_uid = line.split(FS)[0].to_i
  os_username = line.split(FS)[1]
  enabled = line.split(FS)[2] == 't' ? true : false
  admin = line.split(FS)[3] == 't' ? true : false
  has_ldap = line.split(FS)[20] == 't' ? true : false
  has_email = line.split(FS)[23] == 't' ? true : false
  department_id = 1
  security_group_id = line.split(FS)[5].to_i
  employee_type_id = 1
  employee_number = 0
  name = line.split(FS)[8]
  surname = line.split(FS)[9]
  initials = (name[0] + surname[0]).upcase
  password_hash = line.split(FS)[11]
  login_shell = line.split(FS)[12]
  home_dir = line.split(FS)[13]
  email = line.split(FS)[14]
  title = line.split(FS)[15]
  room_number = 1
  phone = '555 444 33'
  fax = phone
  mindays = MIN_PASSWORD_DAYS
  maxdays = MAX_PASSWORD_DAYS
  warndays = WARN_PASSWORD_DAYS
  create_time = Time.now
  update_time = create_time
  password_change_time = create_time
  # puts "user: #{os_username} os_id: #{os_id} email: #{email} password_hash: #{password_hash}"
  begin
    DB[:users].insert(company_id: company_id, os_uid: os_uid, os_username: os_username, admin: admin, enabled: enabled,
      has_ldap: has_ldap, has_email: has_email,
      department_id: department_id, security_group_id: SecurityGroup.find(os_gid: security_group_id).id,
      employee_type_id: employee_type_id, employee_number: employee_number,
      name: name, surname: surname, initials: initials,
      password_hash: password_hash, login_shell: login_shell, home_dir: home_dir,
      email: email, title: title, room_number: room_number, phone: phone, fax: fax,
      mindays: mindays, maxdays: maxdays, warndays: warndays,
      create_time: create_time,update_time: update_time, password_change_time: password_change_time,
      language_id: language_id)
  rescue
    puts "could not insert user: #{os_username} os_uid: #{os_uid} email: #{email}"
    puts "#{e.message}"
    puts "\t #{e.backtrace.join("\n\t ")}"
  end
end

current_user = User.find(id: 1)

File.open(TEST_SEED_DIR.join('groupuser.csv')).each do |line|
  line.chomp!
  x = SecurityGroupMember.new()
  x.security_group_id = SecurityGroup.find(company_id: company_id, os_gid: line.split(FS)[0].to_i).id
  x.user_id = User.find(company_id: company_id, os_uid: line.split(FS)[1].to_i).id
  x.current_user = current_user
  x.save
end
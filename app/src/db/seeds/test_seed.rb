# frozen_string_literal: true

TEST_SEED_DIR = SRC_PATH.join('db', 'seeds', 'test_seed')
# field seperator in seed files
FS = "@@@"

company_code = 'acme'
Company.create(name: 'ACME Inc', code: company_code)

company_id = 1
[
  [ 'google', false, true ],
  [ 'yandex', false, false ]
].each do |name, implemented, allow_external|
  EmailProvider.create(name: name, implemented: implemented, allow_external: allow_external)
end

[
  ['example.com', 'yandex', 'Test domain on Yandex'],
  ['example.org', 'google', 'Test domain on google']
].each do |domain, provider, description, implemented, allow_external|
  Domain.create(company_id: company_id, email_provider_id: EmailProvider.find(name: provider).id,
    name: domain, description: description, sync_enabled: false)
end

[ 'IT', 'Sales', 'Finance' ].each do |name|
  Department.create(company_id: company_id, name: name)
end

[
  ['info', 'Email list for info', 'info@example.com', '111111'],
  ['support', 'Email list for support', 'support@example.com', '2222222'],
  ['info', 'Email list for info', 'info@example.org', '333333'],
  ['support', 'Email list for support', 'support@example.org', '44444']
].each do |name, description, email, list_id|
  domain = email.split('@').last
  EmailList.insert(company_id: company_id, domain_id: Domain.find(name: domain).id,
    name: name, description: description, email: email, list_id: list_id)
end

[
  [ 'test1@example.net', true],
  [ 'test1@example.de', true],
  [ 'test2@example.net', false],
  [ 'test2@example.de', false]
].each do |email, confirmed|
  Email.create(email: email, confirmed: confirmed)
end

Email.where(confirmed: true).all.each do |email|
  EmailList.all.each do |email_list|
    EmailListMember.insert(email_list_id: email_list.id, email_id: email.id)
  end
end

File.open(TEST_SEED_DIR.join('groups.csv')).each do |line|
  line.chomp!
  SecurityGroup.insert(company_id: company_id, os_gid: line.split(FS)[0].to_i, name: line.split(FS)[1], description: line.split(FS)[1])
end

['Fulltime', 'Parttime', 'Trainee'].each do |name| EmployeeType.create(company_id: company_id, name: name) end

[
	['tr', 'Türkçe', 'Turkish'],
	['en', 'English', 'English'],
	['bg', 'Български', 'Bulgarian']
].each do |language_code, name, name_in_english|
	Language.create(language_code: language_code, name: name, name_in_english: name_in_english)
end

language_id = 1

#File.open(TEST_SEED_DIR.join('users.csv')).each do |line|
#  line.chomp!
#  begin
#    u = User.new()
#    u.company_id = company_id
#    u.os_uid = line.split(FS)[0].to_i
#    u.os_username = line.split(FS)[1]
#    u.enabled = line.split(FS)[2] == 't' ? true : false
#    u.admin = line.split(FS)[3] == 't' ? true : false
#    u.has_ldap = line.split(FS)[20] == 't' ? true : false
#    u.has_email = line.split(FS)[23] == 't' ? true : false
#    u.department_id = 1
#    u.security_group_id = SecurityGroup.find(os_gid: line.split(FS)[5].to_i).id
#    u.employee_type_id = 1
#    u.employee_number = 1
#    u.name = line.split(FS)[8]
#    u.surname = line.split(FS)[9]
#    u.initials = (u.name[0] + u.surname[0]).upcase
#    # u.password_hash = line.split(FS)[11]
#    u.password = 'deneme123654'
#    u.password_confirmation = u.password
#    u.login_shell = line.split(FS)[12]
#    u.home_dir = line.split(FS)[13]
#    u.email = line.split(FS)[14]
#    u.title = line.split(FS)[15]
#    u.room_number = 1
#    u.phone = '555 444 33 22'
#    u.fax = '555 444 33 22'
#    u.mindays = MIN_PASSWORD_DAYS
#    u.maxdays = MAX_PASSWORD_DAYS
#    u.warndays = WARN_PASSWORD_DAYS
#    u.create_time = Time.now
#    u.update_time = u.create_time
#    u.password_change_time = u.create_time
#    u.language_id = language_id
#    u.save
#  rescue => e
#    puts "Error: could not create user: #{u.os_username} os_uid: #{u.os_uid} email: #{u.email}"
#    puts "#{e.message}"
#    puts "\t #{e.backtrace.join("\n\t ")}"
#  end
#end

File.open(TEST_SEED_DIR.join('users.csv')).each do |line|
  line.chomp!
  os_uid = line.split(FS)[0].to_i
  os_username = line.split(FS)[1]
  enabled = line.split(FS)[2] == 't' ? true : false
  admin = line.split(FS)[3] == 't' ? true : false
  has_ldap = line.split(FS)[20] == 't' ? true : false
  has_email = line.split(FS)[23] == 't' ? true : false
  department_id = 1
  security_group_id = line.split(FS)[5].to_i
  employee_type_id = 1
  employee_number = 0
  name = line.split(FS)[8]
  surname = line.split(FS)[9]
  initials = (name[0] + surname[0]).upcase
  password_hash = line.split(FS)[11]
  login_shell = line.split(FS)[12]
  home_dir = line.split(FS)[13]
  email = line.split(FS)[14]
  unless has_email
    email = "#{os_username}@#{company_code}"
  end
  title = line.split(FS)[15]
  room_number = 1
  phone = '555 444 33'
  fax = phone
  mindays = MIN_PASSWORD_DAYS
  maxdays = MAX_PASSWORD_DAYS
  warndays = WARN_PASSWORD_DAYS
  create_time = Time.now
  update_time = create_time
  password_change_time = create_time
  # puts "user: #{os_username} os_uid: #{os_uid} email: #{email} has_email: #{has_email} password_hash: #{password_hash}"
  begin
    DB[:users].insert(company_id: company_id, os_uid: os_uid, os_username: os_username, admin: admin, enabled: enabled,
      has_ldap: has_ldap, has_email: has_email,
      department_id: department_id, security_group_id: SecurityGroup.find(os_gid: security_group_id).id,
      employee_type_id: employee_type_id, employee_number: employee_number,
      name: name, surname: surname, initials: initials,
      password_hash: password_hash, login_shell: login_shell, home_dir: home_dir,
      email: email, title: title, room_number: room_number, phone: phone, fax: fax,
      mindays: mindays, maxdays: maxdays, warndays: warndays,
      create_time: create_time,update_time: update_time, password_change_time: password_change_time,
      language_id: language_id)
    if has_email
      Email.insert(email: email, confirmed: true)
    end
  rescue
    puts "could not insert user: #{os_username} os_uid: #{os_uid} email: #{email}"
    puts "#{e.message}"
    puts "\t #{e.backtrace.join("\n\t ")}"
  end
end

# change passwords
admin_pw = 'admin123654'
test_pw = 'test123654'
admin_user_id = 1
# change admin passwprd
u = User.find(id: admin_user_id)
u.current_user = u
u.change_password admin_pw, admin_pw, admin_user_id
# change test account passwords
for i in 2..4
  u = User.find(id: i)
  u.current_user = u
  u.change_password test_pw, test_pw, admin_user_id
end

current_user = User.find(id: 1)

File.open(TEST_SEED_DIR.join('groupuser.csv')).each do |line|
  line.chomp!
  x = SecurityGroupMember.new()
  x.security_group_id = SecurityGroup.find(company_id: company_id, os_gid: line.split(FS)[0].to_i).id
  x.user_id = User.find(company_id: company_id, os_uid: line.split(FS)[1].to_i).id
  x.current_user = current_user
  x.save
end
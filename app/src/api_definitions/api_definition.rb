# frozen_string_literal: true

require 'apigen/rest'
require 'apigen/formats/openapi'
require INITIALIZERS_PATH.join('http_status_codes.rb')
require_relative 'common_definitions'

API = Apigen::Rest::Api.new
API.title 'KAYA API'
API.description 'API for user administration'
VERSION = 'version is not set' unless defined?(VERSION)
API.version VERSION
API.contact do
    name 'Erdal Mutlu & Fatih Genç'
    email 'info@sisiya.org'
    url 'https://sisiya.org'
end
API.server "#{API_URL}/#{API_PATH_PREFIX}"
API.security_schemes << Security::TYPE

# End point definitions
endpoints = %w[authentication company ldap_group ldap_user department domain email email_list email_list_member
  email_provider employee_type job language pipeline security_group security_group_member
  user user_email reset_password password_history]

endpoints.each { |x| require_relative "endpoints/#{x}"}
endpoints.each { |x| require_relative "models/#{x}"}
require_relative 'models/message'
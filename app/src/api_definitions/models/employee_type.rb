# frozen_string_literal: true

API.model :employee_type_object do
  description 'Create Employee Type Request Object'
  type :object do
    company_id(:int32).explain do
      description "Company ID"
      example "1"
    end

    name(:string).explain do
      description "Employee Type Name"
      example "Fulltime"
    end
  end
end

API.model :employee_type_update_object do
  description 'Employee Type Update Request Object'
  type :object do
    name(:string).explain do
      description "Employee Type Name"
      example "Fulltime"
    end
  end
end

API.model :employee_type_response_object do
  description 'Employee Type Response Object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    company_id(:int32).explain do
      description "Company ID"
      example "1"
    end

    name(:string).explain do
      description "Employee Type Name"
      example "Fulltime"
    end
  end
end

API.model :employee_type_response_object_array do
  description 'Employee array response object'
  type :array do
    type :employee_type_response_object
  end
end
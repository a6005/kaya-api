# frozen_string_literal: true

API.model :reset_password_object do
  description 'Reset Password Request'
  type :object do
    current_password(:string?).explain do
      description "The current password of the user (mandatory for normal users)"
      example "test123654"
    end
    password(:string).explain do
      description "New password"
      example "test123456"
    end
    password_confirmation(:string).explain do
      description "New password confirmation"
      example "test123456"
    end
  end
end
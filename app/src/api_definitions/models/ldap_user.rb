# frozen_string_literal: true

API.model :ldap_user_response_object do
  description 'LDAP user response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    username(:string).explain do
      description 'User name'
      example 'test'
    end

    name(:string).explain do
      description 'Name'
      example 'Test User'
    end

    group_id(:int32).explain do
      description 'Group ID'
      example '1'
    end

    home_dir(:string).explain do
      description 'Home directory'
      example '/home/test'
    end
  end
end

API.model :ldap_user_response_object_array do
  description 'LDAP user array response object'
  type :array do
    type :ldap_user_response_object
  end
end
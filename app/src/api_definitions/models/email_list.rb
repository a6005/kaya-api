# frozen_string_literal: true

email_list_name = 'Email list name'
email_list_desc = 'Email list description'
email_list_example = 'Email list for info'
email_list_id = 'Email list ID'

API.model :email_list_object do
  description 'Create Email List Request Object'
  type :object do
    company_id(:int32).explain do
      description 'Company ID'
      example '1'
    end

    domain_id(:int32).explain do
      description 'Domain ID'
      example '1'
    end

    name(:string).explain do
      description email_list_name
      example 'info'
    end

    description(:string).explain do
      description email_list_desc
      example email_list_example
    end

    email(:string).explain do
      description 'Email'
      example 'info@example.com'
    end

    list_id(:string).explain do
      description email_list_id
      example '615237651273'
    end
  end
end

API.model :email_list_update_object do
  description 'Update email list request object'
  type :object do
    name(:string).explain do
      description email_list_name
      example 'info'
    end

    description(:string).explain do
      description email_list_desc
      example email_list_example
    end

    email(:string).explain do
      description 'Email'
      example 'info@example.com'
    end

    list_id(:string).explain do
      description email_list_id
      example '615237651273'
    end
  end
end

API.model :email_list_response_object do
  description 'Email List Response Object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    company_id(:int32).explain do
      description 'Company ID'
      example '1'
    end

    domain_id(:int32).explain do
      description 'Domain ID'
      example '1'
    end

    name(:string).explain do
      description email_list_name
      example 'info'
    end

    description(:string).explain do
      description email_list_desc
      example email_list_example
    end

    email(:string).explain do
      description 'Email'
      example 'info@example.com'
    end

    list_id(:string).explain do
      description email_list_id
      example '615237651273'
    end
  end
end

API.model :email_list_response_object_array do
  description 'Email list array response object'
  type :array do
    type :email_list_response_object
  end
end
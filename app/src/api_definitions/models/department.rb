API.model :department_object do
  description 'Create Department Request Object'
  type :object do
    company_id(:int32).explain do
      description "Company ID"
      example "1"
    end

    name(:string).explain do
      description "Department Name"
      example "IT"
    end
  end
end

API.model :department_update_object do
  description 'Department Update Request Object'
  type :object do
    name(:string).explain do
      description "Department Name"
      example "IT"
    end
  end
end

API.model :department_response_object do
  description 'Department response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end
    company_id(:int32).explain do
      description 'Company ID'
      example '1'
    end
    name(:string).explain do
      description 'Department Name'
      example 'IT'
    end
  end
end

API.model :department_response_object_array do
  description 'Department array response object'
  type :array do
    type :department_response_object
  end
end
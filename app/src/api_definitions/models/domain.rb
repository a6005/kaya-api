# frozen_string_literal: true

domain_name_desc = domain_name_desc
sync_enabled_desc = 'Allow synchronization'
domain_name_desc = 'Domain name'

API.model :domain_object do
  description 'Create Domain Request Object'
  type :object do
    company_id(:int32).explain do
      description 'Company ID'
      example '1'
    end

    email_provider_id(:int32).explain do
      description 'Email provider ID'
      example '1'
    end

    name(:string).explain do
      description domain_name_desc
      example 'example.com'
    end

    description(:string).explain do
      description 'Domain name description'
      example 'This is an example domain'
    end

    sync_enabled(:bool).explain do
      description sync_enabled_desc
      example 'false'
    end
  end
end

API.model :domain_update_object do
  description 'Update Domain Request Object'
  type :object do
    name(:string).explain do
      description domain_name_desc
      example 'example.org'
    end

    description(:string).explain do
      description 'Domain name description'
      example 'This is an example domain'
    end

    sync_enabled(:bool).explain do
      description sync_enabled_desc
      example 'false'
    end
  end
end

API.model :domain_response_object do
  description 'Domain response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    company_id(:int32).explain do
      description 'Company ID'
      example '1'
    end

    email_provider_id(:int32).explain do
      description 'Email provider ID'
      example '1'
    end

    name(:string).explain do
      description domain_name_desc
      example 'example.net'
    end

    description(:string).explain do
      description domain_name_desc
      example 'example.net'
    end
    
    sync_enabled(:bool).explain do
      description sync_enabled_desc
      example 'false'
    end
  end
end

API.model :domain_response_object_array do
  description 'Domain array response object'
  type :array do
    type :domain_response_object
  end
end
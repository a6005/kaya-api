# frozen_string_literal: true

API.model :security_group_object do
  description 'Create Security Group Request Object'
  type :object do
    company_id(:int32).explain do
      description "Company ID"
      example "1"
    end

    os_gid(:int32).explain do
      description "Operating System Group ID"
      example "1000"
    end

    name(:string).explain do
      description "Security Group Name"
      example "users"
    end

    description(:string).explain do
      description "Security Group Description"
      example "Security groups for users"
    end
  end
end

API.model :security_group_update_object do
  description 'Security Group Request Object'
  type :object do
    os_gid(:int32).explain do
      description "Operating System Group ID"
      example "1000"
    end

    name(:string).explain do
      description "Security Group Name"
      example "users"
    end

    description(:string).explain do
      description "Security Group Description"
      example "Security groups for users"
    end
  end
end

API.model :security_group_response_object do
  description 'Security Group Response Object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    company_id(:int32).explain do
      description "Company ID"
      example "1"
    end

    os_gid(:int32).explain do
      description "Operating System Group ID"
      example "1000"
    end

    name(:string).explain do
      description "Security Group Name"
      example "users"
    end

    description(:string).explain do
      description "Security Group Description"
      example "Security groups for users"
    end
  end
end

API.model :security_group_response_object_array do
  description 'Security group array response object'
  type :array do
    type :security_group_response_object
  end
end
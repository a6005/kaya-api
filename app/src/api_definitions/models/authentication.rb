# frozen_string_literal: true

API.model :authentication_object do
  description 'Authentication Request'
  type :object do
    email(:string).explain do
      description "Email address of a user"
      example "test@example.com"
    end
    password(:string).explain do
      description "Password of a user"
      example "test123654"
    end
    expire_period(:string?).explain do
      description "Expire period"
      example ""
    end
  end
end

API.model :authentication_response_object do
  description "Authentication Response Object."
  type :object do
    token(:string).explain do
      description 'Bearer token'
      example 'AeyJhbGciOiJIUzUxMiJ9.eyJuYW1lIjoiVZ'
    end
  end
end
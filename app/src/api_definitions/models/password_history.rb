# frozen_string_literal: true

API.model :password_history_object_array do
  description 'Password History Request'
  type :array do
    type :object do
      user_id(:int32).explain do
        description "User ID"
        example "1001"
      end
      password_change_time(:string?).explain do
        description "The current password of the user (mandatory for normal users)"
        example "1970-12-31 23:45:20"
      end
      changed_by_user_id(:int32).explain do
        description "Password changed by User ID"
        example "1001"
      end
    end
  end
end
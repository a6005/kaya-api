API.model :message do
  description 'Message Response Object'
  type :object do
    message(:string).explain do
      description 'The message'
      example 'Some message'
    end
  end
end
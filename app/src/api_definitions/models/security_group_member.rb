# frozen_string_literal: true

security_group_owner = 'Security group owner'

API.model :security_group_member_object do
  description 'Create Security Group Member Request Object'
  type :object do
    user_id(:int32).explain do
      description 'User ID'
      example '1000'
    end

    owner(:bool).explain do
      description security_group_owner
      example 'false'
    end
  end
end

API.model :security_group_member_update_object do
  description 'Update Security Group Member Request Object'
  type :object do
    owner(:bool).explain do
      description security_group_owner
      example 'true'
    end
  end
end

API.model :security_group_member_response_object do
  description 'Security Group Member Response Object'
  type :object do
    security_group_id(:int32).explain do
      description 'Security group ID'
      example '1'
    end

    user_id(:int32).explain do
      description 'User ID'
      example '1000'
    end

    owner(:bool).explain do
      description security_group_owner
      example 'false'
    end
  end
end

API.model :security_group_member_response_object_array do
  description 'Security group member array response object'
  type :array do
    type :security_group_member_response_object
  end
end
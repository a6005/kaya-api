# frozen_string_literal: true

API.model :user_object do
  description 'Create User Request Object'
  type :object do
    company_id(:int32).explain do
      description "Company ID"
      example "1"
    end
    
    os_uid(:int32).explain do
      description "Operating system user ID"
      example "5001"
    end

    os_username(:string).explain do
      description "Operating system user name"
      example "testuser"
    end

    admin(:bool).explain do
      description "Admin user"
      example "false"
    end

    enabled(:bool).explain do
      description "Enable or disable user"
      example "true"
    end

    has_ldap(:bool).explain do
      description "Is user LDAP enabled"
      example "true"
    end

    has_email(:bool).explain do
      description "Does user have email"
      example "true"
    end
    
    department_id(:int32).explain do
      description "Department ID"
      example "1"
    end

    security_group_id(:int32).explain do
      description "Security group ID of user"
      example "1"
    end

    employee_type_id(:int32).explain do
      description "Employee type ID"
      example "1"
    end

    employee_number(:string).explain do
      description "Employee number"
      example "1213"
    end

    name(:string).explain do
      description "Name"
      example "Test"
    end

    surname(:string).explain do
      description "Surname"
      example "User"
    end

    initials(:string).explain do
      description "Initials"
      example "TU"
    end

    password(:string).explain do
      description "Password"
      example "test123654"
    end

    password_confirmation(:string).explain do
      description "Password confirmation"
      example "test123654"
    end

    login_shell(:string).explain do
      description "Login shell of user"
      example "/bin/bash"
    end

    home_dir(:string).explain do
      description "Home directory of user"
      example "/home/testuser"
    end

    email(:string).explain do
      description "Email of user"
      example "test.user@example.com"
    end

    title(:string).explain do
      description "Title of user"
      example "Tester"
    end

    room_number(:string).explain do
      description "Room number of user"
      example "R102"
    end

    phone(:string).explain do
      description "Phone number of user"
      example "+90 535 555 55 55"
    end

    fax(:string).explain do
      description "Fax number of user"
      example "+90 212 285 55 55"
    end

    mindays(:int32).explain do
      description "Minimum number of days between password changes.  A value of zero for this field indicates that the user may change his/her password at any time"
      example "0"
    end

    maxdays(:int32).explain do
      description "Set the maximum number of days during which a password is valid. Setting to -1 will remove checking a password's validity."
      example "-1"
    end

    warndays(:int32).explain do
      description "Number of days of warning before a password change is required."
      example "365"
    end

    language_id(:int32).explain do
      description "Language ID"
      example "1"
    end
  end
end

API.model :user_update_object do
  description 'Update User Request Object'
  type :object do
    os_uid(:int32?).explain do
      description "Operating system user ID"
      example "5001"
    end

    os_username(:string?).explain do
      description "Operating system user name"
      example "testuser"
    end

    admin(:bool?).explain do
      description "Admin user"
      example "false"
    end

    enabled(:bool?).explain do
      description "Enable or disable user"
      example "true"
    end

    has_ldap(:bool?).explain do
      description "Is user LDAP enabled"
      example "true"
    end

    has_email(:bool?).explain do
      description "Does user have email"
      example "true"
    end
    
    department_id(:int32?).explain do
      description "Department ID"
      example "1"
    end

    security_group_id(:int32?).explain do
      description "Security group ID of user"
      example "1"
    end

    employee_type_id(:int32?).explain do
      description "Employee type ID"
      example "1"
    end

    employee_number(:string?).explain do
      description "Employee number"
      example "1213"
    end

    name(:string?).explain do
      description "Name"
      example "Test"
    end

    surname(:string?).explain do
      description "Surname"
      example "User"
    end

    initials(:string?).explain do
      description "Initials"
      example "TU"
    end

   # password(:string).explain do
   #   description "Password"
   #   example "test123654"
   # end

   # password_confirmation(:string).explain do
   #   description "Password confirmation"
   #   example "test123654"
   # end

    login_shell(:string?).explain do
      description "Login shell of user"
      example "/bin/bash"
    end

    home_dir(:string?).explain do
      description "Home directory of user"
      example "/home/testuser"
    end

    email(:string?).explain do
      description "Email of user"
      example "test.user@example.com"
    end

    title(:string?).explain do
      description "Title of user"
      example "Tester"
    end

    room_number(:string?).explain do
      description "Room number of user"
      example "R102"
    end

    phone(:string?).explain do
      description "Phone number of user"
      example "+90 535 555 55 55"
    end

    fax(:string?).explain do
      description "Fax number of user"
      example "+90 212 285 55 55"
    end

    mindays(:int32?).explain do
      description "Minimum number of days between password changes.  A value of zero for this field indicates that the user may change his/her password at any time"
      example "0"
    end

    maxdays(:int32?).explain do
      description "Set the maximum number of days during which a password is valid. Setting to -1 will remove checking a password's validity."
      example "-1"
    end

    warndays(:int32?).explain do
      description "Number of days of warning before a password change is required."
      example "365"
    end

    language_id(:int32?).explain do
      description "Language ID"
      example "1"
    end
  end
end

API.model :user_response_object do
  description 'User Response Object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    company_id(:int32).explain do
      description "Company ID"
      example "1"
    end
    
    os_uid(:int32).explain do
      description "Operating system user ID"
      example "5001"
    end

    os_username(:string).explain do
      description "Operating system user name"
      example "testuser"
    end

    admin(:bool).explain do
      description "Admin user"
      example "false"
    end

    enabled(:bool).explain do
      description "Enable or disable user"
      example "true"
    end

    has_ldap(:bool).explain do
      description "Is user LDAP enabled"
      example "true"
    end

    has_email(:bool).explain do
      description "Does user have email"
      example "true"
    end
    
    department_id(:int32).explain do
      description "Department ID"
      example "1"
    end

    security_group_id(:int32).explain do
      description "Security group ID of user"
      example "1"
    end

    employee_type_id(:int32).explain do
      description "Employee type ID"
      example "1"
    end

    employee_number(:string).explain do
      description "Employee number"
      example "1213"
    end

    name(:string).explain do
      description "Name"
      example "Test"
    end

    surname(:string).explain do
      description "Surname"
      example "User"
    end

    initials(:string).explain do
      description "Initials"
      example "TU"
    end

    password(:string).explain do
      description "Password"
      example "test123654"
    end

    password_confirmation(:string).explain do
      description "Password confirmation"
      example "test123654"
    end

    login_shell(:string).explain do
      description "Login shell of user"
      example "/bin/bash"
    end

    home_dir(:string).explain do
      description "Home directory of user"
      example "/home/testuser"
    end

    email(:string).explain do
      description "Email of user"
      example "test.user@example.com"
    end

    title(:string).explain do
      description "Title of user"
      example "Tester"
    end

    room_number(:string).explain do
      description "Room number of user"
      example "R102"
    end

    phone(:string).explain do
      description "Phone number of user"
      example "+90 535 555 55 55"
    end

    fax(:string).explain do
      description "Fax number of user"
      example "+90 212 285 55 55"
    end

    mindays(:int32).explain do
      description "Minimum number of days between password changes.  A value of zero for this field indicates that the user may change his/her password at any time"
      example "0"
    end

    maxdays(:int32).explain do
      description "Set the maximum number of days during which a password is valid. Setting to -1 will remove checking a password's validity."
      example "-1"
    end

    warndays(:int32).explain do
      description "Number of days of warning before a password change is required."
      example "365"
    end

    language_id(:int32).explain do
      description "Language ID"
      example "1"
    end
  end
end

API.model :user_response_object_array do
  description 'User array response object'
  type :array do
    type :user_response_object
  end
end

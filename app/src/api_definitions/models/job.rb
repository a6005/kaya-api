# frozen_string_literal: true

time_str = '1971-10-31 23:45:20'

API.model :job_response_object do
  description 'Job Request Object'
  type :object do
    id(:int32).explain do
      description 'Job ID'
      example '1'
    end
    pipeline_id(:int32).explain do
      description 'Pipeline ID'
      example '1'
    end
    worker(:string).explain do
      description 'Worker'
      example 'reset password'
    end
    status(:int32).explain do
      description 'Status'
      example '10'
    end
    create_time(:string).explain do
      description 'Create time'
      example time_str
    end
    enqueue_time(:string).explain do
      description 'Enqueue time'
      example time_str
    end
    start_time(:string).explain do
      description 'Start time'
      example time_str
    end
    finish_time(:string).explain do
      description 'Finish time'
      example time_str
    end
  end
end

API.model :job_update_object do
  description 'Job Update Request Object'
  type :object do
    status(:int32).explain do
      description 'Status'
      example '10'
    end
  end
end

API.model :job_response_object_array do
  description 'Job array response object'
  type :array do
    type :job_response_object
  end
end
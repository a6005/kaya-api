# frozen_string_literal: true

API.model :company_object do
  description 'Create Company Request Object'
  type :object do
    name(:string).explain do
      description 'Company name'
      example 'Acme Inc.'
    end
    
    code(:string).explain do
      description 'Company code'
      example 'acme'
    end
  end
end

API.model :company_response_object do
  description 'Company response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    name(:string).explain do
      description 'Company name'
      example 'ACME Inc.'
    end
    
    code(:string).explain do
      description 'Company code'
      example 'acme'
    end
  end
end

API.model :company_response_object_array do
  description 'Company array response object'
  type :array do
    type :company_response_object
  end
end
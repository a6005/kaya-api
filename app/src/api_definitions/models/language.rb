# frozen_string_literal: true

API.model :language_object do
  description 'Create Language Request Object'
  type :object do
    language_code(:string).explain do
      description "2 letter language code"
      example "tr"
    end

    name(:string).explain do
      description "Language Name"
      example "Türkçe"
    end

    name_in_english(:string).explain do
      description "Language name in English"
      example "Turkish"
    end
  end
end

API.model :language_response_object do
  description 'Create Language Request Object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    language_code(:string).explain do
      description "2 letter language code"
      example "tr"
    end

    name(:string).explain do
      description "Language Name"
      example "Türkçe"
    end

    name_in_english(:string).explain do
      description "Language name in English"
      example "Turkish"
    end
  end
end

API.model :language_response_object_array do
  description 'Language array response object'
  type :array do
    type :language_response_object
  end
end
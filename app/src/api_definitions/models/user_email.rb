# frozen_string_literal: true

user_emails = 'User emails'

API.model :user_email_object do
  description 'Create User Email Request Object'
  type :object do
    email(:string).explain do
      description "Email"
      example "info@example.com"
    end
  end
end

API.model :user_email_response_object do
  description 'User Email Response Object'
  type :object do
    user_id(:int32).explain do
      description "User ID"
      example "1000"
    end

    email(:string).explain do
      description "Email"
      example "me@example.net"
    end

    confirmed(:bool).explain do
      description user_emails
      example "false"
    end
  end
end

API.model :user_email_response_object_array do
  description 'User Email Array Response Object'
  type :array do
    type :user_email_response_object
  end
end
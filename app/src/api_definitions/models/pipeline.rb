# frozen_string_literal: true

time_str = '1971-10-31 23:45:20'

API.model :pipeline_response_object do
  description 'Pipeline Request Object'
  type :object do
    id(:int32).explain do
      description 'Pipeline ID'
      example '1'
    end
    name(:string).explain do
      description 'Pipeline Name'
      example 'reset password'
    end
    user_id(:int32).explain do
      description 'User ID'
      example '1001'
    end
    status(:int32).explain do
      description 'Status'
      example '10'
    end
    start_time(:string).explain do
      description 'Start time'
      example time_str
    end
    finish_time(:string).explain do
      description 'Finish time'
      example time_str
    end
  end
end

API.model :pipeline_response_object_array do
  description 'Pipeline array response object'
  type :array do
    type :pipeline_response_object
  end
end
# frozen_string_literal: true

API.model :username_object do
  description 'username object'
  type :object do
    username(:string).explain do
      description 'Userame'
      example 'test'
    end
  end
end

API.model :ldap_group_response_object do
  description 'LDAP group response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    name(:string).explain do
      description 'Name'
      example 'users'
    end

    description(:string).explain do
      description 'Description'
      example 'users'
    end

    members :array do
      type :username_object
    end
    #type :array do
    #  type :username_object
    #end
  end
end

API.model :ldap_group_response_object_array do
  description 'LDAP group array response object'
  type :array do
    type :ldap_group_response_object
  end
end
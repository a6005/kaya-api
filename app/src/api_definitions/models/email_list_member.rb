# frozen_string_literal: true

email_list_owner = 'Email list owner'

API.model :email_list_member_object do
  description 'Email List Member Request Object'
  type :object do
    email_id(:int32).explain do
      description 'Email ID'
      example '1'
    end

    owner(:bool).explain do
      description email_list_owner
      example 'false'
    end

    can_send_on_behalf(:bool).explain do
      description 'Can send on behalf of the list email'
      example 'false'
    end
  end
end

API.model :email_list_member_update_object do
  description 'Email List Member Update Request Object'
  type :object do
    owner(:bool).explain do
      description email_list_owner
      example 'false'
    end

    can_send_on_behalf(:bool).explain do
      description 'Can send on behalf of the list email'
      example 'false'
    end
  end
end

API.model :email_list_member_response_object do
  description 'Email List Member Response Object'
  type :object do
    email_list_id(:int32).explain do
      description 'Email list ID'
      example '1'
    end

    email_id(:int32).explain do
      description 'Email ID'
      example '1'
    end

    owner(:bool).explain do
      description email_list_owner
      example 'false'
    end
    
    can_send_on_behalf(:bool).explain do
      description 'Can send on behalf of the list email'
      example 'false'
    end
  end
end

API.model :email_list_member_response_object_array do
  description 'Email list member array response object'
  type :array do
    type :email_list_member_response_object
  end
end
# frozen_string_literal: true

API.model :email_provider_object do
  description 'Create Email Provider Request Object'
  type :object do
    name(:string).explain do
      description "Email provider"
      example "Google"
    end

    implemented(:bool).explain do
      description "Implemented"
      example "false"
    end

    allow_external(:bool).explain do
      description "Allow external emails in email lists"
      example "false"
    end
  end
end

API.model :email_provider_response_object do
  description 'Email Provider Response Object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    name(:string).explain do
      description "Email provider"
      example "Google"
    end

    implemented(:bool).explain do
      description "Implemented"
      example "false"
    end

    allow_external(:bool).explain do
      description "Allow external emails in email lists"
      example "false"
    end
  end
end

API.model :email_provider_response_object_array do
  description 'Email provider array response object'
  type :array do
    type :email_provider_response_object
  end
end
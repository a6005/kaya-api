# frozen_string_literal: true

email_desc = 'Email'
confirmed_desc = 'Email confirmed'

API.model :email_object do
  description 'Create Email Request Object'
  type :object do
    email(:string).explain do
      description email_desc
      example 'test1@example.net'
    end
  end
end

API.model :email_response_object do
  description 'Email response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    email(:string).explain do
      description email_desc
      example 'test1@example.net'
    end

    confirmed(:bool).explain do
      description confirmed_desc
      example 'false'
    end
  end
end

API.model :email_response_object_array do
  description 'Email array response object'
  type :array do
    type :email_response_object
  end
end
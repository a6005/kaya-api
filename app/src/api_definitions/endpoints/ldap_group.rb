# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Company LDAP Groups'
endpoint_path = '/companies/{id}/ldap/groups'

API.tag endpoint_tag do
  description "#{endpoint_tag} end point"
end

API.endpoint :update_ldap_groups do
  description 'Synchronize a ldap groups'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_ldap_group do
  description 'Get LDAP group for a company'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :ldap_group_response_object_array
      else
        type :message
      end
    end
  end
end

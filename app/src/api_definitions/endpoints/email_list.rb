# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Email Lists'
endpoint_path = '/email_lists'
endpoint_id_path = '/email_lists/{id}'

API.tag endpoint_tag do
  description 'Email lists end point'
end

API.endpoint :create_email_list do
  description 'Create an email list'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type
  input do
    type :email_list_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :email_list_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_email_list do
  description 'Update an email list'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :email_list_update_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_email_list do
  description 'Get an email list'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :email_list_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_email_list do
  description 'Search email lists'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    domain_id(:int32?).explain do
      description 'Domain ID (optional)'
      example '1'
    end
    name(:string?).explain do
      description 'Email list name (optional)'
      example 'info'
    end
    description(:string?).explain do
      description 'Email list description (optional)'
      example 'This is info list'
    end
    email(:string?).explain do
      description 'Email list (optional)'
      example 'info@example.com'
    end
    list_id(:string?).explain do
      description 'Email list ID (optional)'
      example '5555555555'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :email_list_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_email_list do
  description 'Delete an email list'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
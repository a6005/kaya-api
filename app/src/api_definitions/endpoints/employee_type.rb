# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Employee Types'
endpoint_path = '/employee_types'
endpoint_id_path = '/employee_types/{id}'


API.tag endpoint_tag do
  description 'Companies end point'
end

API.endpoint :create_employee_type do
  description 'Create a employee_type'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type

  input do
    type :employee_type_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :employee_type_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_employee_type do
  description 'Update a employee_type'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :employee_type_update_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_employee_type do
  description 'Get a employee_type'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :employee_type_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_employee_type do
  description 'Search employee_types'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path 
  query do
    name(:string?).explain do
      description 'employee_type name (optional)'
      example 'Acme Inc.'
    end
  end
  
  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :employee_type_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_employee_type do
  description 'Delete a employee_type'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
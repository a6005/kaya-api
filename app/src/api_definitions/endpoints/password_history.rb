# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'User Password History'
endpoint_path = '/users/{id}/password_history'

API.tag endpoint_tag do
  description 'Password history end point'
end

API.endpoint :get_password_history do
  description 'Get password history of an user'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :password_history_object_array
      else
        type :message
      end
    end
  end
end
# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Email Providers'
endpoint_path = '/email_providers'
endpoint_id_path = '/email_providers/{id}'


API.tag endpoint_tag do
  description 'Email provider end point'
end

API.endpoint :create_email_provider do
  description 'Create an email provider'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type

  input do
    type :email_provider_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :email_provider_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_email_provider do
  description 'Update an email provider'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :email_provider_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_email_provider do
  description 'Get an email provider'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :email_provider_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_email_provider do
  description 'Search email providers'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path 
  query do
    name(:string?).explain do
      description 'email_provider name (optional)'
      example 'yandex'
    end
  end
 
  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :email_provider_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_email_provider do
  description 'Delete an email provider'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
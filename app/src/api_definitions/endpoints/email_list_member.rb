# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Email List Members'
endpoint_path = '/email_lists/{id}/members'
endpoint_id_path = '/email_lists/{id}/members/{email_id}'

API.tag endpoint_tag do
  description 'Email list members end point'
end

API.endpoint :create_email_list_member do
  description 'Create an email list member'
  method :post
  tag endpoint_tag
  path endpoint_path do
    id :int32
  end
  security security_type
  input do
    type :email_list_member_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :email_list_member_object
      else
        type :message
      end
    end
  end
 end

API.endpoint :update_email_list_member do
  description 'Update an email list member'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
    email_id :int32
  end

  input do
    type :email_list_member_update_object
  end
  
  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_email_list_member do
  description 'Get email list members'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
    email_id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :email_list_member_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_email_list_member do
  description 'Search email list members'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path do
    id :int32
  end
  query do
    owner(:bool?).explain do
      description "Email list owner"
      example "false"
    end
    can_send_on_behalf(:bool?).explain do
      description "Can send on behalf of the list email"
      example "false"
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :email_list_member_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_email_list_member do
  description 'Delete an email list member'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
    email_id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
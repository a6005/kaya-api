# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Companies'
endpoint_path = '/companies'
endpoint_id_path = '/companies/{id}'

API.tag endpoint_tag do
  description 'Companies end point'
end

API.endpoint :create_company do
  description 'Create a company'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type

  input do
    type :company_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :company_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_company do
  description 'Update a company'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :company_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_company do
  description 'Get a company'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :company_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_company do
  description 'Search companies'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path 
  query do
    name(:string?).explain do
      description 'company name (optional)'
      example 'Acme Inc.'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :company_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_company do
  description 'Delete a company'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Emails'
endpoint_path = '/emails'
endpoint_id_path = '/emails/{id}'

API.tag endpoint_tag do
  description 'Emails end point'
end

API.endpoint :create_email do
  description 'Create a email'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type

  input do
    type :email_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :email_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_email do
  description 'Update a email'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :email_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_email do
  description 'Get a email'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :email_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_email do
  description 'Search emails'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path 
  query do
    email(:string?).explain do
      description 'email name (optional)'
      example 'test1@example.com'
    end
    
    confirmed(:bool?).explain do
      description 'confirmed (optional)'
      example 'true'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :email_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_email do
  description 'Delete a email'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
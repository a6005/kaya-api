# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Departments'
endpoint_path = '/departments'
endpoint_id_path = '/departments/{id}'

API.tag endpoint_tag do
  description 'Companies end point'
end

API.endpoint :create_department do
  description 'Create a department'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type

  input do
    type :department_object
  end
  
  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :department_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_department do
  description 'Update a department'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :department_update_object
  end
  
  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_department do
  description 'Get a department'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :department_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_department do
  description 'Search departments'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path 
  query do
    name(:string?).explain do
      description 'department name (optional)'
      example 'IT'
    end
  end
  
  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :department_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_department do
  description 'Delete a department'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end
  
  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
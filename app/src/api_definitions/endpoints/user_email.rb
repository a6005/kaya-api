# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'User Emails'
endpoint_path = '/users/{id}/emails'

API.tag endpoint_tag do
  description 'User emails endpoint'
end

API.endpoint :create_user_email do
  description 'Add an email for user'
  method :post
  tag endpoint_tag
  path endpoint_path do
    id :int32
  end
  security security_type

  input do
    type :user_email_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :user_email_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_user_email do
  description 'Search user emails'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path do
    id :int32
  end
  query do
    email(:string?).explain do
      description 'email (optional)'
      example 'test@example.com'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :user_email_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_user_email do
  description 'Delete user email'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_path do
    id :int32
  end
  query do
    email(:string).explain do
      description 'email'
      example 'test@example.com'
    end
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
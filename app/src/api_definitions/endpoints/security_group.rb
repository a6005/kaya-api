
# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Security Groups'
endpoint_path = '/security_groups'
endpoint_id_path = '/security_groups/{id}'

API.tag endpoint_tag do
  description 'Security groups end point'
end

API.endpoint :create_security_group do
  description 'Create a security group'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type
  input do
    type :security_group_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :security_group_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_security_group do
  description 'Update a security group'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :security_group_update_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_security_group do
  description 'Get a security group'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :security_group_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_security_group do
  description 'Search security groups list'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    name(:string?).explain do
      description 'Securiity group name (optional)'
      example 'users'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :security_group_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_security_group do
  description 'Delete a security group'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end
 
  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
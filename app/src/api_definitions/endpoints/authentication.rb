# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Authentication'
endpoint_path = '/authentication'

API.tag endpoint_tag do
  description 'Take bearer token'
end

API.endpoint :create_authentication do
  description 'Creates an authentication token'
  method :post
  tag endpoint_tag
  path endpoint_path

  input do
    type :authentication_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :authentication_response_object
      else
        type :message
      end
    end
  end
end
# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'User Reset Password'
endpoint_path = '/users/{id}/reset_password'

API.tag endpoint_tag do
  description 'Reset password of user'
end

API.endpoint :create_reset_password do
  description 'Returns authentication token'
  method :post
  tag endpoint_tag
  path endpoint_path do
    id :int32
  end
  security security_type

  # 1. User is changing his/her password  => current password (optional), new_password and new_password_confirmation
  # 2. Admin is changing users password   => new_password and new_password_confirmation
  input do
    type :reset_password_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
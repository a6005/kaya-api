# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Pipelines'
endpoint_path = '/pipelines'
endpoint_id_path = '/pipelines/{id}'

API.tag endpoint_tag do
  description 'Pipelines end point'
end

API.endpoint :get_pipeline do
  description 'Get a pipeline'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :pipeline_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_pipeline do
  description 'Search pipelines'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path 
  query do
    name(:string?).explain do
      description 'pipeline name (optional)'
      example 'reset_password'
    end
    user_id(:int32?).explain do
      description 'user id (optional)'
      example '1001'
    end
    status(:int32?).explain do
      description 'status (optional)'
      example '10'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :pipeline_response_object_array
      else
        type :message
      end
    end
  end
end
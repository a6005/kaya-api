# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Languages'
endpoint_path = '/languages'
endpoint_id_path = '/languages/{id}'

API.tag endpoint_tag do
  description 'Languages end point'
end

API.endpoint :create_language do
  description 'Create a language'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type
  input do
    type :language_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :language_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_language do
  description 'Update a language'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :language_object
  end
  
  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_language do
  description 'Get a language'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end
  
  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :language_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_language do
  description 'Search languages'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    language_code(:string?).explain do
      description '2 letter language code (optional)'
      example 'tr'
    end
    name(:string?).explain do
      description 'Language name (optional)'
      example 'Türkçe'
    end
    name_in_english(:string?).explain do
      description 'Language name in English (optional)'
      example 'Turkish'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :language_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_language do
  description 'Delete a language'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Users'
endpoint_path = '/users'
endpoint_id_path = '/users/{id}'

API.tag endpoint_tag do
  description 'Users end point'
end

API.endpoint :create_user do
  description 'Create an user'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type
  input do
    type :user_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :user_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_user do
  description 'Update an user'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :user_update_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_user do
  description 'Get an user'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :user_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_user do
  description 'Search list of users'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    os_username(:string?).explain do
      description 'Operating system user name (optional)'
      example 'test'
    end
    name(:string?).explain do
      description 'Name (optional)'
      example 'Test'
    end
    surnamename(:string?).explain do
      description 'Surname (optional)'
      example 'User'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :user_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_user do
  description 'Delete an user'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Jobs'
endpoint_path = '/pipelines/{id}/jobs'
endpoint_id_path = '/pipelines/{id}/jobs/{job_id}'

API.tag endpoint_tag do
  description 'Jobs end point'
end

API.endpoint :update_job do
  description 'Update a job'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
    job_id :int32
  end

  input do
    type :job_update_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_job do
  description 'Get a job'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
    job_id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :job_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_job do
  description 'Search jobs'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path do
    id :int32
  end

  query do
    worker(:string?).explain do
      description 'Worker (optional)'
      example 'reset password'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :job_response_object_array
      else
        type :message
      end
    end
  end
end 
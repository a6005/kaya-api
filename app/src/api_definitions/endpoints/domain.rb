# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Domains'
endpoint_path = '/domains'
endpoint_id_path = '/domains/{id}'


API.tag endpoint_tag do
  description 'Companies end point'
end

API.endpoint :create_domain do
  description 'Create a domain'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type

  input do
    type :domain_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :domain_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_domain do
  description 'Update a domain'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :domain_update_object
  end
  
  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_domain do
  description 'Get a domain'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :domain_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_domain do
  description 'Search domains'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path 
  query do
    name(:string?).explain do
      description 'domain name (optional)'
      example 'example.com'
    end
  end
  
  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :domain_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_domain do
  description 'Delete a domain'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
# frozen_string_literal: true

security_type = Security::TYPE

endpoint_tag = 'Security Group Members'
endpoint_path = '/security_groups/{id}/members'
endpoint_id_path = '/security_groups/{id}/members/{user_id}'

API.tag endpoint_tag do
  description 'Security group members end point'
end

API.endpoint :create_security_group_member do
  description 'Create a security group member'
  method :post
  tag endpoint_tag
  path endpoint_path do
    id :int32
  end
  security security_type
  input do
    type :security_group_member_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :created
        type :security_group_member_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :update_security_group_member do
  description 'Update a security group member'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
    user_id :int32
  end

  input do
    type :security_group_member_update_object
  end
  
  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

API.endpoint :get_security_group_member do
  description 'Get security group members'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
    user_id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :security_group_member_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :search_security_group_member do
  description 'Search security group members'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path do
    id :int32
  end
  query do
    owner(:bool?).explain do
      description 'Security group owner (optional)'
      example 'true'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :ok
        type :security_group_member_response_object_array
      else
        type :message
      end
    end
  end
end

API.endpoint :delete_security_group_member do
  description 'Delete a security group member'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
    user_id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description KAYA_HTTP_DESC[x]
      status KAYA_HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end
# frozen_string_literal: true

class Domain < BaseModel
  many_to_one :email_provider, key: :email_provider_id
  
  def can_sync?
    return self.sync_enabled && self.email_provider.implemented
  end
end
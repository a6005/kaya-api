# frozen_string_literal: true

class PasswordHistory < Sequel::Model(:password_history)
	plugin :sequel_auth, digest_column: :reverse_password_hash,	
		#provider: :crypt, provider_opts: { salt: '$6$by03kVsAXQ1rMBBu' }
		provider: :crypt, provider_opts: { salt: PASSWORD_HISTORY_SALT }
  plugin :validation_helpers

  # many_to_one :user, key: :user_id

  def validate_before_save
    # validates_presence [:user_id, :changed_by_user_id, :password_hash, :reverse_password_hash]
    validates_presence [:user_id, :changed_by_user_id, :password_hash]
  end
    
  def public_values
    values.except(:password_hash, :reverse_password_hash)
  end
   
  # object hooks
  def before_create
    self.password_change_time = Time.now
  end
  
  def after_create
    n = PasswordHistory.where(user_id: user_id).count
	  i = n - PASSWORD_HISTORY_LENGTH 
    if i > 0
      # delete oldest records for this user, keep at most PASSWORD_HISTORY_LENGTH records
      PasswordHistory.where(user_id: user_id).order(:user_id, :password_change_time).first(i).each do |r|
	      r.delete
	    end
    end
  end
end
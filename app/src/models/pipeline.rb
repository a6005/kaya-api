# frozen_string_literal: true

class Pipeline < BaseModel
  plugin :enum
  enum :status, { created: 10, running: 20, canceled: 30, failed: 40, completed: 50}
 
  def update_status
    job_count = Job.where(pipeline_id: self.id).count
    case status
    when  :created
      # If there is a running job then pipeline is running
      j = Job.where(pipeline_id: self.id,
        status: Job.enums[:status][:running]).order_by(:start_time).first
      update(status: :running,start_time: j.start_time) if j
    when :running
      # If all the jobs are finished
      if Job.where(pipeline_id: self.id,status: [10,20,30]).count == 0 
        s = :completed # defalt value
        # if all canceled then pipeline is canceled
        s = :canceled if Job.where(pipeline_id: self.id,status: Job.enums[:status][:canceled]).count ==  job_count
        # if all failed then pipeline is failed
        s = :failed if Job.where(pipeline_id: self.id,status: Job.enums[:status][:failed]).count ==  job_count
        # Find last finished job
        j = Job.where(pipeline_id: self.id).order_by(Sequel.desc(:finish_time)).first
        update(status: s,finish_time: j.finish_time) if j
      end
    end
  end
end
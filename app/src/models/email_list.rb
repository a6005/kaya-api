# frozen_string_literal: true

class EmailList < BaseModel
  many_to_one :company, key: :company_id
  many_to_one :domain, key: :domain_id
  attr_accessor :current_user
  
  def validate_before_save
    check_email
  end

  def after_create
    APP_LOGGER.debug "EmailList:after_create: email list: #{self.name} email: #{self.email}"
    ::Helpers::JobDispatcher.create_email_list(self.current_user.id, self)
  end

  def after_destroy
    APP_LOGGER.debug "EmailList:after_destroy: starting delete_email_list jobs"
    ::Helpers::JobDispatcher.delete_email_list(self.current_user.id, self)
  end

  def check_email
    d = Domain.select(:name, :email_provider_id).where(id: self.domain_id).first
    unless d 
      errors.add(:domain, KAYA_CODE[:domain_not_found])
      return
    end
    x = EmailProvider.select(:allow_external).where(id: d.email_provider_id).first
    
    unless x.allow_external
      email_domain = email.split('@').last
      errors.add(:email, KAYA_CODE[:domain_not_allowed]) unless d.name == email_domain
    end
  end

  def is_google?
    return self.domain.email_provider.name == 'google'
  end

  def is_yandex?
    return self.domain.email_provider.name == 'yandex'
  end
  
 # def email_valid?
 #   d = Domain.select(:name, :email_provider_id).where(id: domain_id).first
 #   return false unless d 
 #   x = EmailProvider.select(:allow_external).where(id: d.email_provider_id).first
 #   unless x.allow_external
 #     email_domain = email.split('@').last
 #     return d.name != email_domain
 #   end
 #   return true
 # end
end
# frozen_string_literal: true

class SecurityGroup < BaseModel
  # one_to_many :security_group_member, key: :security_group_id
  many_to_one :company, key: :company_id
  plugin :after_initialize
  attr_accessor :old_name
  attr_accessor :current_user
  
  def after_initialize
    self.old_name = self.name
    # APP_LOGGER.debug "SecurityGroup:after_initialize: name: #{self.old_name}"
  end
 
  def after_destroy
    APP_LOGGER.debug "SecurityGroup:after_destroy: name: #{self.name}"
    ::Helpers::JobDispatcher.delete_security_group(current_user.id, self.company.code, self.name)
  end
  
  def after_save
    if has_members?
      # start a job to update other systems
      APP_LOGGER.debug "SecurityGroup:after_save: starting update_security_group"
      ::Helpers::JobDispatcher.update_security_group(current_user.id, self)
      if self.old_name != self.name
        # start a job to delete the old record
        ::Helpers::JobDispatcher.delete_security_group(current_user.id, self.company.code, self.old_name)
      end
    else
      APP_LOGGER.debug "SecurityGroup:after_save: security group has no members"
    end
    # super
  end
  
  def has_members?
    begin
      x = User.select(:os_username).where(id: SecurityGroupMember.select(:user_id).where(security_group_id: self.id), has_ldap: true).count
      x > 0 ? true : false
    rescue
      false
    end
  end
  
  def member_usernames
    begin
      x = User.select(:os_username).where(id: SecurityGroupMember.select(:user_id).where(security_group_id: self.id), has_ldap: true).all
      x.map { |k| k.os_username }
    rescue
      false
    end
  end

  def ldap_hash
    # empty memberuid is used to remove the group from ldap
    memberuid = []
    if self.has_members?
      memberuid = self.member_usernames
    end
    
    {
      group: self.name,
      ldap_attributes: {
        cn: self.name,
        objectclass: ['posixGroup'],
        userpassword: 'group123654',
        gidnumber: self.os_gid.to_s,
        description: self.description,
        memberuid: memberuid
      }
    }
  end
end
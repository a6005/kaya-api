# frozen_string_literal: true

class EmailListMember < BaseModel
  many_to_one :email_list, key: :email_list_id
  many_to_one :email, key: :email_id
  attr_accessor :current_user
  
  def after_create
    APP_LOGGER.debug "EmailListMember:after_create: email list: #{self.email_list.name} email: #{self.email.email}"
    ::Helpers::JobDispatcher.add_email_list_member(self.current_user.id, self)
  end

  def after_destroy
    APP_LOGGER.debug "EmailListMemeber:after_destroy: starting delete_email_list jobs"
    ::Helpers::JobDispatcher.remove_email_list_member(self.current_user.id, self)
  end

  def after_update
    APP_LOGGER.debug "EmailListMemeber:after_update: starting update_email_list jobs"
    ::Helpers::JobDispatcher.update_email_list_member(self.current_user.id, self)
  end
end
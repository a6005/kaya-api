# frozen_string_literal: true

class UserEmail < BaseModel

  def validate_before_save
      x = User.find(email: self.email)
      unless x.nil?
        APP_LOGGER.debug "UserEmail:validate_before_save: #{self.email} already used by user: #{x.os_username}"
        errors.add(:email, KAYA_CODE[:email_already_used])
      end
      true 
  end

  def before_create
    self.confirmed = false
  end
end
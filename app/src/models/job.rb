# frozen_string_literal: true

class Job < BaseModel
  plugin :enum
  many_to_one :pipeline
  enum :status, { created: 10, queued: 20, running: 30, canceled: 40, failed: 50, completed: 60}

  # convert JSON data to JSON object
  def data
    JSON.parse(values[:data])
  end

  def public_values
    values.except(:data)
  end
  
  # object hooks
  def before_create
    self.create_time = Time.now
    self.status = :created
    self.data = values[:data].to_json
  end
  
  def validate_before_save
    if self.status == :canceled
      x = Job.find(id: self.id)
      unless x.nil?
        if self.status == :completed || self.status == :failed
          # no way to cancel completed or failed job
          errors.add(:status, KAYA_CODE[:cannot_cancel_job])
        end
      end
    end
  end
 
  def enqueue
    worker = Kernel.const_get(self.worker)
    Resque.enqueue worker, self.id
    update status: :queued, enqueue_time: Time.now, start_time: nil, finish_time: nil
  end
  
  def run
    update status: :running, start_time: Time.now
    pipeline.update_status
  end
  
  def complete
    update status: :completed, finish_time: Time.now
    pipeline.update_status
  end
  
  def fail
    update status: :failed, finish_time: Time.now
    pipeline.update_status
  end

  def finished?
   [:canceled, :failed ,:completed].include? status
  end
end
# frozen_string_literal: true

models = %w[base_model.rb company.rb password_history.rb user.rb domain.rb email.rb email_list.rb
  email_list_member.rb email_provider.rb department.rb job.rb pipeline.rb security_group.rb
  security_group_member.rb employee_type.rb language.rb user_email.rb
]
models.each {|file| require MODELS_PATH.join(file).to_s}
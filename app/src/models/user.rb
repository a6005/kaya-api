# frozen_string_literal: true

class User < BaseModel
  many_to_one :company
  # one_to_many :password_history, key: :user_id
  many_to_one :employee_type
  many_to_one :security_group

  plugin :sequel_auth, digest_column: :password_hash,
    provider: :crypt, provider_opts: { salt_size: SALT_LENGTH }
  plugin :enum
  plugin :after_initialize
  attr_accessor :current_user
  attr_accessor :old_email
  attr_accessor :old_enabled
  attr_accessor :old_has_email
  attr_accessor :old_os_username
  attr_accessor :password_reset
  
  def after_create
    APP_LOGGER.debug "User:after_create: password: #{self.password} password_confirmation: #{self.password_confirmation}"
    ph = PasswordHistory.new(changed_by_user_id: self.id, password_hash: self.password_hash)
	  ph.user_id = self.id
    ph.password = self.password.reverse
    ph.password_confirmation = self.password_confirmation.reverse
    ph.save
    if self.has_email
      Email.create(email: self.email, confirmed: true)
    end
    APP_LOGGER.debug "User:after_create: starting create_user"
    ::Helpers::JobDispatcher.create_user(self.current_user.id, self)
  end

  def after_destroy
    if self.has_email
      Email.find(email: self.email).destroy
    end
    APP_LOGGER.debug "User:after_destroy: starting delete_user jobs"
    ::Helpers::JobDispatcher.delete_user(self.current_user.id, self.company.code, self)
  end

  def after_initialize
    self.password_reset = false
    self.old_os_username = self.os_username
    self.old_email = self.email
    self.old_enabled = self.enabled
    self.old_has_email = self.has_email
    # APP_LOGGER.debug "User:after_initialize: old os_username: #{self.old_os_username} old email: #{self.old_email} old has_email: #{self.old_has_email}, old enabled: #{self.old_enabled}"
  end

  def before_create
    self.create_time = Time.now
  end

  def before_update
    self.update_time = Time.now
    if self.old_enabled && !self.enabled
      self.disable_time = Time.now
    end
    if self.enabled
      self.disable_time = nil
    end
  end

  def after_update
    if self.password_reset
      APP_LOGGER.debug "User:after_update: just resetting password."
      return
    end
    email_changed = self.old_email != self.email
    
    # start a job to update other systems
    APP_LOGGER.debug "User:after_update: starting update_user"
    ::Helpers::JobDispatcher.update_user(self.current_user.id, self, email_changed)
    
    if self.old_os_username != self.os_username
      # start a job to delete the old record
      APP_LOGGER.debug "User:after_update: starting delete_user for old os_username: #{self.old_os_username}"
      # delete users from systems, which have username as a key
      ::Helpers::JobDispatcher.delete_user_by_username(self.current_user.id, self.company.code, self.old_os_username)
    end
    
    if email_changed
      # start a job to disable the old record. Deleting email accounts is dangerous! There is a special
      # tool for listing and if realy needed to delete them.
      APP_LOGGER.debug "User:after_update: starting disable_email for old email: #{self.old_email}"
      # disable email
      ::Helpers::JobDispatcher.disable_email(self.current_user.id, self.company.code, self.old_email)
      # execute create_user just for email systems, that is why the last parameter is true (email_only)
      ::Helpers::JobDispatcher.create_user(self.current_user.id, self, true)
    end

    if self.old_has_email && ! self.has_email
      # start a job to disable the email record.
      APP_LOGGER.debug "User:after_update: email is disabled, starting disable_email for email: #{self.email}"
      # disable email
      ::Helpers::JobDispatcher.disable_email(self.current_user.id, self.company.code, self.email)
    end
  end

  def check_email_domain
    # the domain part of the email must be present in the domains
    # something is wrong here:
    domain_name = self.email.split('@').last
    d = Domain.select(:name, :email_provider_id).where(name: domain_name).first
    unless d 
      errors.add(:domain, KAYA_CODE[:domain_not_found])
      return false
    end
    return true
  end
 
    # password and password_confirmation variables are intance variables
  # defined by attr_accesible, therefore a use short form as method parameters
  def change_password(p, p_confirmation, changed_by_user_id)
    return false unless check_password(p, p_confirmation)
    APP_LOGGER.debug "User:change_password: password: #{p} password_confirmation: #{p_confirmation}"
    self.password_reset = true
    self.password = p
    self.password_confirmation = p_confirmation
    self.password_change_time = Time.now
    # now we have password_hash generated
    ph = PasswordHistory.new(changed_by_user_id: changed_by_user_id, password_hash: self.password_hash)
    ph.user_id = self.id
    ph.password = p.reverse
    ph.password_confirmation = p_confirmation.reverse
    DB.transaction do
      ph.save
      save
    end
    # start a job to reset password on other systems
    ::Helpers::JobDispatcher.reset_password(self, self.password)
    return true
  end

  def check_password(p, p_confirmation)
    # write password rules here
    #if p.empty? || p_confirmation.empty?
    #  return false
    #end
    return true
  end

  def email2
    return Email.find(id: 1).email
  end

  def email_provider_name
    return '' unless self.has_email
    domain_name = self.email.split('@').last
    d = Domain.select(:email_provider_id).where(name: domain_name).first
    return '' unless d
    ep = EmailProvider.select(:name).where(id: d.email_provider_id).first
    return '' unless ep
    return ep.name
  end

  def has_email_provider(name)
    return email_provider_name == name
  end

  def has_google?
    return has_email_provider('google')
  end
  
  def has_ldap?
    self.has_ldap
  end
  
  def has_yandex?
    return has_email_provider('yandex')
  end
 
  def ldap_hash
    return {} unless self.has_ldap?

    {
      user: self.os_username,
      enabled: self.enabled,
      ldap_attributes: {
        objectclass: ['posixAccount', 'shadowAccount', 'inetOrgPerson', 'top'],
        o: self.company.name,
        ou: KAYA_LDAP[self.company.code][:ou_users],
        uid: self.os_username,
        loginshell: self.login_shell,
        homedirectory: self.home_dir,
        gecos: self.os_username,
        mail: self.email,
        userpassword: "{crypt}#{self.password_hash}",
        uidnumber: self.os_uid.to_s,
        gidnumber: self.security_group.os_gid.to_s,
        cn: "#{self.name} #{self.surname}",
        sn: self.surname,
        givenname: self.name,
        initials: self.initials,
        title: self.title,
        telephonenumber: self.phone,
        employeenumber: self.employee_number,
        employeetype: self.employee_type.name,
        roomnumber: self.room_number,
        fax: self.fax,
        labeleduri: API_URL,
        shadowlastchange: self.shadow_last_change.to_s,
        shadowmin: self.mindays.to_s,
        shadowmax: self.maxdays.to_s,
        shadowwarning: self.warndays.to_s
      }
    }
  end

  def public_values
    values.except(:password_hash, :reset_password_token, :reset_password_token_ctime)
  end

  def shadow_last_change
    self.password_change_time.to_i / 86400
  end
  
  def update_ldap
    # start a job to update other systems
    APP_LOGGER.debug "User:update_ldap: starting update_user"
    ::Helpers::JobDispatcher.update_user(current_user.id, self)
  end

  def validate_before_save
    validates_presence [:company_id, :email]
    validates_includes Company.select(:id).map {|e| e.id}, :company_id
    validates_format(URI::MailTo::EMAIL_REGEXP, :email)
    validates_min_length MIN_PASSWORD_LENGTH, :password if password
    validates_format(PASSWORD_REGEX, :password, message: KAYA_CODE[:password_policy]) if password
    # check_email_domain
  end
end
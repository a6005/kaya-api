# frozen_string_literal: true

class SecurityGroupMember < BaseModel
  many_to_one :company, key: :company_id
  many_to_one :security_group, key: :security_group_id
  attr_accessor :current_user

  def after_create
    update_ldap
  end

  def after_destroy
    update_ldap
  end

  def after_save
    update_ldap
  end
  
  def update_ldap
    # start a job to update other systems
    APP_LOGGER.debug "SecurityGroupMember:update_ldap: starting update_security_group"
    ::Helpers::JobDispatcher.update_security_group(current_user.id, self.security_group)
  end
end
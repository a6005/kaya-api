require INITIALIZERS_PATH.join('db.rb')

BaseModel = Class.new(Sequel::Model)

class BaseModel
  plugin :whitelist_security
  plugin :json_serializer
  plugin :timestamps, update_on_create: true, update: :updt, create: :crdt, force: true
  plugin :validify
  plugin :after_initialize
	
#	def before_create
#		unless ["User","Company"].include? self.class.name
#			# self.crid = 
#		end
#	end
#	def before_update
#		unless ["User","Company"].include? self.class.name
#			# self.crid = 
#		end
#	end

end
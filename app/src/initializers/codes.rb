raw_hash = YAML.load_file(LIB_PATH.join('kaya', 'codes.yml'))
h = {}
raw_hash.each do |k, v|
    key = v.split(":").first.to_sym
    value = "#{k}:#{v.split(":").last}"
    h.merge!({key.to_sym => value})
end

KAYA_CODE = h
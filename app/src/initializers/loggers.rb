log_file = File.new("#{LOG_PATH}/#{ENVIRONMENT}_application.log", 'a+')
log_file.sync = true
APP_LOGGER = Logger.new log_file
APP_LOGGER.level = Logger::WARN if ENV['ENVIRONMENT'] == 'development'

# Log stdout while development
#if ENVIRONMENT == 'development'
#    require 'logger'
#    logger = Logger.new($stdout)
#end

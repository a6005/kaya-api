Bundler.require(:db)

db_config = YAML.load(File.read(File.join(DB_PATH, 'database.yml')))[ENVIRONMENT]
db_config['host'] = DB_HOST

DB = Sequel.connect(db_config)
DB.loggers << Logger.new(File.new("#{LOG_PATH}/DB_#{ENVIRONMENT}.log", 'a+'))

# Require sequel plugins
Dir[LIB_PATH.join('sequel', '*.rb')].each { |file| require file }
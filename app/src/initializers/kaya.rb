# LDAP settings by company name
KAYA_LDAP = {
  'acme' => {
    uri: 'ldap://kaya-api-ldap:389',
    user: 'admin',
    password: 'admin123654',
    base_dn: 'dc=example,dc=com',
    bind_dn: 'cn=admin,dc=example,dc=com',
    ou_users: 'people',
    ou_groups: 'groups'
  }
}

# yandex settings by domain name
KAYA_DOMAIN_TOKENS = { 
    'example.com' => { token: ENV['YANDEX_EXAMPLE_COM_TOKEN'] },
    'example.org' => { token: ENV['GOOGLE_EXAMPLE_ORG_TOKEN'] }
}

# google settings by domain name
KAYA_DOMAIN_ADMIN_EMAILS = { 
    'example.com' => { email: ENV['GOOGLE_EXAMPLE_COM_ADMIN_EMAIL'] },
    'example.org' => { email: ENV['GOOGLE_EXAMPLE_ORG_ADMIN_EMAIL'] }
}

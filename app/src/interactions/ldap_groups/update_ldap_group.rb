# frozen_string_literal: true

module Interactions
  class UpdateLdapGroup < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    
    def execute
      begin
        x = Company.find(id: path_params[:id])
        return k_response(http: :not_found, kaya: :not_found) if x.nil?
        return k_response(http: :bad_request, kaya: :could_not_sync_ldap_groups) unless sync_groups(x)
        k_response http: :no_content
      rescue Sequel::UniqueConstraintViolation => e
        k_response(http: :bad_request, kaya: :unique_violation, error: e)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_updated, error: e)
      end
    end
 
    def sync_groups(company)
      security_groups = SecurityGroup.where(company_id: company.id).all
      return false if security_groups.empty?
        
      group_data = []
      security_groups.each do |g|
        group_data.push g.ldap_hash
      end
        
      ::Helpers::OpenLDAP.sync_groups(KAYA_LDAP[company.code], group_data)
    end
  end
end

# frozen_string_literal: true

module Interactions
  class CreateEmailList < ::Interactions::Base
    hash :body_params do
      integer :company_id
      integer :domain_id
      string :name
      string :description
      string :email
      string :list_id
    end
    
    def execute
      begin
        c_user = current_user(headers[:authorization]) 
        body_params.merge!({current_user: c_user})
        x = EmailList.create(body_params)
        k_response(http: :created, result: x.values)
      rescue Sequel::UniqueConstraintViolation => e
        k_response(http: :bad_request, kaya: :unique_violation, error: e)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_created, error: e)
      end
    end
  end
end

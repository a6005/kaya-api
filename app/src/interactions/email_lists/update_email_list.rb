# frozen_string_literal: true

module Interactions
  class UpdateEmailList < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :body_params do
      string :name
      string :description
      string :list_id
    end
    
    def execute
      x = EmailList.find(id: path_params[:id])
      return k_response(http: :not_found, kaya: :not_found) if x.nil?

      begin
        x.current_user = current_user(headers[:authorization]) 
        x.update(body_params)
        k_response(http: :no_content)
      rescue Sequel::UniqueConstraintViolation => e
        k_response(http: :bad_request, kaya: :unique_violation, error: e)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_updated, error: e)
      end
    end
  end
end

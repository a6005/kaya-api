# frozen_string_literal: true

module Interactions
  class SearchEmailList < ::Interactions::Base
    hash :query_params do
      integer :domain_id, default: nil
      string :name, default: nil
      string :description, default: nil
      string :email, default: nil
      string :list_id, default: nil
    end
    
    def execute
      begin
        x = EmailList.dataset
        x = x.where(domain_id: query_params[:domain_id]) unless query_params[:domain_id].nil?
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        x = x.where(description: query_params[:description]) unless query_params[:description].nil?
        x = x.where(email: query_params[:email]) unless query_params[:email].nil?
        x = x.where(list_id: query_params[:list_id]) unless query_params[:list_id].nil?
        k_response(http: :ok, result: x.map{ |e| e.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end

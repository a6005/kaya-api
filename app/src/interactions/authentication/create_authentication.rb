# frozen_string_literal: true

require 'active_support/time'

module Interactions
  class CreateAuthentication < ::Interactions::Base
    hash :body_params do
      string :email
      string :password
      integer :expire_period, default: ENVIRONMENT == 'development' ? 480.minutes.to_i : 30.minutes.to_i
    end
  
    def execute
      if body_params[:email].nil? || body_params[:email].empty?
        return k_response(http: :bad_request, kaya: :bad_request)
      end
      if body_params[:password].nil? || body_params[:password].empty?
        return k_response(http: :bad_request, kaya: :bad_request)
      end

      user = User.find(email: body_params[:email])

      unless user && user.authenticate(body_params[:password])
        # always return unauthorized for security reasons
        return k_response(http: :unauthorized, kaya: :unauthorized)
      end

      payload = {
        name: user.name,
        is_admin: user.admin,
        id: user.id,
        email: user.email,
        company_id: user.company_id,
        expires_at: (Time.now + body_params[:expire_period]).to_i,
        issued_at: Time.now.to_i
      }
      token = JWT.encode payload, APP_SECRET, JWT_ALGORITHM
      k_response http: :created, result: { token: token }
    end
  end
end
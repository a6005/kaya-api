# frozen_string_literal: true

module Interactions
  class UpdateEmailProvider < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :body_params do
      string :name
      boolean :implemented
      boolean :allow_external
    end
    
    def execute
      x = EmailProvider.find(id: path_params[:id])
      return k_response(http: :not_found, kaya: :not_found) if x.nil?

      begin
        x.update(body_params)
        k_response http: :no_content
      rescue => e
        k_response http: :internal_server_error, kaya: :not_updated, error: e
      end
    end
  end
end

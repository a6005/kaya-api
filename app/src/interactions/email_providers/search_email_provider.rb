# frozen_string_literal: true

module Interactions
  class SearchEmailProvider < ::Interactions::Base
    hash :query_params do
      string :name, default: nil
      boolean :implemented, default: nil
      boolean :allow_external, default: nil
    end
    
    def execute
      begin
        x = EmailProvider.dataset
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        x = x.where(implemented: query_params[:implemented]) unless query_params[:implemented].nil?
        x = x.where(allow_external: query_params[:allow_external]) unless query_params[:allow_external].nil?
        k_response(http: :ok, result: x.map{ |e| e.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end

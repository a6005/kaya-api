# frozen_string_literal: true

module Interactions
  class GetEmailProvider < ::Interactions::Base
    hash :path_params do
      integer :id #Nested filter
    end
    
    def execute
      try_exec do
        x = EmailProvider.find(id: path_params[:id])
        if x.nil?
          k_response http: :not_found, kaya: :not_found
        else
          k_response http: :ok, result: x.values
        end
      end
    end
  end
end

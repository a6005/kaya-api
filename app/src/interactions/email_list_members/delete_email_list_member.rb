# frozen_string_literal: true

module Interactions
  class DeleteEmailListMember < ::Interactions::Base
    hash :path_params do
      integer :id
      integer :email_id
    end

    def execute
      begin
        x = EmailListMember.find(email_list_id: path_params[:id], email_id: path_params[:email_id])
        return k_response(http: :not_found, kaya: :not_found) if x.nil?

        x.current_user = current_user(headers[:authorization]) 
        x.destroy
        k_response http: :no_content
      rescue Sequel::ForeignKeyConstraintViolation => e
        k_response http: :bad_request,  kaya: :foreign_key, error: e
      rescue => e
        k_response http: :internal_server_error,  kaya: :not_deleted, error: e
      end
    end
  end
end
# frozen_string_literal: true

module Interactions
  class GetEmailListMember < ::Interactions::Base
    hash :path_params do
      integer :id
      integer :email_id
    end
    
    def execute
      try_exec do
        x = EmailListMember.find(email_list_id: path_params[:id], email_id: path_params[:email_id])
        if x.nil?
          k_response http: :not_found, kaya: :not_found
        else
          k_response http: :ok, result: x.values
        end
      end
    end
  end
end

# frozen_string_literal: true

module Interactions
  class SearchEmailListMember < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :query_params do
      boolean :owner, default: nil
      boolean :can_send_on_behalf, default: nil
    end
    
    def execute
      begin
        x = EmailListMember.dataset
        x = x.where(email_list_id: path_params[:id])
        x = x.where(owner: query_params[:owner]) unless query_params[:owner].nil?
        x = x.where(can_send_on_behalf: query_params[:can_send_on_behalf]) unless query_params[:can_send_on_behalf].nil?
        k_response(http: :ok, result: x.map{ |e| e.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end

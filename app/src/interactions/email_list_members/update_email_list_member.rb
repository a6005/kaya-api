# frozen_string_literal: true

module Interactions
  class UpdateEmailListMember < ::Interactions::Base
    hash :path_params do
      integer :id
      integer :email_id
    end
    hash :body_params do
      boolean :owner
      boolean :can_send_on_behalf
    end
    
    def execute
      begin
        x = EmailListMember.find(email_list_id: path_params[:id], email_id: path_params[:email_id])
        return k_response(http: :not_found, kaya: :not_found) if x.nil?

        x.current_user = current_user(headers[:authorization]) 
        x.owner = body_params[:owner]
        x.can_send_on_behalf = body_params[:can_send_on_behalf]
        x.save
        k_response(http: :no_content)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_updated, error: e)
      end
    end
  end
end

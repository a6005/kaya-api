# frozen_string_literal: true

module Interactions
  class CreateEmailListMember < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :body_params do
      integer :email_id
      boolean :owner
      boolean :can_send_on_behalf
    end
    
    def execute
      begin
        c_user = current_user(headers[:authorization]) 
        body_params.merge!({current_user: c_user})
        x = EmailListMember.new()
        x.current_user = c_user
        x.email_list_id = path_params[:id]
        x.email_id = body_params[:email_id]
        x.owner = body_params[:owner]
        x.can_send_on_behalf = body_params[:can_send_on_behalf]
        x.save
        k_response(http: :created, result: x.values)
      rescue Sequel::ForeignKeyConstraintViolation => e
        k_response(http: :bad_request, kaya: :foreign_key_violation, error: e)
      rescue Sequel::UniqueConstraintViolation => e
        k_response(http: :bad_request, kaya: :unique_violation, error: e)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_created, error: e)
      end
    end
  end
end

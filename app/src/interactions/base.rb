Dir[HELPERS_PATH.join('*.rb')].each { |file| require file }
module Interactions
  class Base < ActiveInteraction::Base
    # Default Filters
    include ::Helpers::Application
    include ::Helpers::Authentication
    
    hash :headers do
      string :authorization, default: nil
    end
    boolean :authenticate, default: true
    
    #set_callback :type_check, :after, -> {
    set_callback :validate, :after, -> {
      if authenticate and not authorize_token(headers[:authorization])
        errors.add(:code, KAYA_CODE[:unauthorized], kaya_code: :token_not_valid, http_code: :unauthorized) 
      end
    }
  end
end
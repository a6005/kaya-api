# frozen_string_literal: true

module Interactions
  class UpdateCompany < ::Interactions::Base
    hash :path_params do
      integer :id    
    end
    hash :body_params do
      string :name
      string :code
    end
    
    def execute
      x = Company.find(id: path_params[:id])
      # return k_response(:not_found, nil, :not_found) if x.nil?
      return k_response(http: :not_found, kaya: :not_found) if x.nil?

      begin
        x.update(body_params)
        k_response http: :no_content
      rescue Sequel::UniqueConstraintViolation => e
        k_response http: :bad_request, kaya: :unique_violation, error: e
      rescue => e
        # k_response(:internal_server_error, nil, :not_updated, e)
        k_response http: :internal_server_error, kaya: :not_updated, error: e
      end
    end
  end
end
# frozen_string_literal: true

module Interactions
  class GetCompany < ::Interactions::Base
    hash :path_params do
      # even if the input hash has more keys active interaction removes all keys that not defined as nested filter
      integer :id #Nested filter
    end
    
    def execute
      begin
        x = Company.find(id: path_params[:id])
        if x.nil?
          k_response http: :not_found, kaya: :not_found
        else
          k_response http: :ok, result: x.values
        end
      rescue => e
        k_response http: :internal_server_error, kaya: :internal_server_error, error: e
      end
    end
  end
end
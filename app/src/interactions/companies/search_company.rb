# frozen_string_literal: true

module Interactions
  class SearchCompany < ::Interactions::Base
    hash :query_params do
      string :name, default: nil
      string :code, default: nil
    end
    
    def execute
      begin
        x = Company.dataset
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        x = x.where(code: query_params[:code]) unless query_params[:code].nil?
        k_response(http: :ok, result: x.map{ |k| k.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end
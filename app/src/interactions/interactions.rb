require INTERACTIONS_PATH.join('base.rb')
# there is a circular require with this code:  Dir[INTERACTIONS_PATH.join('**', '*.rb')].each { |file|require file unless file
Dir[INTERACTIONS_PATH.join('**', '*.rb')].each { 
  |file| 
    f = File.basename(file)
    require file unless f == 'base.rb' or f == 'interactions.rb'
}
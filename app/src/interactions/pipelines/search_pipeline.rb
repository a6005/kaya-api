# frozen_string_literal: true

module Interactions
  class SearchPipeline < ::Interactions::Base
    hash :query_params do
      string :name, default: nil
      integer :user_id, default: nil
      integer :status, default: nil
    end
    
    def execute
      begin
        x = Pipeline.dataset
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        x = x.where(user_id: query_params[:user_id]) unless query_params[:user_id].nil?
        x = x.where(status: query_params[:status]) unless query_params[:status].nil?
        k_response(http: :ok, result: x.map{ |e| e.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end
# frozen_string_literal: true

module Interactions
  class SearchLanguage < ::Interactions::Base
    hash :query_params do
      string :language_code, default: nil
      string :name, default: nil
      string :name_in_english, default: nil
    end
    
    def execute
      begin
        x = Language.dataset
        x = x.where(language_code: query_params[:language_code]) unless query_params[:language_code].nil?
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        x = x.where(name_in_english: query_params[:name_in_english]) unless query_params[:name_in_english].nil?
        k_response(http: :ok, result: x.map{ |e| e.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end

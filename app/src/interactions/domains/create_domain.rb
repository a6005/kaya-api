# frozen_string_literal: true

module Interactions
  class CreateDomain < ::Interactions::Base
    hash :body_params do
      integer :company_id
      integer :email_provider_id
      string :name
      string :description
      boolean :sync_enabled
    end

    def execute
      begin 
        x = Domain.create(body_params)
        k_response http: :created, result: x.values
      rescue Sequel::UniqueConstraintViolation => e
        k_response http: :bad_request, kaya: :unique_violation, error: e
      rescue => e
        k_response http: :internal_server_error, kaya: :not_created, error: e
      end
    end
  end
end

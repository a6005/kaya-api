# frozen_string_literal: true

module Interactions
  class UpdateDomain < ::Interactions::Base
    hash :path_params do
      integer :id    
    end
    hash :body_params do
      string :name
      string :description
      boolean :sync_enabled
    end
    
    def execute
      x = Domain.find(id: path_params[:id])
      return k_response(http: :not_found, kaya: :not_found) if x.nil?

      begin
        x.update(body_params)
        k_response http: :no_content
      rescue => e
        k_response http: :internal_server_error, kaya: :not_updated, error: e
      end
    end
  end
end

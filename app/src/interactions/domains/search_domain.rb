# frozen_string_literal: true

module Interactions
  class SearchDomain < ::Interactions::Base
    hash :query_params do
      integer :company_id, default: nil
      integer :email_provider_id, default: nil
      string :name, default: nil
      string :description, default: nil
      boolean :sync_enabled, default: nil
    end
    
    def execute
      try_exec do
        x = Domain.dataset
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        x = x.where(company_id: query_params[:company_id]) unless query_params[:company_id].nil?
        x = x.where(email_provider_id: query_params[:email_provider_id]) unless query_params[:email_provider_id].nil?
        x = x.where(description: query_params[:description]) unless query_params[:description].nil?
        x = x.where(sync_enabled: query_params[:sync_enabled]) unless query_params[:sync_enabled].nil?
        if x.empty?
          k_response http: :not_found, kaya: :not_found
        else
          k_response http: :ok, result: x.map{ |k| k.values }
        end
      end
    end
  end
end

# frozen_string_literal: true

module Interactions
  class SearchUser < ::Interactions::Base
    hash :query_params do
      string :os_username, default: nil
      string :name, default: nil
      string :surname, default: nil
    end
    
    def execute
      begin
        x = User.dataset
        x = x.where(os_username: query_params[:os_username]) unless query_params[:os_username].nil?
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        x = x.where(surname: query_params[:surname]) unless query_params[:surname].nil?
        k_response(http: :ok, result: x.map{ |k| k.public_values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end

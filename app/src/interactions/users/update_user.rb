# frozen_string_literal: true

module Interactions
  class UpdateUser < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :body_params do
      boolean :admin, default: nil
      string :email, default: nil
      boolean :enabled, default: nil
      boolean :has_email, default: nil
      boolean :has_ldap, default: nil
      string :os_username, default: nil
      integer :os_uid, default: nil
      string :name, default: nil
      string :surname, default: nil
      integer :department_id, default: nil
      integer :security_group_id, default: nil
      integer :language_id, default: nil
      integer :employee_type_id, default: nil
      string :employee_number, default: nil
      string :room_number, default: nil
      string :phone, default: nil
      string :fax, default: nil
      string :initials, default: nil
      string :login_shell, default: nil
      string :home_dir, default: nil
      string :title, default: nil
      integer :mindays, default: nil
      integer :maxdays, default: nil
      integer :warndays, default: nil
    end
           
    def execute
      x = User.find(id: path_params[:id])
      return k_response(http: :not_found, kaya: :not_found) if x.nil?

      begin
        x.current_user = current_user(headers[:authorization]) 
        # h = body_params.select { |k, v| !v.nil? }
        body_params.compact!
        APP_LOGGER.debug "Interactions:UpdateUser:execute: body_params: #{body_params}"
        x.update(body_params)
        k_response(http: :no_content)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_updated, error: e)
      end
    end
  end
end

# frozen_string_literal: true

module Interactions
  class CreateUser < ::Interactions::Base
    hash :body_params do
      integer :company_id
      integer :os_uid
      string :os_username
      boolean :enabled
      boolean :has_ldap
      boolean :has_email
      integer :department_id
      integer :security_group_id
      integer :employee_type_id
      string :employee_number
      string :name
      string :surname
      string :initials
      string :password
      string :password_confirmation
      string :login_shell
      string :home_dir
      string :email
      string :title
      string :room_number
      string :phone
      string :fax
      integer :mindays
      integer :maxdays
      integer :warndays
      integer :language_id
    end

    def execute
      begin 
        c_user = current_user(headers[:authorization]) 
        body_params.merge!({current_user: c_user})
        x = User.create(body_params)
        k_response(http: :created, result: x.public_values)
      rescue Sequel::NotNullConstraintViolation, Sequel::ValidationFailed, Sequel::UniqueConstraintViolation => e
        k_response(http: :bad_request, kaya: :validation_failed, error: e)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_created, error: e)
      end
    end
  end
end

# frozen_string_literal: true

module Interactions
  class GetUser < ::Interactions::Base
    hash :path_params do
      integer :id, desc: "id of user"
    end
        
    def execute
      try_exec do
        x = User.find(id: path_params[:id])
        if x.nil?
          k_response http: :not_found, kaya: :not_found
        else
          k_response http: :ok, result: x.public_values
        end
      end
    end
  end
end

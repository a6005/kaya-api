# frozen_string_literal: true

module Interactions
  class DeleteSecurityGroup < ::Interactions::Base
    hash :path_params do
      integer :id    
    end

    def execute
      x = SecurityGroup.find(id: path_params[:id])
      return k_response(http: :not_found, kaya: :not_found) if x.nil?

      begin
        x.current_user = current_user(headers[:authorization]) 
        x.destroy
        k_response http: :no_content
      rescue Sequel::ForeignKeyConstraintViolation => e
        k_response http: :bad_request, kaya: :foreign_key, error: e
      rescue => e
        k_response http: :internal_server_error, kaya: :not_deleted, error: e
      end
    end
  end
end

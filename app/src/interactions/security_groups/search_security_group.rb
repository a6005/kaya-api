# frozen_string_literal: true

module Interactions
  class SearchSecurityGroup < ::Interactions::Base
    hash :query_params do
      string :name, default: nil
    end
    
    def execute
      begin
        x = SecurityGroup.dataset
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        k_response(http: :ok, result: x.map{ |r| r.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end
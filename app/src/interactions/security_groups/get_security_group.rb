# frozen_string_literal: true

module Interactions
  class GetSecurityGroup < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    
    def execute
      try_exec do
        x = SecurityGroup.find(id: path_params[:id])
        if x.nil?
          k_response http: :not_found, kaya: :not_found
        else
          k_response http: :ok, result: x.values
        end
      end
    end
  end
end

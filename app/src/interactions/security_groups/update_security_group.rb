# frozen_string_literal: true

module Interactions
  class UpdateSecurityGroup < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :body_params do
      integer :os_gid
      string :name
      string :description
    end
    
    def execute
      begin
        x = SecurityGroup.find(id: path_params[:id])
        return k_response(http: :not_found, kaya: :not_found) if x.nil?

        #user_id = 2
        #::Helpers::JobDispatcher:sync_groups(user_id, x)

        x.current_user = current_user(headers[:authorization]) 
        x.update(body_params)
        k_response(http: :no_content)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_updated, error: e)
      end
    end
  end
end
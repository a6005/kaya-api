# frozen_string_literal: true

module Interactions
  class CreateSecurityGroup < ::Interactions::Base
    hash :body_params do
      integer :company_id
      integer :os_gid
      string :name
      string :description
    end
    
    def execute
      begin 
        x = SecurityGroup.create(body_params)
        k_response http: :created, result: x.values
      rescue Sequel::UniqueConstraintViolation => e
        k_response http: :bad_request, kaya: :unique_violation, error: e
      rescue => e
        k_response http: :internal_server_error, kaya: :not_created, error: e
      end
    end
  end
end

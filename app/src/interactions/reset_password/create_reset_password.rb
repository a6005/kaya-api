# frozen_string_literal: true

module Interactions
  class CreateResetPassword < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :body_params do
      string :current_password, default: nil
      string :password
      string :password_confirmation
    end
  
    def execute
      c_user = current_user(headers[:authorization])
      unless c_user.admin
        # user must supply current password
        if body_params[:current_password].nil? || body_params[:current_password].empty?
          return k_response(http: :bad_request, kaya: :bad_request)
        end
        # user can only change his / her own password
        unless c_user.id == path_params[:id]
          APP_LOGGER.debug "CreateResetPassword: user can only change his / her own password"
          return k_response(http: :unauthorized, kaya: :unauthorized) 
        end
      end
        
      APP_LOGGER.debug "CreateResetPassword: searching for user id: #{path_params[:id]}"
      user = User.find(id: path_params[:id])
      return k_response(http: :not_found, kaya: :not_found) unless user

      user.current_user = c_user
 
      APP_LOGGER.debug "CreateResetPassword: current username: #{user.os_username} is admin: #{c_user.admin}"

      # must authenticate current_password for normal users
      unless c_user.admin || user.authenticate(body_params[:current_password])
          APP_LOGGER.debug "CreateResetPassword: wrong current password"
          return k_response(http: :unauthorized, kaya: :unauthorized) 
      end

      APP_LOGGER.debug "CreateResetPassword: resetting password for username: #{user.os_username}"
        
      begin
        unless user.change_password(body_params[:password], body_params[:password_confirmation], c_user.id)
          return k_response(http: :bad_request, kaya: :validation_failed)
        end
        k_response(http: :no_content)
      rescue Sequel::UniqueConstraintViolation => e
        APP_LOGGER.debug "CreateResetPassword: e.class: #{e.class}"
        APP_LOGGER.debug "CreateResetPassword: unique"
        k_response http: :bad_request, kaya: :unique_violation, error: e
      rescue Sequel::ValidationFailed => e
        return k_response(http: :bad_request, kaya: :validation_failed, error: e)
      rescue Sequel::Error => e
        APP_LOGGER.debug "CreateResetPassword: validation failed resetting password for username: #{user.os_username} message: #{e.message}"
        APP_LOGGER.debug "CreateResetPassword: e.class: #{e.class}"
        return k_response(http: :bad_request, kaya: :bad_request, error: e)
      rescue => e
        APP_LOGGER.debug "CreateResetPassword: e.class: #{e.class}"
        APP_LOGGER.debug "CreateResetPassword: failed resetting password for username: #{user.os_username} message: #{e.message}"
        return k_response(http: :unauthorized, kaya: :unauthorized, error: e)
      end
    end
  end
end

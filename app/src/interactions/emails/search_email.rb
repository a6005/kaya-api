# frozen_string_literal: true

module Interactions
  class SearchEmail < ::Interactions::Base
    hash :query_params do
      string :email, default: nil
      boolean :confirmed, default: nil
    end
    
    def execute
      begin
        x = Email.dataset
        x = x.where(email: query_params[:email]) unless query_params[:email].nil?
        x = x.where(confirmed: query_params[:confirmed]) unless query_params[:confirmed].nil?
        k_response(http: :ok, result: x.map{ |e| e.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end

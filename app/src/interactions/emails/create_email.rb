# frozen_string_literal: true

module Interactions
  class CreateEmail < ::Interactions::Base
    hash :body_params do
      string :email
    end
    
    def execute
      begin
        x = Email.create(body_params)
        k_response http: :created, result: x.values
      rescue Sequel::UniqueConstraintViolation => e
        k_response http: :bad_request, kaya: :unique_violation, error: e
      rescue => e
        k_response http: :internal_server_error, kaya: :not_created, error: e
      end
    end
  end
end

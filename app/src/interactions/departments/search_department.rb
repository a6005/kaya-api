# frozen_string_literal: true

module Interactions
  class SearchDepartment < ::Interactions::Base
    hash :query_params do
      string :name, default: nil
    end
    
    def execute
      try_exec do
        x = Department.dataset
        x = x.where(name: query_params[:name]) unless query_params[:name].nil?
        k_response http: :ok, result: x.map{ |e| e.values }
      end
    end
  end
end

# frozen_string_literal: true

module Interactions
  class CreateSecurityGroupMember < ::Interactions::Base
    hash :path_params do
      integer :security_group_id
    end
    hash :body_params do
      integer :user_id
      boolean :owner
    end
    
    def execute
      begin
        x = SecurityGroupMember.new()
        x.current_user = current_user(headers[:authorization]) 
        x.security_group_id = path_params[:security_group_id]
        x.user_id = body_params[:user_id]
        x.owner = body_params[:owner]
        x.save
        k_response http: :created, result: x.values
      rescue Sequel::ForeignKeyConstraintViolation => e
        k_response http: :bad_request, kaya: :foreign_key_violation, error: e
      rescue Sequel::UniqueConstraintViolation => e
        k_response http: :bad_request, kaya: :unique_violation, error: e
      rescue => e
        k_response http: :internal_server_error, kaya: :not_created, error: e
      end
    end
  end
end

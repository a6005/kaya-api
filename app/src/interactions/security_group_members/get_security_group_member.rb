# frozen_string_literal: true

module Interactions
  class GetSecurityGroupMember < ::Interactions::Base
    hash :path_params do
      integer :id
      integer :user_id
    end
    
    def execute
      try_exec do
        x = SecurityGroupMember.find(security_group_id: path_params[:id], user_id: path_params[:user_id])
        if x.nil?
          k_response http: :not_found, kaya: :not_found
        else
          k_response http: :ok, result: x.values
        end
      end
    end
  end
end

# frozen_string_literal: true

module Interactions
  class SearchSecurityGroupMember < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :query_params do
      boolean :owner, default: nil
    end
    
    def execute
      begin
        x = SecurityGroupMember.dataset
        x = x.where(security_group_id: path_params[:id])
        x = x.where(owner: query_params[:owner]) unless query_params[:owner].nil?
        k_response(http: :ok, result: x.map{ |r| r.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end
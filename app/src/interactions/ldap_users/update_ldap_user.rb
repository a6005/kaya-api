# frozen_string_literal: true

module Interactions
  class UpdateLdapUser < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    
    def execute
      begin
        x = Company.find(id: path_params[:id])
        return k_response(http: :not_found, kaya: :not_found) if x.nil?
        return k_response(http: :bad_request, kaya: :could_not_sync_ldap_users) unless sync_users(x)
        k_response http: :no_content
      rescue Sequel::UniqueConstraintViolation => e
        k_response(http: :bad_request, kaya: :unique_violation, error: e)
      rescue => e
        k_response(http: :internal_server_error, kaya: :not_updated, error: e)
      end
    end

    def sync_users(company) 
      ldap_users = User.where(company_id: company.id, has_ldap: true).all
      return false if ldap_users.empty?
      
      user_data = []
      ldap_users.each do |u|
        user_data.push u.ldap_hash
      end

      ::Helpers::OpenLDAP.sync_users(KAYA_LDAP[company.code], user_data)
    end
  end
end

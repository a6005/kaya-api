# frozen_string_literal: true

module Interactions
  class GetLdapUser < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    
    def execute
      begin
        c = Company.find(id: path_params[:id])
        if c.nil?
          k_response(http: :not_found, kaya: :not_found)
        else
          x = ::Helpers::OpenLDAP.get_users(KAYA_LDAP[c.code])
          return k_response(http: :not_found, kaya: :no_found) unless x

          k_response(http: :ok, result: x)
        end
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end

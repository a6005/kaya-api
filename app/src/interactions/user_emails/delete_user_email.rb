# frozen_string_literal: true

module Interactions
  class DeleteUserEmail < ::Interactions::Base
    hash :path_params do
      integer :id    
    end
    
    hash :query_params do
      string :email
    end

    def execute
      x = UserEmail.find(user_id: path_params[:id], email: query_params[:email])
      return k_response(http: :not_found, kaya: :not_found) if x.nil?

      begin
        x.destroy
        k_response http: :no_content
      rescue Sequel::ForeignKeyConstraintViolation => e
        k_response http: :bad_request, kaya: :foreign_key, error: e
      rescue => e
        k_response http: :internal_server_error, kaya: :not_deleted, error: e
      end
    end
  end
end
# frozen_string_literal: true

module Interactions
  class CreateUserEmail < ::Interactions::Base
    hash :path_params do
      integer :id
    end
    hash :body_params do
      string :email
    end
    
    def execute
      begin 
        x = UserEmail.new()
        x.user_id = path_params[:id]
        x.email = body_params[:email]
        x.save
        k_response http: :created, result: x.values
      rescue Sequel::UniqueConstraintViolation => e
        k_response http: :bad_request, kaya: :unique_violation, error: e
      rescue => e
        k_response http: :internal_server_error, kaya: :not_created, error: e
      end
    end
  end
end
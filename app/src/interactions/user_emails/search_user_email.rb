# frozen_string_literal: true

module Interactions
  class SearchUserEmail < ::Interactions::Base
    hash :path_params do
      # even if the input hash has more keys active interaction removes all keys that not defined as nested filter
      integer :id #Nested filter
    end
    
    hash :query_params do
      string :email, default: nil
    end

    def execute
      begin
        x = UserEmail.dataset
        x = x.where(user_id: path_params[:id])
        x = x.where(email: query_params[:email]) unless query_params[:email].nil?
        k_response(http: :ok, result: x.map{ |e| e.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end
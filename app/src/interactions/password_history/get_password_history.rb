# frozen_string_literal: true

module Interactions
  class GetPasswordHistory < ::Interactions::Base
    hash :path_params do
      # even if the input hash has more keys active interaction removes all keys that not defined as nested filter
      integer :id #Nested filter
    end
    
    def execute
      begin
        x = PasswordHistory.where(user_id: path_params[:id]).all
        if x.empty?
          k_response http: :not_found, kaya: :not_found
        else
          k_response http: :ok, result: x.map{ |k| k.public_values }
        end
      rescue => e
        k_response http: :internal_server_error, kaya: :internal_server_error, error: e
      end
    end
  end
end
# frozen_string_literal: true

module Interactions
  class SearchJob < ::Interactions::Base
    hash :path_params do
      integer :id    
    end
    hash :query_params do
      string :worker, default: nil
    end
    
    def execute
      begin
        x = Job.dataset
        x = x.where(pipeline_id: path_params[:id])
        x = x.where(worker: query_params[:worker]) unless query_params[:worker].nil?
        k_response(http: :ok, result: x.map{ |e| e.values })
      rescue => e
        k_response(http: :internal_server_error, kaya: :internal_server_error, error: e)
      end
    end
  end
end
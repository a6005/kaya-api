# frozen_string_literal: true

module Interactions
  class UpdateJob < ::Interactions::Base
    hash :path_params do
      integer :id    
      integer :job_id    
    end
    hash :body_params do
      integer :status
    end
    
    def execute
      x = Job.find(id: path_params[:job_id], pipeline_id: path_params[:id])
      # return k_response(:not_found, nil, :not_found) if x.nil?
      return k_response(http: :not_found, kaya: :not_found) if x.nil?

      begin
        x.update(body_params)
        k_response http: :no_content
      rescue Sequel::UniqueConstraintViolation => e
        k_response http: :bad_request, kaya: :unique_violation, error: e
      rescue => e
        k_response http: :internal_server_error, kaya: :not_updated, error: e
      end
    end
  end
end
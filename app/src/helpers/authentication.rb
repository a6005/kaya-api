module Helpers
  module Authentication
    def authorize_token(token)
      begin
        APP_LOGGER.debug "Helpers:Authentication:authorize_token: token: #{token}"
        token = token.gsub('Bearer', '').strip
        JWT.decode(token, APP_SECRET, true, { algorithm: JWT_ALGORITHM })
      rescue => e
        APP_LOGGER.debug "Helpers:Authentication:authorize_token: error"
        APP_LOGGER.error e.backtrace
        nil
      end
    end

    def current_user(token)
      APP_LOGGER.debug "Helpers:Authentication:current_user: token: #{token}"
      payload = authorize_token(token)
      if payload
        User.find(id: payload.first['id'])
      end
    end
  end
end
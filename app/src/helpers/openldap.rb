# frozen_string_literal: true

require 'net/ldap'
require_relative '../../environment.rb'

module Helpers
  module OpenLDAP
    class << self
      def delete_group(conf, group_name)
        return false if conf.nil?

        APP_LOGGER.debug "Helpers::OpenLDAP.delete_group: Deleting group : #{group_name}..."
        ldap = connect_and_bind(conf[:uri], conf[:bind_dn], conf[:password])
        return false unless ldap

        dn = "cn=#{group_name},ou=#{conf[:ou_groups]},#{conf[:base_dn]}"
        APP_LOGGER.debug "Helpers::OpenLDAP.delete_group: deleting dn: #{dn}"
        if dn_exists(ldap, dn)
          APP_LOGGER.debug "Helpers::OpenLDAP.delete_group: dn: #{dn} does exists. Deleting..."
          ldap.delete(dn: dn)
        else
          APP_LOGGER.debug "Helpers::OpenLDAP.delete_group: dn: #{dn} does not exist. No need to delete."
          return true
        end
      end

      def delete_user(conf, username)
        return false if conf.nil?

        APP_LOGGER.debug "Helpers::OpenLDAP.delete_user: Deleting username: #{username}..."
        ldap = connect_and_bind(conf[:uri], conf[:bind_dn], conf[:password])
        return false unless ldap

        dn = "uid=#{username},ou=#{conf[:ou_users]},#{conf[:base_dn]}"
        APP_LOGGER.debug "Helpers::OpenLDAP.delete_user: deleting dn: #{dn}"
        if dn_exists(ldap, dn)
          APP_LOGGER.debug "Helpers::OpenLDAP.delete_user: dn: #{dn} does exists. Deleting..."
          ldap.delete(dn: dn)
        else
          APP_LOGGER.debug "Helpers::OpenLDAP.delete_user: dn: #{dn} does not exist. No need to delete."
          return true
        end
      end

      def get_groups(conf)
        return false if conf.nil?
        
        APP_LOGGER.debug "conf: #{conf}"
        ldap = connect_and_bind(conf[:uri], conf[:bind_dn], conf[:password])
        return false unless ldap

        ou = conf[:ou_groups]
        base_dn = conf[:base_dn]
        dn = "ou=#{ou},#{base_dn}"
        return false unless dn_exists(ldap, dn)

        APP_LOGGER.debug "Helpers::OpenLDAP.get_groups: Checking for dn: #{dn}"
        filter = Net::LDAP::Filter.eq('objectclass', 'posixGroup')
        attributes = ['cn', 'gidnumber', 'description', 'memberuid']
        a = ldap.search(base: dn, filter: filter, attributes: attributes)
        return false if a.nil?
        groups = []
        a.each do |entry|
          h = {}
          # initializing for ordering 
          h[:id] = 0
          h[:name] = 0
          h[:description] = 0
          h[:members] = []
          m = []
          entry.each do |attr, values|
            values.each do |value|
              h[:id] = value.to_i if attr.to_s == 'gidnumber'
              h[:name] = value if attr.to_s == 'cn'
              h[:description] = value if attr.to_s == 'description'
              m.push value if attr.to_s == 'memberuid'
            end
          end
          unless m.empty?
            h[:members] = m 
            groups.push h
          end
        end
        #[ 
        #  { id: 1001, name: 'test1', description: 'test group 1', members: [ 'test', 'test2'] },
        #  { id: 1002, name: 'test2', description: 'test group 2', members: [ 'test', 'test3'] },
        #]
        groups
      end

      def get_users(conf)
        return false if conf.nil?

        APP_LOGGER.debug "Helpers::Ldap:get_users: conf: #{conf}"
        ldap = connect_and_bind(conf[:uri], conf[:bind_dn], conf[:password])
        return false unless ldap

        ou = conf[:ou_users]
        base_dn = conf[:base_dn]
        dn = "ou=#{ou},#{base_dn}"
        return false unless dn_exists(ldap, dn)

        APP_LOGGER.debug "Helpers::Ldap:get_users: Checking for dn: #{dn}"
        # filter = Net::LDAP::Filter.eq('objectclass', ['posixAccount', 'shadowAccount'])
        attributes = ['uid', 'uidnumber', 'cn', 'gidnumber', 'homedirectory']
        # a = ldap.search(base: dn, filter: filter, attributes: attributes)
        a = ldap.search(base: dn, attributes: attributes)
        return false if a.nil?
        users = []
        a.each do |entry|
          h = {}
          # initializing for ordering 
          h[:id] = 0
          h[:username] = ''
          h[:name] = ''
          h[:group_id] = 0
          h[:home_dir] = ''
          entry.each do |attr, values|
            values.each do |value|
              h[:id] = value.to_i if attr.to_s == 'uidnumber'
              h[:username] = value if attr.to_s == 'uid'
              h[:name] = value if attr.to_s == 'cn'
              h[:group_id] = value.to_i if attr.to_s == 'gidnumber'
              h[:home_dir] = value if attr.to_s == 'homedirectory'
            end
          end
          users.push h unless h[:id] == 0
        end
        #[ 
        #  { id: 1001, username: 'test1', name: 'Test User 1', group_id: '1', home_dir: '/home/test1' },
        #  { id: 1002, username: 'test2', name: 'Test User 2', group_id: '2', home_dir: '/home/test2' }
        #]

        users
      end

      def reset_password(conf, username, password_hash)
        return false if conf.nil?

        APP_LOGGER.debug "Helpers::OpenLDAP.reset_password: Resetting password for username: #{username}..."
        APP_LOGGER.debug "Helpers::OpenLDAP.reset_password: Resetting conf: #{conf}..."
        ldap = connect_and_bind(conf[:uri], conf[:bind_dn], conf[:password])
        return false unless ldap

        dn = "uid=#{username},ou=#{conf[:ou_users]},#{conf[:base_dn]}"
        APP_LOGGER.debug "Helpers::OpenLDAP.reset_password: resetting password dn: #{dn}"
        ldap.replace_attribute(dn, 'userpassword', "{crypt}#{password_hash}")
      end

      def sync_groups(conf, data)
        return false if conf.nil?
        
        APP_LOGGER.debug "Helpers::OpenLDAP.sync_groups: ..."
        ldap = connect_and_bind(conf[:uri], conf[:bind_dn], conf[:password])
        return false unless ldap
        return false unless init_ou(ldap, conf[:base_dn], conf[:ou_groups])

        if data.is_a? Array
          data.each do |d|
            group = d[:group]
            attributes = d[:ldap_attributes]
            sync_group(ldap, conf, group, attributes)
          end
        else
            group = data[:group]
            attributes = data[:ldap_attributes]
            sync_group(ldap, conf, group, attributes)
        end
      end
 
      def sync_users(conf, data)
        return false if conf.nil?
        
        APP_LOGGER.debug "Helpers::OpenLDAP.sync_users: ..."
        ldap = connect_and_bind(conf[:uri], conf[:bind_dn], conf[:password])
        return false unless ldap
        return false unless init_ou(ldap, conf[:base_dn], conf[:ou_users])

        if data.is_a? Array
          data.each do |d|
            user = d[:user]
            enabled = d[:enabled]
            attributes = d[:ldap_attributes]
            sync_user(ldap, conf, user, attributes, enabled)
          end
        else
          user = data[:user]
          enabled = data[:enabled]
          attributes = data[:ldap_attributes]
          sync_user(ldap, conf, user, attributes, enabled)
        end
      end
     
      private
      
      def sync_group(ldap, conf, group, attributes)
        APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: group: #{group}..."
        APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: attributes: #{attributes}"
      
        ou = conf[:ou_groups]
        base_dn = conf[:base_dn]
        dn = "cn=#{group},ou=#{ou},#{base_dn}"
        APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: dn: #{dn}"
      
        has_members = attributes[:memberuid].count > 0 ? true : false

        #if dn_exists(ldap, dn)
        #  if has_members
        #    return false unless ldap.delete(dn: dn)
        #    ldap.add(dn: dn, attributes: attributes)
        #  else
        #    ldap.delete(dn: dn)
        #  end
        #else
        #  if has_members
        #    ldap.add(dn: dn, attributes: attributes)
        #  end
        #end

        if dn_exists(ldap, dn)
          if has_members
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: group : #{group} exists. Updating..."
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: group : #{group} exists. First deleting..."
            x = ldap.delete(dn: dn)
            if x
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: deleted group: #{group}"
            else
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: could not delete group: #{group}"
              return false
            end 
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: group : Now creating #{group} ..."
            x = ldap.add(dn: dn, attributes: attributes)
            if x
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: created group: #{group}"
            else
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: could not create group: #{group}"
              return false
            end 
          else
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: group : #{group} exists, but has no members. Deleting..."
            x = ldap.delete(dn: dn)
            if x
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: deleted group: #{group}"
            else
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: could not delete group: #{group}"
              return false
            end 
          end
        else
          if has_members
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: group : #{group} does not exists. Creating..."
            x = ldap.add(dn: dn, attributes: attributes)
            if x
               APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: created group: #{group}" 
            else
               APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: could not create group: #{group}"
              return false
            end 
          else
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_group: group : #{group} does not exist in LDAP and has no members. Nothing to do..."
          end
        end
        true
      end

      def sync_user(ldap, conf, user, attributes, enabled)
        APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: #{user}..."
        APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: attributes: #{attributes}"

        ldap = connect_and_bind(conf[:uri], conf[:bind_dn], conf[:password])
        return false unless ldap

        ou = conf[:ou_users]
        base_dn = conf[:base_dn]
        dn = "uid=#{user},ou=#{ou},#{base_dn}"
        APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: dn: #{dn}"

        #if dn_exists(ldap, dn)
        #  if enabled
        #    return false unless ldap.delete(dn: dn)
        #    ldap.add(dn: dn, attributes: attributes)
        #  else
        #    x = ldap.delete(dn: dn)
        #  end
        #else
        #  if enabled
        #    ldap.add(dn: dn, attributes: attributes)
        #  end
        #end

        if dn_exists(ldap, dn)
          if enabled
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: user : #{user} exists. Updating..."
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: user : #{user} exists. First deleting..."
            x = ldap.delete(dn: dn)
            if x
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: deleted user: #{user}"
            else
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: could not delete user: #{user}"
              return false
            end 

            APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: user : Now creating #{user} ..."
            x = ldap.add(dn: dn, attributes: attributes)
            if x
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: created user: #{user}"
            else
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: could not create user: #{user}"
              return false
            end
          else
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: user : #{user} exists. User is disabled. Deleting..."
            x = ldap.delete(dn: dn)
            if x
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: updated user: #{user}"
            else
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: could not update user: #{user}"
            end 
          end
        else
          if enabled
            APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: user : #{user} does not exists. Creating..."
            x = ldap.add(dn: dn, attributes: attributes)
            if x
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: created user: #{user}" 
            else
              APP_LOGGER.debug "Helpers::OpenLDAP.sync_user: could not create user: #{user}"
            end 
          end
        end
        true
      end
      
      def connect_and_bind(uri, bind_dn, password)
        h = extract_uri(uri)
        host = h[:host]
        port = h[:port]
        encrypted = h[:encrypted]

        APP_LOGGER.debug "Helpers::OpenLDAP.connect_and_bind: connecting to host: #{host} port: #{port}, bind_dn: #{bind_dn}, password: #{password}"
        begin
          if encrypted
            ldap = Net::LDAP.new(host: host, port: port, auth: { method: :simple, username: bind_dn, password: password },
              # encryption: { method: :simple_tls, tls_options: OpenSSL::SSL::SSLContext::DEFAULT_PARAMS })
              encryption: { method: :simple_tls })
          else
            ldap = Net::LDAP.new(host: host, port: port, auth: { method: :simple, username: bind_dn, password: password })
          end
          ldap.bind
          APP_LOGGER.debug 'Helpers::OpenLDAP.connect_and_bind: LDAP authentication is succeeded'
        rescue => e
          APP_LOGGER.debug 'Helpers::OpenLDAP.connect_and_bind: LDAP authentication is failed!'
          APP_LOGGER.debug "Helpers::OpenLDAP.connect_and_bind: error: #{e.message}"
          APP_LOGGER.debug "Helpers::OpenLDAP.connect_and_bind: Trace: \t #{e.backtrace.join("\n\t ")}"
          return false
        end
        ldap
      end

      def create_ou(ldap, base_dn, ou)
        dn = "ou=#{ou},#{base_dn}"
        attributes = { ou: ou, objectclass: ['organizationalUnit'] }
        APP_LOGGER.debug "Helpers::OpenLDAP.create_ou: Creating dn: #{dn} attributes: #{attributes}"
        begin
          ldap.add(dn: dn, attributes: attributes)
          true
        rescue => e
          APP_LOGGER.error "Helpers:LDAP:create_ou: error: #{e.message}"
          APP_LOGGER.error e.backtrace.join("\n\t ")
          false
        end
      end

      def dn_exists(ldap, dn)
        APP_LOGGER.debug "Helpers::OpenLDAP.dn_exists: Checking for dn: #{dn}"
        ldap.search(base: dn, return_result: false, scope: Net::LDAP::SearchScope_BaseObject)
      end

      # ldap_uri: ldap://host:port, ldaps://host:port or without the port and ldap(s):// parts
      def extract_uri(ldap_uri)
        ldap_host = 'localhost'
        ldap_port = 389
    
        a = ldap_uri.split(':')
        encrypted = false
        x = a[0]
        encrypted = true if x.length == 5

        return { host: ldap_uri, port: ldap_port, encrypted: encrypted } if a.count == 1
    
        if a.count == 3
          ldap_port = a[2]
          ldap_host = a[1].split('//')[1]
        else
          ldap_host = a[1].split('//')[1]
          if a[0] == 'ldaps'
            ldap_port = 636
          end
        end
        return { host: ldap_host, port: ldap_port, encrypted: encrypted }
      end

      def init_ou(ldap, base_dn, ou)
        return true if dn_exists(ldap, "ou=#{ou},#{base_dn}")
        APP_LOGGER.debug "Helpers::OpenLDAP.init_ou: Initializing ou: #{ou} base_dn: #{base_dn}..."
        # return false unless create_ou(ldap, base_dn, ou)
        # true
        create_ou(ldap, base_dn, ou)
      end
    end
  end
end
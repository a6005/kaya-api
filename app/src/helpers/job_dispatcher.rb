# frozen_string_literal: true

module Helpers
  module JobDispatcher
    class << self
      def add_email_list_member(user_id, email_list_member)
        begin
          pipeline = Pipeline.create(name: 'add_email_list_member', user_id: user_id, status: :created)
          %w[google yandex].each do |k|
            send "add_email_list_member_#{k}", email_list_member, pipeline
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:add_email_list_member: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def create_email_list(user_id, email_list)
        begin
          pipeline = Pipeline.create(name: 'create_email_list', user_id: user_id, status: :created)
          %w[google yandex].each do |k|
            send "create_email_list_#{k}", email_list, pipeline
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:create_email_list: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def create_user(user_id, user, email_only = false)
        pipline_workers = %w[google ldap yandex]
        if email_only
          # When email is changed email accounts are not updated. The old email
          # account is disabled and a new account is created instead.
          # This is the case for just running create_user with email_only option
          pipline_workers = %w[google yandex]
        end
        
        begin
          pipeline = Pipeline.create(name: 'create_user', user_id: user_id, status: :created)
          pipline_workers.each do |k|
            send "create_user_#{k}", user, pipeline 
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:create_user: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def delete_email_list(user_id, email_list)
        begin
          pipeline = Pipeline.create(name: 'delete_email_list', user_id: user_id, status: :created)
          %w[google yandex].each do |k|
            send "delete_email_list_#{k}", email_list, pipeline 
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:delete_email_list: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def delete_security_group(user_id, company_code, security_group_name)
        begin
          pipeline = Pipeline.create(name: 'delete_security_group', user_id: user_id, status: :created)
          %w[ldap].each do |k|
            send "delete_security_group_#{k}", company_code, security_group_name, pipeline 
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:delete_security_group: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end
 
      def delete_user(user_id, company_code, user)
        begin
          pipeline = Pipeline.create(name: 'delete_user', user_id: user_id, status: :created)
          
          # for ldap
          if user.has_ldap?
            create_job(pipeline.id, 'LdapUserDeleter', { company_code: company_code, username: user.os_username })
          end

          # for emial systems
          domain = user.email.split('@').last
          %w[google yandex].each do |k|
            send "delete_user_#{k}", user, domain, pipeline 
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:delete_user: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def delete_user_google(user, domain, pipeline)
        if user.has_google? && Domain.find(name: domain).can_sync?
          create_job(pipeline.id, 'GoogleUserDeleter', { domain: domain, email: user.email })
        end
      end

      def delete_user_yandex(user, domain, pipeline)
        if user.has_yandex? && Domain.find(name: domain).can_sync?
          create_job(pipeline.id, 'YandexUserDeleter', { domain: domain, email: user.email })
        end
      end
 
      def delete_user_by_username(user_id, company_code, username)
        begin
          pipeline = Pipeline.create(name: 'delete_user', user_id: user_id, status: :created)

          # for ldap
          create_job(pipeline.id, 'LdapUserDeleter', { company_code: company_code, username: username })
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:delete_user_by_username: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def disable_email(user_id, company_code, email)
        begin
          pipeline = Pipeline.create(name: 'disable_email', user_id: user_id, status: :created)

          domain = email.split('@').last
          # for yandex
          if Domain.find(name: domain).can_sync?
            create_job(pipeline.id, 'YandexUserDisabler', { domain: domain, email: email })
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:disable_email: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def remove_email_list_member(user_id, email_list_member)
        begin
          pipeline = Pipeline.create(name: 'remove_email_list_member', user_id: user_id, status: :created)
          %w[google yandex].each do |k|
            send "remove_email_list_member_#{k}", email_list_member, pipeline
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:remove_email_list_member: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def reset_password(user, password)
        begin
          pipeline = Pipeline.create(name: 'reset_password', user_id: user.id, status: :created)
          %w[google ldap yandex].each do |k|
            send "reset_password_#{k}", user, pipeline, password 
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:reset_password: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def update_email_list_member(user_id, email_list_member)
        begin
          pipeline = Pipeline.create(name: 'update_email_list_member', user_id: user_id, status: :created)
          %w[google yandex].each do |k|
            send "update_email_list_member_#{k}", email_list_member, pipeline
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:update_email_list_member: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def update_security_group(user_id, security_group)
        begin
          pipeline = Pipeline.create(name: 'update_security_group', user_id: user_id, status: :created)
          %w[ldap].each do |k|
            send "update_security_group_#{k}", security_group, pipeline 
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:update_security_group: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      def update_user(user_id, user, email_changed = false)
        pipline_workers = %w[google ldap yandex]
        if email_changed
          # When email is changed do not update email systems. The old email
          # account is disabled and a new account is created instead.
          pipline_workers = %w[ldap]
        end

        begin
          pipeline = Pipeline.create(name: 'update_user', user_id: user_id, status: :created)
          pipline_workers.each do |k|
            send "update_user_#{k}", user, pipeline 
          end
        rescue => e
          APP_LOGGER.error("Helpers:JobDispatcher:update_user: error: #{e}")
          APP_LOGGER.error "#{e.backtrace.join("\n\t ")}"
        end
      end

      private

      def create_job(pipeline_id, worker, data)
        begin
         job = Job.create(pipeline_id: pipeline_id, worker: worker, data: data)
         job.enqueue if job
        rescue => e
          APP_LOGGER.error "Helpers:JobDsipatcher:create_job: Could not start worker: #{worker} for pipeline: #{pipeline_id} \n error: #{e.message}"
          APP_LOGGER.error "\t #{e.backtrace.join("\n\t ")}"
        end
      end
 
      def create_email_list_google(email_list, pipeline)
        if email_list.is_google? && email_list.domain.can_sync?
          create_job(pipeline.id, 'GoogleEmailListCreator',
            { domain: email_list.domain.name, email: email_list.email,
              name: email_list.name, description: email_list.description })
        end
      end
      
      def create_email_list_yandex(email_list, pipeline)
        if email_list.is_yandex? && email_list.domain.can_sync?
          create_job(pipeline.id, 'YandexEmailListCreator',
            { domain: email_list.domain.name, email: email_list.email,
              name: email_list.name, description: email_list.description })
        end
      end

      def add_email_list_member_google(email_list_member, pipeline)
        APP_LOGGER.debug "Helpers:JobDispatcher:add_email_list_member_google: starting"
        if email_list_member.email_list.is_google? && email_list_member.email_list.domain.can_sync?
          APP_LOGGER.debug "Helpers:JobDispatcher:add_email_list_member_google: creating job"
          create_job(pipeline.id, 'GoogleEmailListMemberCreator',
            { domain: email_list_member.email_list.domain.name, email_list_email: email_list_member.email_list.email,
              email: email_list_member.email.email, can_send_on_behalf: email_list_member.can_send_on_behalf })
        end
      end

      def add_email_list_member_yandex(email_list_member, pipeline)
        APP_LOGGER.debug "Helpers:JobDispatcher:add_email_list_member_yandex: starting"
        if email_list_member.email_list.is_yandex? && email_list_member.email_list.domain.can_sync?
          APP_LOGGER.debug "Helpers:JobDispatcher:add_email_list_member_yandex: creating job"
          create_job(pipeline.id, 'YandexEmailListMemberCreator',
            { domain: email_list_member.email_list.domain.name, email_list_email: email_list_member.email_list.email,
              email: email_list_member.email.email, can_send_on_behalf: email_list_member.can_send_on_behalf })
        end
      end

      def create_user_google(user, pipeline)
        domain = user.email.split('@').last
        if user.has_google? && Domain.find(name: domain).can_sync?
          password = user.password
          if password.nil?
            # This is when a new email account is created during email change.
            # That is why the user.password is nil. The admin must set a password afterwards.
            password = SecureRandom.alphanumeric(MIN_PASSWORD_LENGTH)
          end
          create_job(pipeline.id, 'GoogleUserCreator',
            { domain: domain, email: user.email, password: password, name: user.name, surname: user.surname })
        end
      end

      def create_user_ldap(user, pipeline)
        return unless user.has_ldap?
        create_job(pipeline.id, 'LdapUserCreator', { user_id: user.id })
      end

      def create_user_yandex(user, pipeline)
        domain = user.email.split('@').last
        if user.has_yandex? && Domain.find(name: domain).can_sync?
          password = user.password
          if password.nil?
            # This is when a new email account is created during email change.
            # That is why the user.password is nil. The admin must set a password afterwards.
            password = SecureRandom.alphanumeric(MIN_PASSWORD_LENGTH)
          end
          create_job(pipeline.id, 'YandexUserCreator',
            { domain: domain, email: user.email, password: password, name: user.name, surname: user.surname })
        end
      end

      def delete_email_list_google(email_list, pipeline)
        if email_list.is_google? && email_list.domain.can_sync?
          create_job(pipeline.id, 'GoogleEmailListDeleter', { domain: email_list.domain.name, email: email_list.email })
        end
      end
      
      def delete_email_list_yandex(email_list, pipeline)
        if email_list.is_yandex? && email_list.domain.can_sync?
          create_job(pipeline.id, 'YandexEmailListDeleter', { domain: email_list.domain.name, email: email_list.email })
        end
      end

      def delete_security_group_ldap(company_code, security_group_name, pipeline)
        create_job(pipeline.id, 'LdapSecurityGroupDeleter', { company_code: company_code, group_name: security_group_name })
      end
 
      def remove_email_list_member_google(email_list_member, pipeline)
        APP_LOGGER.debug "Helpers:JobDispatcher:remove_email_list_member_google: starting"
        if email_list_member.email_list.is_google? && email_list_member.email_list.domain.can_sync?
          APP_LOGGER.debug "Helpers:JobDispatcher:remove_email_list_member_google: creating job"
          create_job(pipeline.id, 'GoogleEmailListMemberDeleter',
            { domain: email_list_member.email_list.domain.name, email_list_email: email_list_member.email_list.email,
              email: email_list_member.email.email, can_send_on_behalf: email_list_member.can_send_on_behalf })
        end
      end

      def remove_email_list_member_yandex(email_list_member, pipeline)
        APP_LOGGER.debug "Helpers:JobDispatcher:remove_email_list_member_yandex: starting"
        if email_list_member.email_list.is_yandex? && email_list_member.email_list.domain.can_sync?
          APP_LOGGER.debug "Helpers:JobDispatcher:remove_email_list_member_yandex: creating job"
          create_job(pipeline.id, 'YandexEmailListMemberDeleter',
            { domain: email_list_member.email_list.domain.name, email_list_email: email_list_member.email_list.email,
              email: email_list_member.email.email, can_send_on_behalf: email_list_member.can_send_on_behalf })
        end
      end

      def reset_password_ldap(user, pipeline, password)
        return unless user.has_ldap?
        create_job(pipeline.id, 'LdapPasswordResetter', { user_id: user.id, password: password })
      end
      
      def reset_password_google(user, pipeline, password)
        APP_LOGGER.debug "Helpers:JobDispatcher:reset_password_google: starting"
        domain = user.email.split('@').last
        APP_LOGGER.debug "Helpers:JobDispatcher:reset_password_google: domain: #{domain} has google: #{user.has_google?} can sync: #{Domain.find(name: domain).can_sync?}"
        if user.has_google? && Domain.find(name: domain).can_sync?
          create_job(pipeline.id, 'GooglePasswordResetter', { domain: domain, email: user.email, password: password })
        end
      end
                
      def reset_password_yandex(user, pipeline, password)
        domain = user.email.split('@').last
        if user.has_yandex? && Domain.find(name: domain).can_sync?
          create_job(pipeline.id, 'YandexPasswordResetter', { domain: domain, email: user.email, password: password })
        end
      end

      def update_email_list_member_google(email_list_member, pipeline)
        APP_LOGGER.debug "Helpers:JobDispatcher:update_email_list_member_google: starting"
        if email_list_member.email_list.is_google? && email_list_member.email_list.domain.can_sync?
          APP_LOGGER.debug "Helpers:JobDispatcher:update_email_list_member_google: creating job"
          create_job(pipeline.id, 'GoogleEmailListMemberUpdater',
            { domain: email_list_member.email_list.domain.name, email_list_email: email_list_member.email_list.email,
              email: email_list_member.email.email, can_send_on_behalf: email_list_member.can_send_on_behalf })
        end
      end

      def update_email_list_member_yandex(email_list_member, pipeline)
        APP_LOGGER.debug "Helpers:JobDispatcher:update_email_list_member_yandex: starting"
        if email_list_member.email_list.is_yandex? && email_list_member.email_list.domain.can_sync?
          APP_LOGGER.debug "Helpers:JobDispatcher:update_email_list_member_yandex: creating job"
          create_job(pipeline.id, 'YandexEmailListMemberUpdater',
            { domain: email_list_member.email_list.domain.name, email_list_email: email_list_member.email_list.email,
              email: email_list_member.email.email, can_send_on_behalf: email_list_member.can_send_on_behalf })
        end
      end

      def update_security_group_ldap(security_group, pipeline)
        create_job(pipeline.id, 'LdapSecurityGroupUpdater', { security_group_id: security_group.id })
      end

      def update_user_google(user, pipeline)
        domain = user.email.split('@').last
        if user.has_google? && Domain.find(name: domain).can_sync?
          create_job(pipeline.id, 'GoogleUserUpdater', 
            { domain: domain, email: user.email, name: user.name, surname: user.surname, enabled: user.enabled })
        end
      end

      def update_user_ldap(user, pipeline)
        return unless user.has_ldap?
        create_job(pipeline.id, 'LdapUserUpdater', { user_id: user.id })
      end

      def update_user_yandex(user, pipeline)
        domain = user.email.split('@').last
        if user.has_yandex? && Domain.find(name: domain).can_sync?
          create_job(pipeline.id, 'YandexUserUpdater',
            { domain: domain, email: user.email, name: user.name, surname: user.surname, enabled: user.enabled })
        end
      end
    end
  end
end
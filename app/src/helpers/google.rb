# frozen_string_literal: true

# gem install google-api-client
# https://github.com/googleapis/google-api-ruby-client/blob/master/generated/google-apis-admin_directory_v1/lib/google/apis/admin_directory_v1/service.rb
# https://googleapis.dev/ruby/google-api-client/v0.53.0/Google/Apis/AdminDirectoryV1.htm

require 'google/apis/admin_directory_v1'
require 'googleauth'

module Helpers
  module Google
    class << self
      def add_email_list_member(file_name, admin_email, email_list_email, email)
        m = {
          :email => email
        }
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          # there was a warning when hash is given as a last argument to this method
          # as a workaround fields is given, the default was nil for fields argument anyway
          response = service.insert_member(email_list_email, m, fields: nil)
        rescue => e
          APP_LOGGER.error "Helpers:Google:add_email_list_member: error: #{e}"
        end
        return response ? true : false
      end
      
      def create_email_list(file_name, admin_email, email, name, description)
        email_hash = {
          :email => email,
          :name => name,
          :description => description
       }
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          # there was a warning when hash is given as a last argument to this method
          # as a workaround fields is given, the default was nil for fields argument anyway
          response = service.insert_group(email_hash, fields: nil)
        rescue => e
          APP_LOGGER.error "Helpers:Google:create_email_list: error: #{e}"
        end
        return response ? true : false
      end
            
      def create_user(file_name, admin_email, email, password, first_name, last_name)
        user_hash = {
          :primary_email => email,
          :name => {
            :given_name => first_name,
            :family_name => last_name
          },
          :password => password,
          :suspended => false,
          :change_password_at_next_login => false
        }
        APP_LOGGER.debug "Helpers:Google:create_user: user hash: #{user_hash}"
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          # there was a warning when hash is given as a last argument to this method
          # as a workaround fields is given, the default was nil for fields argument anyway
          response = service.insert_user(user_hash, fields: nil)
        rescue => e
          APP_LOGGER.error "Helpers:Google:create_user: error: #{e}"
        end
        return response ? true : false
      end
      
      def delete_email_list(file_name, admin_email, email)
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          response = service.delete_group(email)
        rescue => e
          APP_LOGGER.error "Helpers:Google:delete_email_list: error: #{e}"
        end
        return response ? true : false
      end
       
      def delete_user(file_name, admin_email, email)
        APP_LOGGER.debug "Helpers:Google:delete_user: user email: #{email}"
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          response = service.delete_user(email)
        rescue => e
          APP_LOGGER.error "Helpers:Google:delete_user: error: #{e}"
        end
        return response ? true : false
      end

      def disable_user(file_name, admin_email, email)
        user_hash = { :primary_email => email, :suspended => true }
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          # there was a warning when hash is given as a last argument to this method
          # as a workaround fields is given, the default was nil for fields argument anyway
          response = service.update_user(email, user_hash, fields: nil)
        rescue => e
          APP_LOGGER.error "Helpers:Google:disable_user: error: #{e}"
        end
        return response ? true : false
      end
      
      def enable_user(service, email)
        user_hash = { :primary_email => email, :suspended => false }
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          # there was a warning when hash is given as a last argument to this method
          # as a workaround fields is given, the default was nil for fields argument anyway
          response = service.update_user(email, user_hash, fields: nil)
        rescue => e
          APP_LOGGER.error "Helpers:Google:enable_user: error: #{e}"
        end
        return response ? true : false
      end
            
      def get_email_lists(file_name, admin_email, domain)
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          response = service.list_groups(domain: domain)
        rescue => e
          APP_LOGGER.error "Helpers:Google:get_email_lists: error: #{e}"
        end
        return response ? response.groups : nil
      end
 
      def get_email_list_members(file_name, admin_email, domain, email)
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          response = service.list_members(email)
        rescue => e
          APP_LOGGER.error "Helpers:Google:get_email_list_members: error: #{e}"
        end
        return response ? response.members : nil
      end

      def remove_email_list_member(file_name, admin_email, domain, email_list_email, email)
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          response = service.delete_member(email_list_email, email)
        rescue => e
          APP_LOGGER.error "Helpers:Google:remove_email_list_member: error: #{e}"
        end
        return response ? true : false
      end
     
      def reset_password(file_name, admin_email, email, password)
        user_hash = { :primary_email => email, :password => password }
        # APP_LOGGER.debug "Helpers:Google:reset_password: Resetting password #{user_hash}..."
        begin
          # APP_LOGGER.debug "Helpers:Google:reset_password: calling get_service..."
          service = get_service(file_name, admin_email)
          return false unless service
          # APP_LOGGER.debug "Helpers:Google:reset_password: calling service.update_user..."
          # there was a warning when hash is given as a last argument to this method
          # as a workaround fields is given, the default was nil for fields argument anyway
          response = service.update_user(email, user_hash, fields: nil)
          # APP_LOGGER.debug "Helpers:Google:reset_password: response #{response}"
        rescue => e
          APP_LOGGER.error "Helpers:Google:reset_password: error: #{e}"
        end
        return response ? true : false
      end
 
      def update_user(file_name, admin_email, email, first_name, last_name, enabled)
        user_hash = {
          :name => { :given_name => first_name, :family_name => last_name },
          :suspended => enabled ? false : true
        }
        APP_LOGGER.debug "Helpers:Google:update_user: user hash: #{user_hash}"
        begin
          service = get_service(file_name, admin_email)
          return false unless service
          # there was a warning when hash is given as a last argument to this method
          # as a workaround fields is given, the default was nil for fields argument anyway
          response = service.update_user(email, user_hash, fields: nil)
        rescue => e
          APP_LOGGER.error "Helpers:Google:update_user: error: #{e}"
        end
        return response ? true : false
      end
    
      private

      def get_service(file_name, admin_email)
        # https://developers.google.com/admin-sdk/directory/v1/guides/delegation
        # https://googleapis.dev/ruby/google-api-client/latest/Google/Apis/AdminDirectoryV1.html
        begin
          scopes = [
	          ::Google::Apis::AdminDirectoryV1::AUTH_ADMIN_DIRECTORY_USER,
	          ::Google::Apis::AdminDirectoryV1::AUTH_ADMIN_DIRECTORY_GROUP,
          	::Google::Apis::AdminDirectoryV1::AUTH_ADMIN_DIRECTORY_GROUP_MEMBER
          ]
          # APP_LOGGER.debug "Helpers:Google:get_service: trying to get a service object 2"
          service = ::Google::Apis::AdminDirectoryV1::DirectoryService.new
          # APP_LOGGER.debug "Helpers:Google:get_service: got a service object"
          return false unless File.file?(file_name)
          service.authorization = ::Google::Auth::ServiceAccountCredentials.make_creds(
            json_key_io: File.open(file_name), scope: scopes)
          # APP_LOGGER.debug "Helpers:Google:get_service: authorized the service object"
          service.authorization.sub = admin_email
          # APP_LOGGER.debug "Helpers:Google:get_service: set admin email to #{admin_email}"
          # APP_LOGGER.debug "Helpers:Google:get_service: authorization: #{service.authorization}"
          # APP_LOGGER.debug "Helpers:Google:get_service: authorization uri: #{service.authorization.authorization_uri}"
          # APP_LOGGER.debug "Helpers:Google:get_service: token credential uri: #{service.authorization.token_credential_uri}"
          # APP_LOGGER.debug "Helpers:Google:get_service: client id: #{service.authorization.client_id}"
          # APP_LOGGER.debug "Helpers:Google:get_service: redirect uri: #{service.authorization.redirect_uri}"
          # APP_LOGGER.debug "Helpers:Google:get_service: scope: #{service.authorization.scope}"
          # APP_LOGGER.debug "Helpers:Google:get_service: access type: #{service.authorization.access_type}"
          # APP_LOGGER.debug "Helpers:Google:get_service: authorization access token: #{service.authorization.access_token}"
          # t = service.authorization.fetch_access_token!
          service.authorization.fetch_access_token!
          # APP_LOGGER.debug "Helpers:Google:get_service: token: #{t}"
          # APP_LOGGER.debug "Helpers:Google:get_service: OK"
          return service
        rescue => e
          APP_LOGGER.error "Helpers:Google:get_service: error: #{e}"
        end
        return false
      end
    end
  end
end
# frozen_string_literal: true

require 'yandex-pdd-2'

module Helpers
  module Yandex
    class << self
      def add_email_list_member(token, domain, email_list_email, email, can_send_on_behalf = false)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client

        h = { domain: domain, maillist: email_list_email, subscriber: email, can_send_on_behalf: can_send_on_behalf }
        APP_LOGGER.debug "Helpers:Yandex:add_email_list_member: Adding #{h}"
        x = client.subscription_add(h)
        APP_LOGGER.debug "Helpers:Yandex:add_email_list_member: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:add_email_list_member: failed: #{x}"
          return false
        end
        return true
      end
      
      def create_email_list(token, domain, email)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client

        h = { domain: domain, maillist: email }
        APP_LOGGER.debug "Helpers:Yandex:create_email_list: Creating email list #{h}..."
        x = client.maillist_add(h)
        APP_LOGGER.debug "Helpers:Yandex:create_email_list: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:create_email_list: failed: #{x}"
          return false
        end
        return x['uid']
      end
      
      def create_user(token, domain, email, password, first_name, last_name)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client

        login = email.split('@').first
        h = { domain: domain, login: login, password: password }
        APP_LOGGER.debug "Helpers:Yandex:create_user: Creating #{h}..."
        x = client.mailbox_add(h)
        APP_LOGGER.debug "Helpers:Yandex:create_user: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:create_user: failed: #{x}"
          return false
        end
        return update_user(token, domain, email, first_name, last_name, true)
      end
      
      def delete_email_list(token, domain, email)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client
        
        h = { domain: domain, maillist: email }
        APP_LOGGER.debug "Helpers:Yandex:delete_mail_list: Deleting email list #{h}..."
        x = client.maillist_delete(h)
        APP_LOGGER.debug "Helpers:Yandex:delete_email_list: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:delete_email_list: failed: #{x}"
          return false
        end
        return true
      end
      
      def delete_user(token, domain, email)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client
        
        login = email.split('@').first
        h = { domain: domain, login: login }
        APP_LOGGER.debug "Helpers:Yandex:delete_user: Deleting #{h}..."
        x = client.mailbox_delete(h)
        APP_LOGGER.debug "Helpers:Yandex:delete_user: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:delete_user: failed: #{x}"
          return false
        end
        return true
      end
      
      def disable_user(token, domain, email)
        APP_LOGGER.debug "Helpers:Yandex:disable_user: Disabling #{email}..."
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client
        
        h = { domain: domain, login: email, enabled: false }
        APP_LOGGER.debug "Helpers:Yandex:disable_user: Disabling #{h}..."
        x = client.mailbox_edit(h)
        APP_LOGGER.debug "Helpers:Yandex:disable_user: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:disable_user: failed: #{x}"
          return false
        end
        return true
      end
      
      def enable_user(token, domain, email)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client
        
        h = { domain: domain, login: email, enabled: true }
        APP_LOGGER.debug "Helpers:Yandex:enable_user: Enabling #{h}..."
        x = client.mailbox_edit(h)
        APP_LOGGER.debug "Helpers:Yandex:enable_user: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:enable_user: failed: #{x}"
          return false
        end
        return true
      end
      
      def get_email_lists(token, domain)
        client = ::Yandex::Pdd::Client.new(token)
        # client = connect(token)
        return false unless client
        
        APP_LOGGER.debug "Helpers:Yandex:get_email_lists: Getting mail lists #{domain}..."
        x = client.maillist_list(domain)
        APP_LOGGER.debug "Helpers:Yandex:get_email_lists: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:get_email_lists: failed: #{x}"
          return false
        end
        return x['maillists']
      end
      
      def get_email_list_members(token, domain, email)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client
        
        APP_LOGGER.debug "Helpers:Yandex:get_subscribers: Getting subscribers for #{email}..."
        x = client.subscription_sublist(domain, email)
        APP_LOGGER.debug "Helpers:Yandex:get_subscribers: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:get_subscribers: failed: #{x}"
          return false
        end
        return x['subscribers']
      end
      
      def remove_email_list_member(token, domain, mail_list_email, email)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client
        
        h = { domain: domain, maillist: mail_list_email, subscriber: email }
        APP_LOGGER.debug "Helpers:Yandex:remove_subscriber: Removing subscriber #{h}"
        x = client.subscription_destroy(h)
        APP_LOGGER.debug "Helpers:Yandex:remove_subscriber: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:remove_subscriber: failed: #{x}"
          return false
        end
        return true
      end
      
      def reset_password(token, domain, email, password)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client

        h = { domain: domain, login: email, password: password }
        APP_LOGGER.debug "Helpers:Yandex:reset_password: Resetting password #{h}..."
        x = client.mailbox_edit(h)
        APP_LOGGER.debug "Helpers:Yandex:reset_password: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:reset_password: failed: #{x}"
          return false
        end
        return true
      end

      def update_email_list_member(token, domain, email_list_email, email, can_send_on_behalf = false)
        h = { domain: domain, maillist: email_list_email, subscriber: email, can_send_on_behalf: can_send_on_behalf }
        APP_LOGGER.debug "Helpers:Yandex:update_email_list_member: Removing #{h}"
        remove_email_list_member(token, domain, email_list_email, email)
        return add_email_list_member(token, domain, email_list_email, email, can_send_on_behalf)
      end

      def update_user(token, domain, email, first_name, last_name, enabled)
        client = ::Yandex::Pdd::Client.new(token)
        return false unless client

        h = { domain: domain, login: email, iname: first_name, fname: last_name, enabled: enabled }
        APP_LOGGER.debug "Helpers:Yandex:update_user: Updating #{h}..."
        x = client.mailbox_edit(h)
        APP_LOGGER.debug "Helpers:Yandex:update_user: result: #{x}"
        if x['success'] != 'ok'
          APP_LOGGER.debug "Helpers:Yandex:update_user: failed: #{x}"
          return false
        end
        return true
      end
      
      private

      def connect(token) 
        return Yandex::Pdd::Client.new(token)
      end
    end
  end
end
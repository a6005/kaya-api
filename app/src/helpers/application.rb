module Helpers
  module Application
    def k_response(http:, result: nil, kaya: nil, error: nil)
      if error
        APP_LOGGER.error "Helpers:Application::k_response: #{error.class}: #{error.message}\n"
        APP_LOGGER.error "Helpers:Application::k_response: #{error.backtrace.join("\n\t ")}"
      end 

      out = {}
      if result
        out = result
      else
        msg = nil
        msg = KAYA_CODE[kaya] if kaya
        msg += " #{error.message}".gsub("\n", ' ') if error
        out[:message] = msg if msg 
      end

      if out.empty? && http != :ok
        [KAYA_HTTP[http]]
      else
        [KAYA_HTTP[http], out.to_json]
      end
    end
    
    def try_exec
      begin 
        yield
      rescue => e
        errors.add(:unhandled_error, e.message)
        APP_LOGGER.error "Helpers:Application::try_exec: #{e.message}\n"
        APP_LOGGER.error "Helpers:Application::try_exec: #{e.backtrace.join("\n\t ")}"
      end
    end
  end
end
# frozen_string_literal: true

require 'apigen/formats/openapi'
require 'apigen/formats/example'
require 'yaml'

describe Apigen::Formats::OpenAPI::V3 do
  it 'generates expected output' do
    generated = Apigen::Formats::OpenAPI::V3.generate(Apigen.example)
    File.write('spec/apigen/formats/openapi_generated.yml',generated)
    expect(generated).to eq(YAML.load_file("spec/apigen/formats/openapi_expected.yml").to_yaml)
  end

  it 'fails with void type' do
    api = Apigen::Rest::Api.new
    api.model :user do
      type :object do
        name :void
      end
    end
    expect { Apigen::Formats::OpenAPI::V3.generate(api) }.to raise_error 'Unsupported primary type :void.'
  end

  it 'fails with unknown type' do
    api = Apigen::Rest::Api.new
    api.model :user do
      type :object do
        name :string
      end
    end
    api.models[:user].type.properties[:name] = Apigen::ObjectProperty.new('not a type')
    expect { Apigen::Formats::OpenAPI::V3.generate(api) }.to raise_error 'Unsupported type: not a type.'
  end
end

# frozen_string_literal: true

require 'yaml'
require_relative './json_base'

module Apigen
  module Formats
    module OpenAPI
      ##
      # OpenAPI 3 generator.
      module V3
        class << self
          include Apigen::Formats::JsonBase

          def generate(api)
            # TODO: Allow overriding any of the hardcoded elements.
            hash = {'openapi' => '3.0.2'}
            hash.merge! 'info' => info(api) if info(api)
            hash.merge! 'servers' => servers(api.servers) if servers(api.servers)
            hash.merge! 'tags' => tags(api.tags) if tags(api.tags)
            hash.merge! 'paths' => paths(api) 
            hash.merge! 'components' => {'schemas' => definitions(api)} if definitions(api)
            hash['components'].merge! 'securitySchemes' => security_definitions(api) if security_definitions(api)
            hash.to_yaml
          end

          private
          def info(api)
            hash = {}
            hash.merge! 'version' => api.version if api.version 
            hash.merge! 'title' => api.title if api.title
            hash.merge! 'description' => api.description if api.description
            hash.merge! 'termsOfService' => api.terms_of_service if api.terms_of_service
            hash.merge! 'contact' => contact(api.contact) if contact(api.contact)
            hash.merge! 'license' => license(api.license) if license(api.license)
            hash.empty? ? nil : hash
          end
          
          def contact(c)
            hash = {}
            hash.merge! 'name' => c.name if c.name  
            hash.merge! 'email' => c.email if c.email
            hash.merge! 'url' => c.url if c.url
            hash.empty? ? nil : hash
          end
          
          def license(l)
            hash = {}
            hash.merge! 'name' => l.name if l.name 
            hash.merge! 'url' => l.url if l.url
            hash.empty? ? nil : hash         
          end
          
          def servers(s)
            array = []
            s.each do |e|
              hash = {}
              hash.merge! 'url' => e.url if e.url 
              hash.merge! 'description' => e.description if e.description
              array << hash  unless hash.empty?
            end            
            array.empty? ? nil : array
          end

          def tags(t)
            array = []
            t.each do |e|
              hash = {}
              hash.merge! 'name' => e.name if e.name 
              hash.merge! 'description' => e.description if e.description
              array << hash  unless hash.empty?
            end            
            array.empty? ? nil : array
          end

          def paths(api)
            hash = {}
            api.endpoints.each do |endpoint|
              parameters = []
              parameters.concat(endpoint.path_parameters.properties.map { |name, property| path_parameter(api, name, property) })
              parameters.concat(endpoint.query_parameters.properties.map { |name, property| query_parameter(api, name, property) })
              parameters.concat(endpoint.header_parameters.properties.map { |name, property| header_parameter(api, name, property) })
              responses = endpoint.outputs.map { |output| response(api, output) }.to_h
              operation = {
                'operationId' => endpoint.name.to_s,
                'parameters' => parameters,
                'responses' => responses
              }
              add_description(operation, endpoint.description)
              operation['requestBody'] = input(api, endpoint.input) if endpoint.input
              operation['tags'] = endpoint.tags.map {|t| t.name} if endpoint.tags
              operation['security'] = [{'bearerAuth' => []}] if endpoint.security=="bearer"
              hash[endpoint.path] ||= {}
              hash[endpoint.path][endpoint.method.to_s] = operation
            end
            hash
          end

          def path_parameter(api, name, property)
            parameter = {
              'in' => 'path',
              'name' => name.to_s,
              'required' => true,
              'schema' => schema(api, property.type)
            }
            add_description(parameter, property.description)
            add_example(parameter, property.example)
            parameter
          end

          def query_parameter(api, name, property)
            parameter = {
              'in' => 'query',
              'name' => name.to_s,
              'required' => property.required?,
              'schema' => schema(api, property.type)
            }
            add_description(parameter, property.description)
            add_example(parameter, property.example)
            parameter
          end

          def header_parameter(api, name, property)
            parameter = {
              'in' => 'header',
              'name' => name.to_s,
              'required' => property.required?,
              'schema' => schema(api, property.type)
            }
            add_description(parameter, property.description)
            add_example(parameter, property.example)
            parameter
          end

          def input(api, property)
            parameter = {
              'required' => true,
              'content' => {
                'application/json' => {
                  'schema' => schema(api, property.type)
                }
              }
            }
            add_description(parameter, property.description)
            add_example(parameter, property.example)
            parameter
          end

          def response(api, output)
            response = {}
            add_description(response, output.description)
            add_example(response, output.example)
            if output.type != Apigen::PrimaryType.new(:void)
              response['content'] = {
                'application/json' => {
                  'schema' => schema(api, output.type)
                }
              }
            end
            [output.status.to_s, response]
          end

          def model_ref(type)
            "#/components/schemas/#{type}"
          end

          def supports_discriminator?
            true
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

module Apigen
    module Rest
        class Tag
            attribute_setter_getter :name
            attribute_setter_getter :description
            def initialize(name=nil,description=nil)
                @name = name
                @description = description
            end
        end
    end
end
# frozen_string_literal: true

module Apigen
    module Rest
        class SecurityScheme
            attribute_setter_getter :type
            attribute_setter_getter :scheme
            attribute_setter_getter :description
            attribute_setter_getter :bearer_format
            def initialize(name=nil,type=nil,description=nil,scheme=nil)
                @name = name
                @type = type
                @description = description
                @scheme = scheme
                @bearer_format = nil
            end
        end
    end
end
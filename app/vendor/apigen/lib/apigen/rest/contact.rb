# frozen_string_literal: true

module Apigen
    module Rest
        class Contact
            attribute_setter_getter :name
            attribute_setter_getter :email
            attribute_setter_getter :url
            def initialize
                @name = nil
                @url = nil
                @email = nil
            end
        end
    end
end
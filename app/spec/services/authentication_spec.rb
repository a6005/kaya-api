# frozen_string_literal: true

require_relative '../../environment.rb'
require APP_ROOT.join('spec', 'spec_helper.rb')

endpoint = 'authentication'
s_name = endpoint

RSpec.describe endpoint do
  include_context 'common'

  let(:url) { "#{api_v1_url}/#{endpoint}" }

  describe 'POST methods' do
    let(:expire_period) { 120 }
    let(:response_blank_body) { conn.post url }
    let(:response_empty_email) { conn.post url, { email: '' }.to_json }
    let(:response_empty_password) { conn.post url, { password: '' }.to_json }
    let(:response_password_empty_email) { conn.post url, { password: test_pw, email: ''}.to_json }
    let(:response_email_empty_password) { conn.post url, { password: '', email: test_email}.to_json }
    let(:response_password_non_email) { conn.post url, { password: test_pw, email: none_existent_email}.to_json }
    let(:response_valid_credentials) { conn.post url, { password: test_pw, email: test_email}.to_json }
    let(:response_valid_credentials_expire) { conn.post url, { password: test_pw, email: test_email, expire_period: expire_period}.to_json }
    #let(:claims) { JWT.decode(JSON.parse(response_valid_credentials.body)['token'], nil, false) }

   it 'should respond with json mime type' do
     # json = JSON.parse(response_blank_body.headers.to_json).with_indifferent_access
     json = JSON.parse(response_blank_body.headers.to_json)
     expect(json[content_type]).to eq(mime_json)
   end

   it 'should not authenticate with blank body' do
     expect(response_blank_body.status).to eq(KAYA_HTTP[:bad_request])
   end

   it 'should not authenticate with empty email' do
     expect(response_empty_email.status).to eq(KAYA_HTTP[:bad_request])
   end

   it 'should not authenticate with empty password' do
     expect(response_empty_password.status).to eq(KAYA_HTTP[:bad_request])
   end

   it 'should not authenticate password and empty email' do
     expect(response_password_empty_email.status).to eq(KAYA_HTTP[:bad_request])
   end

   it 'should not authenticate email with empty password' do
     expect(response_email_empty_password.status).to eq(KAYA_HTTP[:bad_request])
   end

   it 'should not authenticate non existent email with password' do
     expect(response_password_non_email.status).to eq(KAYA_HTTP[:unauthorized])
   end

   it 'should match with JSON Schema' do
     expect(response_valid_credentials).to match_response_schema("#{s_name}_response_object")
   end
   
   it 'should authenticate with test account' do
     expect(response_valid_credentials.status).to eq(KAYA_HTTP[:created])
   end

   it 'should authenticate with test account and return access token' do
     json = JSON.parse(response_valid_credentials.body).with_indifferent_access
     expect(json).to have_key(:token)
   end

  # it 'should respond with valid JWT token' do
  #   expect { JWT.decode(token, key) }.to_not raise_error(JWT::DecodeError)
  # end

   it 'returns a JWT with valid claims' do
      u = User.find(id: test_user_id)
      claims = JWT.decode(token, nil, false)
      h = claims[0]
      expect(h['id']).to eq(u.id)
      expect(h['name']).to match(u.name)
      expect(h['is_admin']).to eq(u.admin)
      expect(h['email']).to match(u.email)
      expect(h['company_id']).to eq(u.company_id)
      expect(h['expires_at']).to be_an(Integer)
      expect(h['issued_at']).to be_an(Integer)
   end
  
   it 'should authenticate with test account and set expire' do
     expect(response_valid_credentials_expire.status).to eq(KAYA_HTTP[:created])
   end
  end
end
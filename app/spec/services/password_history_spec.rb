# frozen_string_literal: true

require_relative '../../environment.rb'
require APP_ROOT.join('spec', 'spec_helper.rb')

endpoint = 'password_history'
s_name = 'password_history'

RSpec.describe endpoint do
  include_context 'common'

  let(:url) { "#{api_v1_url}/users/#{test_user_id}/#{endpoint}" }
  let(:not_found_url) { "#{api_v1_url}/users/99/#{endpoint}" }
  
  describe 'GET methods' do
    let(:get_response_no_token) { conn.get url }
    let(:get_response_valid) { conn.authorization :Bearer, token ; conn.get url }
    let(:get_response_not_found) { conn.authorization :Bearer, token ; conn.get not_found_url }
   
    it 'should respond with HTTP unauthorized code' do
      expect(get_response_no_token.status).to eq(KAYA_HTTP[:unauthorized])
    end

    it 'should respond with content type header' do
      json = JSON.parse(get_response_no_token.headers.to_json)
      expect(json).to have_key(content_type)
    end

    it 'should respond with json' do
      json = JSON.parse(get_response_no_token.headers.to_json)
      expect(json[content_type]).to eq(mime_json)
    end

    it 'should respond with HTTP not found' do
      expect(get_response_not_found.status).to eq(KAYA_HTTP[:not_found])
    end
    
    it 'when not found should respond a message' do
      json = JSON.parse(get_response_not_found.body)
      expect(json).to have_key(message_str)
    end
    
    it 'should respond with HTTP ok code' do
      # generate password history
      user = User.find(id: test_user_id)
      user.current_user = user
      PASSWORD_HISTORY_LENGTH.times do
        pw = "FFaker::Lorem.characters(MIN_PASSWORD_LENGTH)#{rand(1..20)}"
        user.change_password(pw, pw, test_user_id)
        sleep(10)
      end
      # give some time to reset password workers
      sleep(10)
      # reset to original password
      user.change_password(test_pw, test_pw, test_user_id)
      # now get the password history
      expect(get_response_valid.status).to eq(KAYA_HTTP[:ok])
    end

    it 'should match with JSON Schema' do
      expect(get_response_valid).to match_response_schema("#{s_name}_object_array")
    end
  end
end
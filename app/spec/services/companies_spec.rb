# frozen_string_literal: true

require_relative '../../environment.rb'
require APP_ROOT.join('spec', 'spec_helper.rb')

endpoint = 'companies'
s_name = 'company'

RSpec.describe endpoint do
  include_context 'common'

  let(:url) { "#{api_v1_url}/#{endpoint}" }
  
  describe 'GET methods' do
    let(:get_response_no_token) { conn.get url }
    let(:get_response_valid) { conn.authorization :Bearer, token ; conn.get url }
    let(:get_response_not_found) { conn.authorization :Bearer, token ; conn.get url, { name: "Not found #{s_name}" } }
    let(:get_response_valid_empty) { conn.authorization :Bearer, token ; conn.get url, { name: 'not_found', code: 'notfound'} }

    describe 'search path' do
      it 'should respond with HTTP unauthorized code' do
        expect(get_response_no_token.status).to eq(KAYA_HTTP[:unauthorized])
      end

      it 'should respond with content type header' do
        json = JSON.parse(get_response_no_token.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(get_response_no_token.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
      
      #it 'should respond with HTTP not found' do
      #  expect(get_response_not_found.status).to eq(KAYA_HTTP[:not_found])
      #end
      #
      #it 'when not found should respond a message' do
      #  json = JSON.parse(get_response_not_found.body)
      #  expect(json).to have_key(message_str)
      #end

      it 'should respond with HTTP ok code' do
        expect(get_response_valid.status).to eq(KAYA_HTTP[:ok])
      end

      it 'should match with JSON Schema' do
        expect(get_response_valid).to match_response_schema("#{s_name}_response_object_array")
      end
      
      it 'should respond with HTTP ok code for empty list' do
        expect(get_response_valid_empty.status).to eq(KAYA_HTTP[:ok])
      end

      it 'should match with JSON Schema' do
        puts "response: #{get_response_valid_empty.body}"
        expect(get_response_valid_empty.body).to eq(empty_array_json_object)
      end
    end

    describe 'id path' do
      let(:url) { "#{api_v1_url}/#{endpoint}/1" }
      let(:url_not_found) { "#{api_v1_url}/#{endpoint}/999999" }
      let(:get_response_no_token) { conn.get url }
      let(:get_response_valid) { conn.authorization :Bearer, token ; conn.get url }
      let(:get_response_not_found) { conn.authorization :Bearer, token ; conn.get url_not_found }

      it 'should respond with HTTP unauthorized code' do
        expect(get_response_no_token.status).to eq(KAYA_HTTP[:unauthorized])
      end
      
      it 'should respond with content type header' do
        json = JSON.parse(get_response_no_token.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(get_response_no_token.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end

      it 'should respond with HTTP not found' do
        expect(get_response_not_found.status).to eq(KAYA_HTTP[:not_found])
      end
      
      it 'when not found should respond a message' do
        json = JSON.parse(get_response_not_found.body)
        expect(json).to have_key(message_str)
      end
      
      it 'should respond with HTTP ok code' do
        expect(get_response_valid.status).to eq(KAYA_HTTP[:ok])
      end
      
      it 'should match with JSON Schema' do
        expect(get_response_valid).to match_response_schema("#{s_name}_response_object")
      end
    end
  end

  describe 'POST methods' do
    let(:post_payload) { { name: FFaker::Company.name, code: FFaker::Code.ean }.to_json }
    let(:post_payload_invalid) { {'xxxxxxxx' => FFaker::Name.name }.to_json }
    let(:post_payload_unique_key) { { name: Company.first.name, code: Company.first.code }.to_json }
    let(:post_response_valid) { conn.authorization :Bearer, token ;  conn.get url }
    let(:post_response_unique_key) { conn.authorization :Bearer, token ; conn.post url, post_payload_unique_key }
    let(:post_response_test_data) { conn.authorization :Bearer, token ; conn.post url, post_payload }
    let(:post_response_no_header_no_body) { conn.post url }
    let(:post_response_no_header_invalid_body) { conn.post url, post_payload_invalid }
    let(:post_response_no_header_valid_body) { conn.post url, post_payload }
    let(:post_response_no_body_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.post url }
    let(:post_response_no_body_valid_token) { conn.authorization :Bearer, token ; conn.post url }
    let(:post_response_invalid_body_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.post url, post_payload_invalid }
    let(:post_response_valid_token) { conn.authorization :Bearer, token ; conn.post url }
    let(:post_response_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.post url, post_payload }
    let(:post_response_body_no_auth_header) { conn.post url, post_payload }
    let(:post_response_body_auth_header) { conn.post url, post_payload }
    let(:post_response_with_no_body_token) { conn.authorization :Bearer, invalid_token ; conn.post url }
    let(:post_response_auth_header_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.post url }
    let(:conn_no_header_type) { Faraday.new(api_url) }
    let(:post_response_no_header_type) { conn_no_header_type.post url }
    let(:post_response_invalid_body) { conn.authorization :Bearer, token ; conn.post url, post_payload_invalid }

    describe 'JSON responses' do
      it 'header: no, body: no' do
        json = JSON.parse(post_response_no_header_no_body.headers.to_json)
        expect(json).to have_key(content_type)
      end
      
      it 'header: no, body: invalid' do
        json = JSON.parse(post_response_no_header_invalid_body.headers.to_json)
        expect(json).to have_key(content_type)
      end
      
      it 'header: no, body: valid' do
        json = JSON.parse(post_response_no_header_valid_body.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'header: yes, body: no, token: invalid' do
        r = conn.post url
        json = JSON.parse(post_response_no_body_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end
      
      it 'header: yes, body: no, token: valid' do
        json = JSON.parse(post_response_no_body_valid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end
      
      it 'header: yes, body: invalid, token: invalid' do
        json = JSON.parse(post_response_invalid_body_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'header: yes, body: valid, token: invalid' do
        json = JSON.parse(post_response_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end
    end

    describe 'HTTP bad request' do
      it 'no content type in the headers' do
        expect(post_response_no_header_type.status).to eq(KAYA_HTTP[:bad_request])
      end
      
      it 'header: no, body: no' do
        expect(post_response_no_header_no_body.status).to eq(KAYA_HTTP[:bad_request])
      end

      it 'header: no, body: invalid' do
        expect(post_response_no_header_invalid_body.status).to eq(KAYA_HTTP[:bad_request])
      end
      
      it 'header: yes, body: invalid' do
        expect(post_response_invalid_body.status).to eq(KAYA_HTTP[:bad_request])
      end

      it 'should respond with content type header' do
        json = JSON.parse(post_response_invalid_body.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(post_response_invalid_body.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
   
      it 'unique key violation' do
        expect(post_response_unique_key.status).to eq(KAYA_HTTP[:bad_request])
      end

      it 'when unique key violation should respond a message' do
        json = JSON.parse(post_response_unique_key.body)
        expect(json).to have_key(message_str)
      end
    end

    describe 'HTTP unauthorized' do
      it 'invalid token' do
        expect(post_response_invalid_token.status).to eq(KAYA_HTTP[:unauthorized])
      end 

      it 'should respond with content type header' do
        json = JSON.parse(post_response_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(post_response_invalid_token.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
   
      it 'should respond with a message' do
        json = JSON.parse(post_response_invalid_token.body)
        expect(json).to have_key(message_str)
      end
    end

    describe 'HTTP created' do
      it "should create test data and respond with #{KAYA_HTTP[:created]}" do
        expect(post_response_test_data.status).to eq(KAYA_HTTP[:created])
      end

      it 'should create test data to match with JSON Schema' do
        expect(post_response_test_data).to match_response_schema("#{s_name}_response_object")
      end     
 
      it 'should respond with content type header' do
        json = JSON.parse(post_response_test_data.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(post_response_test_data.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
    end
  end  

  describe 'DELETE method' do
    let(:last_id) { Company.last.id }
    let(:next_id) { last_id + 1 }
    let(:not_found_id) { next_id + 2 }
    let(:url) { "#{api_v1_url}/#{endpoint}/#{last_id}" }
    let(:url_id1) { "#{api_v1_url}/#{endpoint}/1" }
    let(:url_not_found) { "#{api_v1_url}/#{endpoint}/#{not_found_id}" }
    let(:delete_response_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.delete url }
    let(:delete_response_bad_request) { conn.authorization :Bearer, token ; conn.delete url_id1 }
    let(:delete_response_not_found) { conn.authorization :Bearer, token ; conn.delete url_not_found }
    let(:delete_response) { conn.authorization :Bearer, token ; conn.delete url }
    
    describe 'HTTP bad request' do
      it "should not delete when used as foreign key" do
        expect(delete_response_bad_request.status).to eq(KAYA_HTTP[:bad_request])
      end
 
      it 'should respond with content type header' do
        json = JSON.parse(delete_response_bad_request.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(delete_response_bad_request.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
      
      it 'should respond a message' do
        json = JSON.parse(delete_response_bad_request.body)
        expect(json).to have_key(message_str)
      end
    end
   
    describe 'HTTP not found' do
      it "should not delete non existent" do
        expect(delete_response_not_found.status).to eq(KAYA_HTTP[:not_found])
      end
   
      it 'should respond with content type header' do
        json = JSON.parse(delete_response_not_found.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(delete_response_not_found.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
   
      it 'when not found should respond a message' do
        json = JSON.parse(delete_response_not_found.body)
        expect(json).to have_key(message_str)
      end
    end

    describe 'HTTP no content:' do
      it "should delete the test data and return #{KAYA_HTTP[:no_content]}" do
        expect(delete_response.status).to eq(KAYA_HTTP[:no_content])
      end
          
      it 'no content type in the header' do
        Company.create(name: FFaker::Company.name, code: FFaker::Code.ean)
        json = JSON.parse(delete_response.headers.to_json)
        expect(json[content_type]).to be_nil
      end
      
      it 'no mime type json in the header' do
        Company.create(name: FFaker::Company.name, code: FFaker::Code.ean)
        json = JSON.parse(delete_response.headers.to_json)
        expect(json[mime_json]).to be_nil
      end
      
      it 'should have empty body' do
        Company.create(name: FFaker::Company.name, code: FFaker::Code.ean)
        expect(delete_response.body).to be_empty
      end
    end
    
    describe 'HTTP unauthorized' do
      it 'invalid token' do
        expect(delete_response_invalid_token.status).to eq(KAYA_HTTP[:unauthorized])
      end 

      it 'should respond with content type header' do
        json = JSON.parse(delete_response_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(delete_response_invalid_token.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
   
      it 'should respond with a message' do
        json = JSON.parse(delete_response_invalid_token.body)
        expect(json).to have_key(message_str)
      end
    end
  end 
  
  describe 'PUT method' do
    let(:last_id) { Company.last.id }
    let(:next_id) { last_id + 1 }
    let(:url_last_id) { "#{api_v1_url}/#{endpoint}/#{last_id}" }
    let(:url_next_id) { "#{api_v1_url}/#{endpoint}/#{next_id}" }
    let(:put_payload) { { name: FFaker::Company.name, code: FFaker::Code.ean }.to_json }
    let(:put_payload_unique_key) { { name: Company.first.name, code: FFaker::Code.ean }.to_json }
    let(:put_payload_test_data) { { name: FFaker::Company.name, code: FFaker::Code.ean }.to_json }
    let(:put_response_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.put url_last_id, put_payload_test_data }
    let(:put_response_unique_key) { conn.authorization :Bearer, token ; conn.put url_last_id, put_payload_unique_key }
    let(:put_response_not_found) { conn.authorization :Bearer, token ; conn.put url_next_id, put_payload }
    let(:put_response_test_data) { conn.authorization :Bearer, token ; conn.put url_last_id, put_payload_test_data }

    describe 'HTTP bad request' do
      it "unique key violation" do
        expect(put_response_unique_key.status).to eq(KAYA_HTTP[:bad_request])
      end
      
      it 'should respond with content type header' do
        json = JSON.parse(put_response_unique_key.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(put_response_unique_key.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
   
      it 'should respond with a message' do
        json = JSON.parse(put_response_unique_key.body)
        expect(json).to have_key(message_str)
      end
    end

    describe 'HTTP not found' do
      it "should not update non existent" do
        expect(put_response_not_found.status).to eq(KAYA_HTTP[:not_found])
      end 

      it 'should respond with content type header' do
        json = JSON.parse(put_response_not_found.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(put_response_not_found.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
   
      it 'should respond with a message' do
        json = JSON.parse(put_response_not_found.body)
        expect(json).to have_key(message_str)
      end
    end

    describe 'HTTP no content' do
      it "should update the test data and return #{KAYA_HTTP[:no_content]}" do
        expect(put_response_test_data.status).to eq(KAYA_HTTP[:no_content])
      end
      
      it 'no content type in the header' do
        json = JSON.parse(put_response_test_data.headers.to_json)
        expect(json).not_to have_key(content_type)
      end
      
      it 'no mime type json in the header' do
        json = JSON.parse(put_response_test_data.headers.to_json)
        expect(json[mime_json]).to be_nil
      end
      
      it 'should have empty body' do
        expect(put_response_test_data.body).to be_empty
      end
    end

    describe 'HTTP unauthorized' do
      it 'invalid token' do
        expect(put_response_invalid_token.status).to eq(KAYA_HTTP[:unauthorized])
      end 

      it 'should respond with content type header' do
        json = JSON.parse(put_response_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with mime type json' do
        json = JSON.parse(put_response_invalid_token.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end
   
      it 'should respond with a message' do
        json = JSON.parse(put_response_invalid_token.body)
        expect(json).to have_key(message_str)
      end
    end
  end 
end
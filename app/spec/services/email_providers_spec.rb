# frozen_string_literal: true

require_relative '../../environment.rb'
require APP_ROOT.join('spec', 'spec_helper.rb')

endpoint = 'email_providers'
s_name = 'email_provider'

RSpec.describe endpoint do
  include_context 'common'

  let(:url) { "#{api_v1_url}/#{endpoint}" }
  
  describe 'GET methods' do
    let(:get_response_no_token) { conn.get url }
    let(:get_response_valid) { conn.authorization :Bearer, token ; conn.get url }
    let(:get_response_not_found) { conn.authorization :Bearer, token ; conn.get url, { name: "Not found #{s_name}" } }
    let(:get_response_valid_empty) { conn.authorization :Bearer, token ; conn.get url, { name: 'not_found'} }

    describe 'search' do   
      it 'should respond with HTTP unauthorized code' do
        expect(get_response_no_token.status).to eq(KAYA_HTTP[:unauthorized])
      end

      it 'should respond with content type header' do
        json = JSON.parse(get_response_no_token.headers.to_json)
        expect(json).to have_key(content_type)
      end

      it 'should respond with json' do
        json = JSON.parse(get_response_no_token.headers.to_json)
        expect(json[content_type]).to eq(mime_json)
      end

      it 'should respond with HTTP ok code' do
        expect(get_response_valid.status).to eq(KAYA_HTTP[:ok])
      end

      it 'should match with JSON Schema' do
        expect(get_response_valid).to match_response_schema("#{s_name}_response_object_array")
      end
      
      it 'should respond with HTTP ok code for empty list' do
        expect(get_response_valid_empty.status).to eq(KAYA_HTTP[:ok])
      end

      it 'should match with JSON Schema' do
        puts "response: #{get_response_valid_empty.body}"
        expect(get_response_valid_empty.body).to eq(empty_array_json_object)
      end
    end

    describe 'id' do   
      let(:url) { "#{api_v1_url}/#{endpoint}/1" }
      let(:url_not_found) { "#{api_v1_url}/#{endpoint}/999999" }
      let(:get_response_valid) { conn.authorization :Bearer, token ;  conn.get url }
      let(:get_response_not_found) { conn.authorization :Bearer, token ;  conn.get url_not_found }

      it 'should match with JSON Schema' do
        expect(get_response_valid).to match_response_schema("#{s_name}_response_object")
      end
       
      it 'should respond with HTTP not found' do
        expect(get_response_not_found.status).to eq(KAYA_HTTP[:not_found])
      end
      
      it 'when not found should respond a message' do
        json = JSON.parse(get_response_not_found.body)
        expect(json).to have_key(message_str)
      end
    end
  end

  describe 'POST methods' do
    let(:post_payload) { {'name' => "Test #{s_name}", implemented: false, allow_external: true }.to_json }
    let(:post_payload_invalid) { {'xxxxxxxx' => "Test #{s_name}" }.to_json }
    let(:post_response_valid) { conn.authorization :Bearer, token ;  conn.get url }
    let(:post_response_test_data) { conn.authorization :Bearer, token ; conn.post url, post_payload }
    let(:post_response_no_header_no_body) { conn.post url }
    let(:post_response_no_header_invalid_body) { conn.post url, post_payload_invalid }
    let(:post_response_no_header_valid_body) { conn.post url, post_payload }
    let(:post_response_no_body_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.post url }
    let(:post_response_no_body_valid_token) { conn.authorization :Bearer, token ; conn.post url }
    let(:post_response_invalid_body_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.post url, post_payload_invalid }
    let(:post_response_valid_token) { conn.authorization :Bearer, token ; conn.post url }
    let(:post_response_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.post url, post_payload }
    let(:post_response_body_no_auth_header) { conn.post url, post_payload }
    let(:post_response_body_auth_header) { conn.post url, post_payload }
    let(:post_response_with_no_body_token) { conn.authorization :Bearer, invalid_token ; conn.post url }
    let(:post_response_auth_header_invalid_token) { conn.authorization :Bearer, invalid_token ; conn.post url }
    let(:conn_no_header_type) { Faraday.new(api_url) }
    let(:post_response_no_header_type) { conn_no_header_type.post url }
    let(:post_response_invalid_body) { conn.authorization :Bearer, token ; conn.post url, post_payload_invalid }

    describe 'JSON responses' do
      it 'header: no, body: no' do
        json = JSON.parse(post_response_no_header_no_body.headers.to_json)
        expect(json).to have_key(content_type)
      end
      it 'header: no, body: invalid' do
        json = JSON.parse(post_response_no_header_invalid_body.headers.to_json)
        expect(json).to have_key(content_type)
      end
      it 'header: no, body: valid' do
        json = JSON.parse(post_response_no_header_valid_body.headers.to_json)
        expect(json).to have_key(content_type)
      end
      it 'header: yes, body: no, token: invalid' do
        r = conn.post url
        json = JSON.parse(post_response_no_body_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end
      it 'header: yes, body: no, token: valid' do
        json = JSON.parse(post_response_no_body_valid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end
      it 'header: yes, body: invalid, token: invalid' do
        json = JSON.parse(post_response_invalid_body_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end
      it 'header: yes, body: valid, token: invalid' do
        json = JSON.parse(post_response_invalid_token.headers.to_json)
        expect(json).to have_key(content_type)
      end
    end

    describe 'HTTP bad request' do
      it 'no content type in the headers' do
        expect(post_response_no_header_type.status).to eq(KAYA_HTTP[:bad_request])
      end
      it 'header: no, body: no' do
        expect(post_response_no_header_no_body.status).to eq(KAYA_HTTP[:bad_request])
      end
      it 'header: no, body: invalid' do
        expect(post_response_no_header_invalid_body.status).to eq(KAYA_HTTP[:bad_request])
      end
      it 'header: yes, body: invalid' do
        expect(post_response_invalid_body.status).to eq(KAYA_HTTP[:bad_request])
      end
    end

    describe 'HTTP unauthorized' do
      it 'invalid token' do
        expect(post_response_invalid_token.status).to eq(KAYA_HTTP[:unauthorized])
      end
    end

    describe 'HTTP created' do
      it 'should create test data to match wth JSON Schema' do
        expect(post_response_test_data.status).to eq(KAYA_HTTP[:created])
        expect(post_response_test_data).to match_response_schema("#{s_name}_response_object")
      end
    end
  end  

  describe 'DELETE method' do
    let(:last_id) { EmailProvider.last.id }
    let(:next_id) { last_id + 1 }
    let(:not_found_id) { next_id + 2 }
    let(:url) { "#{api_v1_url}/#{endpoint}/#{last_id}" }
    let(:url_id1) { "#{api_v1_url}/#{endpoint}/1" }
    let(:url_not_found) { "#{api_v1_url}/#{endpoint}/#{not_found_id}" }
    let(:delete_response_bad_request) { conn.authorization :Bearer, token ;  conn.delete url_id1 }
    let(:delete_response_not_found) { conn.authorization :Bearer, token ;  conn.delete url_not_found }
    let(:delete_response) { conn.authorization :Bearer, token ;  conn.delete url }
    
    describe 'HTTP bad request' do
      it "should not delete when used as foreign key" do
        expect(delete_response_bad_request.status).to eq(KAYA_HTTP[:bad_request])
      end
    end
    
    describe 'HTTP not found' do
      it "should not delete non existent" do
        expect(delete_response_not_found.status).to eq(KAYA_HTTP[:not_found])
      end
    end
    describe 'HTTP no content' do
      it "should delete the test data and return #{KAYA_HTTP[:no_content]}" do
        expect(delete_response.status).to eq(KAYA_HTTP[:no_content])
      end
    end
  end 
  
  describe 'PUT method' do
    describe 'HTTP bad request' do
      it "should not update non existent" do
        EmailProvider.create(name: 'test email provier', implemented: true, allow_external: true)
        conn.authorization :Bearer, token
        url = "#{api_v1_url}/#{endpoint}/#{EmailProvider.last.id}"
        r = conn.put url, { name: EmailProvider.first.name, implemented: false, allow_external: true  }.to_json
        expect(r.status).to eq(KAYA_HTTP[:bad_request])
      end
    end
    describe 'HTTP not found' do
      it "should not update non existent" do
        next_id = EmailProvider.last.id + 1
        url = "#{api_v1_url}/#{endpoint}/#{next_id}"
        conn.authorization :Bearer, token
        r = conn.put url, { name: 'test 123', implemented: true, allow_external: false }.to_json
        expect(r.status).to eq(KAYA_HTTP[:not_found])
      end
    end
    describe 'HTTP no content' do
      it "should update the test data and return #{KAYA_HTTP[:no_content]}" do
        url = "#{api_v1_url}/#{endpoint}/#{EmailProvider.last.id}"
        conn.authorization :Bearer, token
        r = conn.put url, { name: 'test provier', implemented: true, allow_external: true }.to_json
        expect(r.status).to eq(KAYA_HTTP[:no_content])
      end
    end
  end 
end
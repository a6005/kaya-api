# frozen_string_literal: true

require_relative '../../environment.rb'
require APP_ROOT.join('spec', 'spec_helper.rb')

endpoint = 'reset_password'
s_name = endpoint

RSpec.describe endpoint do
  include_context 'common'

  let(:url) { "#{api_v1_url}/users/#{test2_user_id}/#{endpoint}" }
  let(:auth_response) { conn.post auth_url, { email: test2_email, password: test2_pw }.to_json }
  let(:token) {
    j = JSON.parse(auth_response.body).with_indifferent_access
    j[:token].to_s
  }

  let(:invalid_pw) { 'test135135' }
  let(:new_short_pw) { 'test123' }
  
  let(:response_no_token) { conn.post url }
  let(:response_empty_current_password) { conn.post url, { current_password: '' }.to_json }
  let(:response_empty_password) { conn.post url, { password: '' }.to_json }
  let(:response_empty_new_passwords) { conn.authorization :Bearer, token ; conn.post url, { 
    current_password: test2_pw, password: '',  password_confirmation: '' }.to_json }
  let(:response_empty_password_confirmation) { conn.post url, { password_confirmation: '' }.to_json }
  let(:response_invalid_password) { conn.authorization :Bearer, token ; conn.post url, {
    current_password: invalid_pw, password: new_valid_pw, password_confirmation: new_valid_pw }.to_json }
  let(:response_valid_password) { conn.authorization :Bearer, token ; conn.post url, {
    current_password: test2_pw, password: new_valid_pw, password_confirmation: new_valid_pw }.to_json }

  describe 'JSON' do
    it 'should respond with json mime type' do
      json = JSON.parse(response_no_token.headers.to_json)
      expect(json[content_type]).to eq(mime_json)
    end
  end

  describe 'Bad request' do
    before(:example) do
      auth_conn.authorization :Bearer, token
    end

    it 'blank body' do
      expect(response_no_token.status).to eq(KAYA_HTTP[:bad_request])
    end

    it 'empty current_password for non admin' do
      expect(response_empty_current_password.status).to eq(KAYA_HTTP[:bad_request])
    end

    it 'should not reset password with empty password' do
      expect(response_empty_password.status).to eq(KAYA_HTTP[:bad_request])
    end

    it 'should not reset password with empty password confirmation' do
      expect(response_empty_password_confirmation.status).to eq(KAYA_HTTP[:bad_request])
    end

    it 'should not reset password with empty passwords' do
      expect(response_empty_new_passwords.status).to eq(KAYA_HTTP[:bad_request])
    end
 
    it 'should not set the same password' do
      r = auth_conn.post url, { current_password: test2_pw, password: test2_pw, password_confirmation: test2_pw }.to_json
      expect(r.status).to eq(KAYA_HTTP[:bad_request])
    end
 
    it 'should not change password when passwords do not match' do
      r = auth_conn.post url, { current_password: test2_pw, password: test2_pw, password_confirmation: invalid_pw }.to_json
      expect(r.status).to eq(KAYA_HTTP[:bad_request])
    end
  end

  describe 'Unauthorized' do
    before(:example) do
      auth_conn.authorization :Bearer, token
    end

    it 'should not change password with invalid credentils' do
      expect(response_invalid_password.status).to eq(KAYA_HTTP[:unauthorized])
    end
 
    it 'should not change password when current password is not valid' do
      r = auth_conn.post url, { current_password: invalid_pw, password: test2_pw, password_confirmation: test2_pw }.to_json
      expect(r.status).to eq(KAYA_HTTP[:unauthorized])
    end
  end

  describe 'Change password:' do
    before(:example) do
      auth_conn.authorization :Bearer, token
    end

    it 'should change password multple times' do
      current_pw = test2_pw
      for i in 1..PASSWORD_HISTORY_LENGTH do
        new_pw = "#{FFaker::Lorem.characters(MIN_PASSWORD_LENGTH)}#{rand(1..20)}"
        # puts "i: #{i} changeing password to #{new_pw} current pw: #{current_pw}"
        r = auth_conn.post url, { current_password: current_pw, password: new_pw, password_confirmation: new_pw }.to_json
        current_pw = new_pw
        # sleep(1)
        n = PasswordHistory.where(user_id: test2_user_id).count
        # puts "p=#{current_pw} history count: #{n}"
        expect(r.status).to eq(KAYA_HTTP[:no_content])
      end
      n = PasswordHistory.where(user_id: test2_user_id).count
      # reset password to original
      new_pw = test2_pw
      # puts "password history: #{n} current_pw=#{current_pw} new_pw: #{new_pw}"
      r = auth_conn.post url, { current_password: current_pw, password: new_pw, password_confirmation: new_pw }.to_json
      #r = auth_conn.post url, { current_password: current_pw, password: test2_pw, password_confirmation: test2_pw }.to_json
      expect(r.status).to eq(KAYA_HTTP[:no_content])
    end
  end
end
 
require_relative '../../environment.rb'
require APP_ROOT.join('spec', 'spec_helper.rb')

RSpec.describe 'user' do
  let(:company_id) { Company.select(:id).first.id }
  let(:security_group_id) { SecurityGroup.select(:id).first.id }
  let(:employee_type_id) { EmployeeType.select(:id).first.id }
  let(:department_id) { Department.select(:id).first.id }
  let(:language_id) { Language.select(:id).first.id }
  let(:os_username) { FFaker::Internet.user_name }
  let(:os_uid) { rand(1000..5000) }
  let(:password) { FFaker::Lorem.characters(10) }
  let(:email) { FFaker::Internet.email }
  let(:a_user) { User.new(
        password: password,
        password_confirmation: password,
        company_id: company_id,
        security_group_id: security_group_id,
        department_id: department_id,
        language_id: language_id,
        os_uid: os_uid,
        os_username: os_username,
        email: email,
        name: FFaker::Name.name,
        surname: FFaker::Name.name,
        initials: 'TT',
        login_shell: '/bin/bash',
        home_dir: "/home/#{os_username}",
        title: 'devops',
        room_number: '1111',
        phone: FFaker::PhoneNumber.phone_number,
        fax: FFaker::PhoneNumber.phone_number,
        enabled: true,
        has_ldap: false, 
        has_email: false,
        employee_number: 9999,
        employee_type_id: employee_type_id,
        mindays: 100, 
        maxdays: 500, 
        warndays: 10
      )}
  
  describe 'db fields' do
    it 'should contain mindays, maxdays and warndays for password' do
      expect(User.columns).to include :mindays, :maxdays, :warndays
    end
  end
  
  describe 'validations' do
    describe 'password' do
      it "should be equal with confirmation" do
        expect {User.create(password: password, password_confirmation: "#{password}-",
                   os_username: FFaker::Name.name)}.to raise_error Sequel::ValidationFailed
      end

      it "should raise validation error when password_confirmation is not present" do
        expect { User.create(password: password, os_username: FFaker::Name.name) }.to raise_error Sequel::ValidationFailed
      end
      
      it "should support $6$ password encryption" do
        u = User.last
        u.name = FFaker::Name.name
        u.current_user = u
        u.save
        expect(u.password_hash[0..2]).to eq("$6$")
      end
    end
    
    #describe 'email' do
    #  it 'domain should come from the available domains' do
    #    d = Domain.first.name
    #    email = "#{FFaker::Internet.user_name}@d"
    #    user = a_user
    #    user.save
 
    #    user.email = FFaker::Internet.email
    #    expect { user.update }.to raise_error Sequel::ValidationFailed
    #  end
    #end
  end
end

require_relative '../environment.rb'
Bundler.require(:test)
require 'simplecov'
require 'simplecov_json_formatter'
require 'net/http'
require_relative 'support/api_schema_matcher'

# Generate HTML and JSON reports
SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([
  SimpleCov::Formatter::HTMLFormatter,
  SimpleCov::Formatter::JSONFormatter
])

RSpec.shared_context 'common' do 
  let(:api_v1_url) { "/#{API_PATH_PREFIX}" }
  let(:api_url) { API_URL }
  let(:admin_email) { 'admin@example.com' }
  let(:admin_pw) { 'admin123654' }
  let(:admin_user_id) { 1 }
  let(:conn) { Faraday.new(api_url, headers: { 'Content-Type' => mime_json }) }
  let(:auth_conn) { Faraday.new(api_url, headers: { 'Content-Type' => mime_json }) }
  let(:auth_url) { "#{api_v1_url}/authentication" }
  let(:content_type) { 'content-type' }
  let(:empty_array_json_object) { [].to_json }
  let(:invalid_token) { 'thisisaninvalidetoken' }
  let(:new_valid_pw) { 'test123456' }
  let(:none_existent_email) { 'nouser.noname@example.com' }
  let(:mime_json) { 'application/json' }
  let(:message_str) { 'message' }
  let(:test_email) { 'test@example.com' }
  let(:test_pw) { 'test123654' }
  let(:test_user_id) { 2 }
  let(:test2_email) { 'test2@example.com' }
  let(:test2_pw) { 'test123654' }
  let(:test2_user_id) { 3 }

  let(:auth_response) { conn.post auth_url, { email: test_email, password: test_pw }.to_json }
  let(:token) {
    j = JSON.parse(auth_response.body).with_indifferent_access
    j[:token].to_s
  }

end

#RSpec.configure do |config|
#  config.before(:suite) do
#    test_email = 'test@example.com'
#    test_pw = 'test123'
#    api_v1_url = "/#{API_PATH_PREFIX}"
#  end
#end

SimpleCov.minimum_coverage SIMPLE_COV_MIN_COVERAGE.to_i 
SimpleCov.start
SimpleCov.at_exit do
  op = SimpleCov.result.coverage_statistics[:line].percent
  color = case op
    when 0..79 then :red
    when 80..94 then :yellow
  else
    :green
  end
  File.write("coverage.svg", Net::HTTP.get(URI.parse("https://img.shields.io/badge/coverage-#{op.round(2)}-#{color}.svg")))
  SimpleCov.result.format!
end

Dir[SRC_PATH.join('initializers', '*.rb')].each { |file| require file }
require SRC_PATH.join('models', 'models.rb')
%w[application.rb authentication.rb openldap.rb job_dispatcher.rb].each do |f| require HELPERS_PATH.join(f).to_s end
require WORKERS_PATH.join('workers.rb')
ENV['ENVIRONMENT'] = 'test'
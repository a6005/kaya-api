require_relative './spec_helper.rb'
require API_DEF_PATH

describe "API definition" do
  it "should produce correct api definition yml" do
    def_file = "definition.yml"
    File.write(def_file,::Apigen::Formats::OpenAPI::V3.generate(API))
    document = Openapi3Parser.load_file(def_file)
    File.delete def_file
    expect(document.valid?).to be true
  end
end